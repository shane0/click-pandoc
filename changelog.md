# change log


## Thu 18 Mar 2021

* [x] updated `test_serve.py` to use `http.server`

## Mon Oct 14

* [x] docx

## Sun Oct 13 

* [x] pdf
* [x] latex

## Sat Oct 12 

* [x] serve
* [x] deploy

## Thu Oct 10

* [x] setup 
* [x] write
* [x] convert

## Wed Oct 9

* [x] init repo

