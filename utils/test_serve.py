#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import http.server
import socketserver
import click
import config

def main(folder,port):
    try:
        import SimpleHTTPServer
        pwd = os.getcwd()
        web_dir = os.path.join(os.path.dirname(__file__), folder)
        os.chdir(web_dir)
        Handler = http.server.SimpleHTTPRequestHandler
        httpd = socketserver.TCPServer(("", port), Handler)
        click.echo('serving %s' % folder)
        click.echo('at %s' % port) 
        click.echo('use ctrl + c to stop')
        httpd.serve_forever()
    finally:
        os.chdir(pwd)

if __name__ == "__main__":
    click.echo('run via click: python cli.py serve')

