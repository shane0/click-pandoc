---
title: troubleshooting
---

**draft**

<iframe width="560" height="315" src="https://www.youtube.com/embed/gI4h3T6ElX0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

this is me summarizing [this](https://shanenull.com/code/smart-questions.html) as I have time

and [readme maturity](https://github.com/LappleApple/feedmereadmes/blob/master/README-maturity-model.md)

# troubleshooting computer issues

* relax - do not make things worse stay rational - consider how much of your time this issue is worth? is it large scope?
	* often people panic and knee jerk when troubleshooting and immediately start changing multiple random things without a clue what is going on
* document
	* write down the exact steps that produce the problem
	* if you see any errors get a screenshot or photo
* try to repeat the problem before asking someone for help - it could be a one time glitch
	* close the error, restart, etc. and see if it happens again
	* repeating the issue is important because once you get to the point where you make changes then you have a way of determining if your changes fixed the issue, or made it worse
	* if you cannot reproduce the issue how is someone supposed to fix it? - do  you want them making random changes until it goes away? probably introducing more future problems? possibly wasting a lot of your time?
* research - once you have a clue what exactly is broken (could be multiple layers)
	* check the documentation - if the documentation is bad you already failed - choose a product with documentation (man page, full working readme, issue tracking
	* check for issues - if there is no issue tracker you have already failed - choose a product that has active responsive issue tracking
	* google it after checking documentation, see if the software or hardware has documentation
	* if you had to google it you may have already failed - reconsider using the product that broke if it broke, and has poor documentation - it maybe have been created by someone who doesn't know what they're doing and probably should be avoided
* make one adjustment at a time and test by repeating the steps that reproduce the issue
	* if the adjustment did not fix it, undo that adjustment
	* repeat until fixed

## checklist for pre failure

sometimes your real problem isn't the error in front of you, it's your poor choice in products

* [ ] is the project open sourced? there are alternatives for nearly all proprietary software now - most are superior quality 
* [ ] does the project have a working readme? 
* [ ] does the project have tests?
* [ ] does the project have issue trackers with an an active community 

## alternatives

* markdown
* shell scripts
* python
	* pandocs
	* pandas
	* click
	* django
