---
title: awesome_docs
---

# awesome documentation

Projects with awesome docs.

* [man pages](https://www.kernel.org/doc/man-pages/)
* [python docs](https://docs.python.org/3/)
* [pandocs](https://github.com/jgm/pandoc/blob/master/README.md) 

What makes them awesome?

* change log includes releases including issues fixed
* badges - status?  is this stable or a wip?  does it test, build? is there active work on issues?
* readme has working install, build, usage steps & links to tutorials
* documented api
* versioned documentation
* instructions and templates for users, developers, maintainers
