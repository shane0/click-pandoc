# powershell

## history

81  batch commands
96  vbscript
2006 ps

## printers

```powershell
get-printers | add-content c:\temp\printers-orig.md
```

## create files

```powershell
$receiptfile = 'filenames.csv'
Get-Content -Path $receiptfile | ForEach-Object {New-Item -ItemType File -Path $_} 
```

## find duplicates

```powershell
$csv = '.\file-with-duplicates.csv'
$dirty = Import-Csv -Path $csv
$dirty | Group-Object -Property column_name | Where-Object { $_.count -ge 2 }
# optional | clip 
```
##  last edited

$recent = Get-ChildItem -Path c:\flowstate\work\web\flask_sites\skeleton\projects\*.* -Filter *.md -r | ? {
    $_.LastWriteTime}
### recently edited

$recent = Get-ChildItem -Path c:\flowstate\work\web\flask_sites\skeleton\projects\*.* -Filter *.md -r | ? {
    $_.LastWriteTime -gt (Get-Date).AddDays(-4)}

$recent.fullname
$recent.Count

## conjunction

_model: page
---
title: conjunction
---
body:


    :::powershell
    #find E:\rlbobj\FixDatAccount3421.ps1
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'FixDatAccount3421.ps1 ' |select Path

    #esd / eipp ftp
    gci -path @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '63.232.227.92' | select Path

    #calling sftp2.paystation.com
    gci -path @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'sftp2.paystation.com' | select Path

    #find rlb24200.exe
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'rlb33600' |select Path

    #find utl840.exe
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts")  -Filter "utl*.exe" -Recurse | select fullname

    #find moveit
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts")  -Filter "*moveit*" -Recurse | select fullname
    Get-ChildItem -Path \\itstore  -Filter "*moveit*" -Recurse | select fullname
    #find moveit scripts
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat  -Recurse  | Select-String -pattern 'moveit' |select Path | Out-File -FilePath c:\backup\dev\moveit_list.txt

    #find all usages copy
    Get-ChildItem -Force -Path @("e:\rlbobj","e:\scripts") -Recurse  -Filter *.bat | Select-String -pattern '@CALL' | Out-File -FilePath c:\backup\dev\call_list.txt

    #find all usage
    Get-ChildItem -Force -Path @("e:\rlbobj","e:\scripts") -Recurse  -Filter *.bat | Select-String -pattern 'usage' | Out-File -FilePath c:\backup\dev\usage_list.txt

    #find ps advanced

    Get-ChildItem -Path @("e:\rlbobj","e:\scripts") -filter *.ps1 -Recurse | Select-String -pattern 'CmdletBinding' | select path | out-file c:\backup\dev\psadvfunc_list.txt

    #owner of file
    #running from seattle
    $box = '1165'
    $date = get-date -format yyMMdd
    $basepath = 'E:\' + $date + '\' + $box
    ls $basepath
    $path = 'E:\160208\3671\C21.SENT'
    $user = Get-Acl $path | % { $_.Owner }
    #getting owners
    $box = '1165'
    #$date = get-date -format yyMMdd
    $date  = 160119
    $basepath = 'E:\' + $date + '\' + $box
    $basefiles = ls $basepath
    $basefiles | % { get-acl $_.fullname | select PSChildName,owner }

    #one file
    get-acl 'E:\160208\3671\C21.SENT' | select owner

    #find my swiftco file

    gci -path c:\backup -filter *.ps1 | ? {$_.Name -like "*swift*"} | select fullname

    # search for work, generates a file, then search the file
    # make this recurring, logging a report in html on static site
    $f = 'c:\logs\boxupload.txt'
    $d = get-date -format yyMMdd
    $d >> $f
    Get-ChildItem  E:\151116\  | Where-Object {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object | select fullname >> $f
    invoke-item $f
    #new-item -path $f -type file -force
    # regex cc
    ^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$

    # find work days for a box
    $dir = 'e:\'
    $pattern = '2728'
    gci -Path $dir -filter "*$pattern*" -r | ? {$_.PSisContainer -eq $true}
    get-childitem $dir -Directory -recurse | where {$_.Attributes -eq 'Directory' -and $_.name -eq 1473}


    # search for filenames or content matching patterns
    $pattern = 'linton'
    $pattern = 'rlm20000.exe'

    $dir = 'e:\scripts'
    $dir = 'e:\rlbobj'
    Get-ChildItem -Path $dir -filter "*$pattern*"  | Where {$_.PSisContainer -eq $false -and $_.Name -match $pattern} | select fullname
    Get-ChildItem -Path $dir -r | Where {$_.PSisContainer -eq $false -and $_.Name -match $pattern} | select fullname
    Get-ChildItem -Path $dir -r | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts")  | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path
    Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts")  | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    tree $($dir) /f  | Select-Object -Skip 2

    #search for filename
    $pattern = '74.200.55.172'
    $dir = 'e:\rlbobj\'
    #filename match
    Get-ChildItem -Path $dir  | Where {$_.PSisContainer -eq $false -and $_.Name -match $pattern} | select fullname | clip
    #content match
    Get-ChildItem -Path $dir -file  | Where {$_.PSisContainer -eq $false} |Select-String -pattern $pattern | group path | select name

    #search in files http://wiki/doku.php?id=it:software:rlms2retailweb
    $pattern = 'rlm20000'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name

    $allvbs = Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts") -include *.vbs | select fullname
    $allvbs | clip

    #find tiff2pdf script

    #search in files
    $pattern = 'tiff2pdf'
    gci -path c:\backup -file -filter *.ps1 | Select-String -pattern $pattern
    Get-ChildItem  -Path @("c:\backup","e:\scripts") -file -Filter *.ps1 | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name

    #search in files for rlm2000.exe
    $pattern = 'ivr10000'
    gci -path e:\rlbobj -file -filter *.bat | Select-String -pattern $pattern | select path | clip
    Get-ChildItem  -Path @("c:\backup","e:\scripts") -file -Filter *.ps1 | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name



    #search in files 2
    $pattern = 'pid'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.ps1 -Recurse | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    #compare to all
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.ps1 -Recurse | group path | select name

    #find css
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.css -Recurse | group path | select name

    $allps = Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts") -include *.ps1 | select fullname
    $allps | clip

    # search afs for special config
    $thisone = 'e:\rlbobj\afsimport.bat'
    $pattern2 = '^@IF\s\%'
    $file =  Select-String -Path $thisone -pattern $pattern2 -Context 1,2
    foreach ($_ in $file){
    $_
    }

    #search for cc
    $searchPath = 'e:\151207'
    $ccpattern = '^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$'
    #search for filenames w/ pattern
    Get-ChildItem -path $searchPath -recurse | Select-String -list -pattern $ccpattern | group path | select name

    #return content?
    $search =  Select-String -Path $thisone -pattern $pattern2
    foreach ($_ in $search){
    $_
    }
    C:\trash\cctest


    # general use find folders
    $pathdir = "\\sea-r610-28\X"
    Get-ChildItem -path $pathdir | Where-Object {$_.PSisContainer -eq $true} | select name

    #example of pulling tif paths out of index and testing path
    $tpath = '\D:\\\d{6}\\\d{4}\\docs\\\d{3}\\s\d{10}\.tif'
    $indx = 'C:\backup\test.csv'
    $matches = select-string -Path $indx -Pattern $tpath  -AllMatches | % { $_.Matches } | % { $_.Value }
    foreach($_ in $matches)
    {
    $_
    test-path $_
    }

    #search from list
    foreach($_ in get-content 'C:\backup\tickets\sanitary\3408-sanitary.txt')
    {
    $clean = $_.replace(" ","")
    $list = $clean | where {$_.contains("e:") -or $_.contains('E:')}
    get-content $list | select-string  "posting"
    }

    #copssh http://wiki/doku.php?id=it:infrastructure:software:copssh

    # to generate this list
    Get-ChildItem -Path C:\RLMS\apps\* -include *.bat,*.ps1, *.vbs | select-string cop | select path | out-file c:\rlms\apps\calls_cop.txt
    Get-ChildItem -Path @("e:\rlbobj\*","e:\scripts\*") -include *.bat,*.ps1, *.vbs | select-string copssh `
    | select path | out-file E:\tools\Infrastructure\swiftco\calls_cop_seattle.txt

    #find qdr
    Get-ChildItem -Path e:\tools -include *.qdr -Recurse | select fullname | clip

    #find jack henry
    Get-ChildItem -Path @("e:\rlbobj\*","e:\scripts\*") -include *.bat,*.ps1, *.vbs | select-string '216.116.95.111' `
    | select path

    #get files edited in x days
    (Get-ChildItem -Path c:\backup -Filter *.zip | ? {
      $_.LastWriteTime -gt (Get-Date).AddDays(-90)
    }) | select fullname | clip

    #find e:\rlbobj\sendbofaach.bat
    Get-ChildItem -Path @("e:\rlbobj\*","e:\scripts\*") -include *.bat | select-string 'e:\rlbobj\sendbofaach.bat' | select path
    | select path

    #find winscp examples

    $pattern = '%UID%'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    Get-ChildItem  e:\rlbobj -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    # E:\rlbobj\test2761.bat E:\rlbobj\sendHSBC21.bat
    #homestreet proc move find whatev, to pass parameters

    $pattern = 'fulldate'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} `
    | Select-String -pattern $pattern | group path | select name| Sort-Object -Property LastWriteTime

    #shaneslife lol
    gci -Path e:\tools -filter *.png -Recurse | % {remove-item -path $_.fullname}
    #clean garba
    gci -Path e:\tools -filter *.remove -Recurse | % {remove-item -path $_.fullname}

    gci -Path c:\backup\tools -filter *.png -Recurse | % {copy-item -path $_.fullname -Destination C:\backuparchive\life}

    #ps find .sent example
    Get-ChildItem -Path @("e:\rlbobj","e:\scripts") -filter *.ps1 -Recurse | Select-String -pattern '.sent' | select path

    #find wells fargo for lily
    gci -path @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'fargo' | select Path

    # impact sales 4131 did they have work?

    #specific box count down from x days ago
    $x = 7
    $e = 'e:\'
    $match = '4131'
    for ($i=$x; $i -ge 0; $i--) {
    $dt = (Get-Date).AddDays(-$i)
    $ymd = Get-Date $dt -Format yyMMdd
    $boxfolder1 = "$e$ymd"
    #write-host "looking in $boxfolder1"
    foreach($_ in (ls -path $boxfolder1 | ? {$_.PSIsContainer -eq $true -and  $_.Name -match $match }| Sort-Object ))
    {
    $boxfolder2 = "$boxfolder1\$_"
    $boxfolder2
    }
    }

    #find login for lily
    Get-ChildItem -path @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'mi15736 ' | select Path

    # find atom projects saved

    get-content -path C:\Users\shane.null\.atom\projects.cson | Select-String -list -pattern \\
    select-string C:\Users\shane.null\.atom\projects.cson -pattern title -list

    #find the sc


    # find ocr
    locations.txt
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'FixDatAccount3421.ps1 ' |select Path

    # find what creates boxusage.txt its E:\rlbobj\rlb243.bat or E:\rlbobj\GetInstitutionList.bat
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'boxusage.txt' |select Path

    # atom project imports
    ls -Path C:\backup\tools\1000-4000 -Directory | select fullname | clip

    # find tacoma ftp login
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '1013-tacoma' |select Path


    #14878
    X:\Drive Recovery
    – Submitted by Jill Barnett on 2016-06-14 at 01:28 PM

    Check to see most recent copy of following files:

    Coupon Implementation\Check Digit Test\check digit.xls

    RLMS\PMN ACH Migration\PMN ACH to Pedestal Conversion.xlsx

    #find in journal?
    gci -Path C:\backup\core\journal  | Select-String -pattern "4611" | select path

    # find mongoose files
    $mongoose = gci -path c:\ -r -filter mong*
    $m2 = ls '\\10.0.0.142\c$' -Filter mong* -r

    # find wbadmin
    gci -path c:\backup -r | select-string -pattern "wbadmin" | select path

    # generate some dates to resend
    $x = 38
    for ($i=$x; $i -ge 0; $i--){
    $dt = (Get-Date).AddDays(-$i)
    $ymd = Get-Date $dt -Format yyMMdd
    $ymd
    }

    # find powershell on wiki, etc.

    ls -Path \\10.0.0.142\c$\inetpub\wwwroot\dokuwiki\data\pages\ -Filter *.txt -r | select-string "<code>" | % {copy-item $_.path -Destination C:\backup\dev\samples}

    ls -Path \\10.0.0.142\c$\inetpub\wwwroot\dokuwiki\data\pages\ -Filter *.txt -r | select-string "wbadmin" | % {copy-item $_.path -Destination C:\backup\dev\samples}

    ls -path @("e:\rlbobj","e:\scripts") -include *.bat,*.ps1,*.vbs -r |  % {copy-item $_.fullname -Destination C:\backup\dev\samples}

    # find winscp session

    ls -path e:\rlbobj -filter send*.bat | select-string "winscp"
    ls -path e:\rlbobj -filter send*.bat | select-string "pass"

    # find map drives

    $map = ls -Path c:\backup -Filter *.bat -r | Select-String "net use" | select path
    $map | clip
    ls -Path c:\backup -Filter *.bat -r | Select-String "net use" | % {copy-item $_.path -destination  C:\backup\dev\map-drives\examples}

    # message board where's data
    ls -path C:\backup\core\message-board\expressjs-messageboard-master -r | select-string "asdfasdf"
    # find most recently edited file
    gci C:\backup\core\message-board\expressjs-messageboard-maste -r | sort LastWriteTime | select -last 1

    # kodak search
    $kfinds = gci c:\backup -r | select-string "kodak" | Get-Unique | select path
    $kfinds | clip

    # find kodak software

    $zfinds = gci c:\backup -filter *.ps1 -r | select-string "'network-scanner-result'" | Get-Unique | select path

    # list sites on \\10.0.0.22\c$\inetpub
    gci \\10.0.0.22\c$\inetpub -directory | select name | clip

    # find gpg

    ls c:\backup -Directory -filter *encr* -r | select fullname


    # find usage for sendmail.ps1
    ls e:\scripts -File -include *.bat,*.ps1 -r| select-string "e:\scripts\email\sendemail*"
    steven used it, attachments needs single quotes, to address does not?  just needs , seperation

    # find sentinel

    $sentfinds = gci e:\rlbobj -filter *.bat | select-string ".sent" | Get-Unique | select path
    $sentfinds | clip

    # building 4612 prep need afs date mmddyyyy
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'mmddyyyy' |select Path
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'set visiondir' |select Path
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'moveit' |select Path
    moveit

    # find tacoma ftp login
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '' |select Path

    # demo of auto documentation
    ls e:\tools\steven\batch-tests -filter *.bat  | Select-String -pattern 'BLAH.EXE' |select Path

    # splf http://wiki/doku.php?id=it:winscp
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'winscp' |select Path

    # 4886 leading zeros come from file:///E:/rlbobj/whs81800.exe what calls it

    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'whs81800' |select Path

    # find iso
    $isos = gci c:\ -filter *.iso -r
    $isos | select fullname | clip

    # find examples to fix 8446 9402
    $orphanboxes = ls \\sea-r320-30\c$\24x7\Output -r | select-string -pattern '.dat does not exist'

    # rlb37520
    $3752 = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'rlb37520' |select Path
    $3752 | clip

    # find jekyll journal temp
    ls c:\backup -Filter *.ps1 -r | select-string -Pattern 'site.data.tickets' | select Path


    $env:path.split(“;”)

    # script broke, find C:\backuparchive
    ls C:\backuparchive -filter markdown*.ps1 -r

    # helen find some afs doc? afs install w/ derog snohomish 1st quarter attachment maybe
    $starts = ls \\10.0.0.142\c$\inetpub\wwwroot\dokuwiki\data\pages  -Filter start.txt -r
    $starts | % {$_}


    # find getbusinessdate
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '@if "%2"==""' |select Path


    # find RLB51760

    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'RLB51760' |select Path


    # find idahostatesman ftp
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'idahostatesman' |select Path
    Get-ChildItem -Path  'Y:\Customer Accounts' -Filter *.doc | Select-String -pattern 'idahostatesman' |select Path

    # lilly evergreen won't send 64.122.205.115
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '64.122.205.115' |select Path

    # find qc report
    $match = "Pedestal_QC_Report*"
    Get-ChildItem -Path  e: -file -Filter *.pdf -Recurse |? {$_.PSIsContainer -eq $false -and  $_.Name -match $match }
    Get-ChildItem -Path  y:\ -file -Filter Pedestal_QC_Report*.pdf -Recurse
    Get-ChildItem -Path  y:\ -file -Filter *.pdf -Recurse | % {$_ | ac c:\temp\ypdfs.md}
    $match = "Pedestal_QC_Report*"
    Get-ChildItem -Path  e: -file -Filter *.pdf -Recurse |? {$_.PSIsContainer -eq $false -and  $_.Name -match $match } | % {$_ | ac c:\temp\ypdfs.md}

    # find delete timebombs
    $bombs = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '\*\.\*' |select Path

    $timebombs = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '@rd' |select Path
    $timebombs | clip
    # find ftp in imp folder file:///Y:/Customer%20Accounts/Boise/Implementation/FTP%20at%20Boise%20City.txt
    $ftp = Get-ChildItem 'Y:\Customer Accounts\*' -file -Filter *.txt -r
    $ftp | ? {$_.name -match 'ftp'} | select fullname

    # find jobs that didn't run on 20161114
    $14outs = Get-ChildItem -Path '\\10.0.0.142\c$\inetpub\wwwroot\static\scheduler_logs' -r -filter *20161114*.out
    $14outs.name | clip
    $dirs = Get-ChildItem -Path '\\10.0.0.142\c$\inetpub\wwwroot\static\scheduler_logs' -Directory
    $dirs.name | Sort-Object name | clip

    get-content '\\sea-2020-63\prod\_scheduler_research_20161114\eliminated.md' | % {start-process http://wiki:8000/scheduler_logs/$_}
    get-content '\\sea-2020-63\prod\_scheduler_research_20161114\in-timeframe.md' | % {start-process http://wiki:8000/scheduler_logs/$_}

    # find MI15736 for ftpplus can't connect to fiserv
    $fiservscripts = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern "mi15736" |select Path


    # kodak canon 180 driver software for afs Directory: Y:\Temp\bill.fricke\Documents\Canon CR-180 Installation Code
    #    and Setup Doc\Canon CR-180 Installation Code and Setup Doc
    Get-ChildItem -Path y:\ -Directory -Recurse *SU180*
    # activation key?
    Get-ChildItem -Path \\itstore\ -file -Recurse *180*

    # find job rms4554.bat
    $4554iswhere = ls \\sea-r320-30\c$\24x7\Output -r | select-string -pattern 'rms4554.bat'

    # find the pc document script
    $wikipcscript =
    ls C:\backup\dev -r | select-string -pattern 'seattle.xml'

    # find vision ip

    $visionmatch = ls y:\ -r *visionip* | out-file c:\temp\visionip.md
    $visionmatch | select fullname

    # pic for exchange done had to do the snapin this is in dev/exchange now

    ls \\itstore\apps -filter *pic*.ps1 -r | select fullname
    ls \\mail1\c$ -filter *pic*.ps1 -r | select fullname

    # afsimport calls

    get-content -Path e:\rlbobj\afsimport.bat | Select-String -pattern '@call' | select line | clip
    get-content -Path e:\rlbobj\afsimport.bat | Select-String -pattern '(e):\\' | select line | clip

    # bootstrap css
    ls -Path C:\users\shane.null\flask-cookie -r | select-string -Pattern '@brand-primary' | select filename


    # random kodak crap

    #backups

    invoke-item C:\backup\core\jobs\kodakbackup.ps1
    invoke-item C:\backup\kodak\kodak.ps1

    # installs


    [xml]$netmap = Get-Content  'C:\backup\seattle.xml'
    $netmap.'network-scanner-result'.devices.item | ? {$_.apps -like '*KODAK Capture Pro Software*'} | select hostname


    #kodak apps backup one
    if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"}
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
    $source = '\\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps'
    $target = "E:\tools\Infrastructure\kodaks\backups\rlb221-scan007-apps.zip"
    sz a -mx=9 $target $source


    # backup function
    function backup-kodak($computer)
    {
    if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"}
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
    $source = "\\$computer\c$\kodak\xvcs6c\Apps"
    $target = "E:\tools\Infrastructure\kodaks\backups\$computer`-apps.zip"
    sz a -mx=9 $target $source
    }
    $computer ='rlb221-scan007.rli.local'
    $computer = 'rlb220-test001.rli.local'
    $computer = 'rlb220-scan003.rli.local'
    $computer = 'rlb220-index008.rli.local'
    backup-kodak($computer)

    #info

    \\rlb220-test001.rli.local\c$\kodak\xvcs6c\Apps
    \\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps
    \\rlb220-scan003.rli.local\c$\kodak\xvcs6c\Apps
    \\rlb220-index008.rli.local\c$\Kodak\xvcs6c\Apps

    $installs = get-content C:\backup\kodak\installs.md
    foreach($_ in $installs)
    {
    Get-ChildItem -Path $_
    }

    gci \\rlb220-test001.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-test001.rli.local.txt
    gci \\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb221-scan007.rli.local.txt
    gci \\rlb220-scan003.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-scan003.rli.local.txt
    gci \\rlb220-index008.rli.local\c$\Kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-index008.rli.local.txt
    gci \\rlb220-scan002.rli.local\C$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-scan002.rli.local.txt
    $ref = get-content 'E:\tools\Infrastructure\kodaks\rlb220-scan002.rli.local.txt'
    $1 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-index008.rli.local.txt'
    $2 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-scan003.rli.local.txt'
    $3 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-test001.rli.local.txt'
    $4 =  get-content 'E:\tools\Infrastructure\kodaks\rlb221-scan007.rli.local.txt'

    Compare-Object $ref $1
    Compare-Object $ref $2
    Compare-Object $ref $3
    Compare-Object $ref $4
    Compare-Object $ref $5

    # atom packages

    ls C:\Users\shane.null\.atom\packages -Directory | select name | clip

    #find c#
    ls \\rlb221-fs004\c$\ -filter *.cs -r

    # find this 3466 alert because they didn't think to include the script generating the email
    $noreadability = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'ezpay' |select Path
    $noreadability | clip

    # fix apm

    Get-ItemProperty 'Registry::HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings' | Select-Object *Proxy*


    $proxies = (Get-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings').proxyServer
    [System.Net.WebProxy]::GetDefaultProxy() | select address

    $IESettings = Get-ItemProperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings'
    $Proxy = "http://$(($IESettings.ProxyServer.Split(';') | ? {$_ -match 'ttp='}) -replace '.*=')"
    Invoke-WebRequest 'http://some-site.com' -ProxyUseDefaultCredentials -Proxy $Proxy

    reg query "HKCU\software\microsoft\windows\currentversion\internet Settings" /v "ProxyServer"

    # find pdf cat

    $path = 'C:\Users\shane.null\Envs'
    ls $path -Filter pdfcat.py -r

    # find jills keepass
    ls e:\ -r -filter *.kbdx


## old cheatssheet lektor linux from life repo
_model: page
---
title: conjunction
---
body:


    :::powershell
    #find E:\rlbobj\FixDatAccount3421.ps1
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'FixDatAccount3421.ps1 ' |select Path

    #esd / eipp ftp
    gci -path @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '63.232.227.92' | select Path

    #calling sftp2.paystation.com
    gci -path @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'sftp2.paystation.com' | select Path

    #find rlb24200.exe
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'rlb33600' |select Path

    #find utl840.exe
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts")  -Filter "utl*.exe" -Recurse | select fullname

    #find moveit
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts")  -Filter "*moveit*" -Recurse | select fullname
    Get-ChildItem -Path \\itstore  -Filter "*moveit*" -Recurse | select fullname
    #find moveit scripts
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat  -Recurse  | Select-String -pattern 'moveit' |select Path | Out-File -FilePath c:\backup\dev\moveit_list.txt

    #find all usages copy
    Get-ChildItem -Force -Path @("e:\rlbobj","e:\scripts") -Recurse  -Filter *.bat | Select-String -pattern '@CALL' | Out-File -FilePath c:\backup\dev\call_list.txt

    #find all usage
    Get-ChildItem -Force -Path @("e:\rlbobj","e:\scripts") -Recurse  -Filter *.bat | Select-String -pattern 'usage' | Out-File -FilePath c:\backup\dev\usage_list.txt

    #find ps advanced

    Get-ChildItem -Path @("e:\rlbobj","e:\scripts") -filter *.ps1 -Recurse | Select-String -pattern 'CmdletBinding' | select path | out-file c:\backup\dev\psadvfunc_list.txt

    #owner of file
    #running from seattle
    $box = '1165'
    $date = get-date -format yyMMdd
    $basepath = 'E:\' + $date + '\' + $box
    ls $basepath
    $path = 'E:\160208\3671\C21.SENT'
    $user = Get-Acl $path | % { $_.Owner }
    #getting owners
    $box = '1165'
    #$date = get-date -format yyMMdd
    $date  = 160119
    $basepath = 'E:\' + $date + '\' + $box
    $basefiles = ls $basepath
    $basefiles | % { get-acl $_.fullname | select PSChildName,owner }

    #one file
    get-acl 'E:\160208\3671\C21.SENT' | select owner

    #find my swiftco file

    gci -path c:\backup -filter *.ps1 | ? {$_.Name -like "*swift*"} | select fullname

    # search for work, generates a file, then search the file
    # make this recurring, logging a report in html on static site
    $f = 'c:\logs\boxupload.txt'
    $d = get-date -format yyMMdd
    $d >> $f
    Get-ChildItem  E:\151116\  | Where-Object {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object | select fullname >> $f
    invoke-item $f
    #new-item -path $f -type file -force
    # regex cc
    ^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$

    # find work days for a box
    $dir = 'e:\'
    $pattern = '2728'
    gci -Path $dir -filter "*$pattern*" -r | ? {$_.PSisContainer -eq $true}
    get-childitem $dir -Directory -recurse | where {$_.Attributes -eq 'Directory' -and $_.name -eq 1473}


    # search for filenames or content matching patterns
    $pattern = 'linton'
    $pattern = 'rlm20000.exe'

    $dir = 'e:\scripts'
    $dir = 'e:\rlbobj'
    Get-ChildItem -Path $dir -filter "*$pattern*"  | Where {$_.PSisContainer -eq $false -and $_.Name -match $pattern} | select fullname
    Get-ChildItem -Path $dir -r | Where {$_.PSisContainer -eq $false -and $_.Name -match $pattern} | select fullname
    Get-ChildItem -Path $dir -r | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts")  | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path
    Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts")  | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    tree $($dir) /f  | Select-Object -Skip 2

    #search for filename
    $pattern = '74.200.55.172'
    $dir = 'e:\rlbobj\'
    #filename match
    Get-ChildItem -Path $dir  | Where {$_.PSisContainer -eq $false -and $_.Name -match $pattern} | select fullname | clip
    #content match
    Get-ChildItem -Path $dir -file  | Where {$_.PSisContainer -eq $false} |Select-String -pattern $pattern | group path | select name

    #search in files http://wiki/doku.php?id=it:software:rlms2retailweb
    $pattern = 'rlm20000'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name

    $allvbs = Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts") -include *.vbs | select fullname
    $allvbs | clip

    #find tiff2pdf script

    #search in files
    $pattern = 'tiff2pdf'
    gci -path c:\backup -file -filter *.ps1 | Select-String -pattern $pattern
    Get-ChildItem  -Path @("c:\backup","e:\scripts") -file -Filter *.ps1 | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name

    #search in files for rlm2000.exe
    $pattern = 'ivr10000'
    gci -path e:\rlbobj -file -filter *.bat | Select-String -pattern $pattern | select path | clip
    Get-ChildItem  -Path @("c:\backup","e:\scripts") -file -Filter *.ps1 | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name



    #search in files 2
    $pattern = 'pid'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.ps1 -Recurse | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    #compare to all
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.ps1 -Recurse | group path | select name

    #find css
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.css -Recurse | group path | select name

    $allps = Get-ChildItem -Recurse -Force -Path @("e:\rlbobj","e:\scripts") -include *.ps1 | select fullname
    $allps | clip

    # search afs for special config
    $thisone = 'e:\rlbobj\afsimport.bat'
    $pattern2 = '^@IF\s\%'
    $file =  Select-String -Path $thisone -pattern $pattern2 -Context 1,2
    foreach ($_ in $file){
    $_
    }

    #search for cc
    $searchPath = 'e:\151207'
    $ccpattern = '^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$'
    #search for filenames w/ pattern
    Get-ChildItem -path $searchPath -recurse | Select-String -list -pattern $ccpattern | group path | select name

    #return content?
    $search =  Select-String -Path $thisone -pattern $pattern2
    foreach ($_ in $search){
    $_
    }
    C:\trash\cctest


    # general use find folders
    $pathdir = "\\sea-r610-28\X"
    Get-ChildItem -path $pathdir | Where-Object {$_.PSisContainer -eq $true} | select name

    #example of pulling tif paths out of index and testing path
    $tpath = '\D:\\\d{6}\\\d{4}\\docs\\\d{3}\\s\d{10}\.tif'
    $indx = 'C:\backup\test.csv'
    $matches = select-string -Path $indx -Pattern $tpath  -AllMatches | % { $_.Matches } | % { $_.Value }
    foreach($_ in $matches)
    {
    $_
    test-path $_
    }

    #search from list
    foreach($_ in get-content 'C:\backup\tickets\sanitary\3408-sanitary.txt')
    {
    $clean = $_.replace(" ","")
    $list = $clean | where {$_.contains("e:") -or $_.contains('E:')}
    get-content $list | select-string  "posting"
    }

    #copssh http://wiki/doku.php?id=it:infrastructure:software:copssh

    # to generate this list
    Get-ChildItem -Path C:\RLMS\apps\* -include *.bat,*.ps1, *.vbs | select-string cop | select path | out-file c:\rlms\apps\calls_cop.txt
    Get-ChildItem -Path @("e:\rlbobj\*","e:\scripts\*") -include *.bat,*.ps1, *.vbs | select-string copssh `
    | select path | out-file E:\tools\Infrastructure\swiftco\calls_cop_seattle.txt

    #find qdr
    Get-ChildItem -Path e:\tools -include *.qdr -Recurse | select fullname | clip

    #find jack henry
    Get-ChildItem -Path @("e:\rlbobj\*","e:\scripts\*") -include *.bat,*.ps1, *.vbs | select-string '216.116.95.111' `
    | select path

    #get files edited in x days
    (Get-ChildItem -Path c:\backup -Filter *.zip | ? {
      $_.LastWriteTime -gt (Get-Date).AddDays(-90)
    }) | select fullname | clip

    #find e:\rlbobj\sendbofaach.bat
    Get-ChildItem -Path @("e:\rlbobj\*","e:\scripts\*") -include *.bat | select-string 'e:\rlbobj\sendbofaach.bat' | select path
    | select path

    #find winscp examples

    $pattern = '%UID%'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    Get-ChildItem  e:\rlbobj -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
    # E:\rlbobj\test2761.bat E:\rlbobj\sendHSBC21.bat
    #homestreet proc move find whatev, to pass parameters

    $pattern = 'fulldate'
    Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} `
    | Select-String -pattern $pattern | group path | select name| Sort-Object -Property LastWriteTime

    #shaneslife lol
    gci -Path e:\tools -filter *.png -Recurse | % {remove-item -path $_.fullname}
    #clean garba
    gci -Path e:\tools -filter *.remove -Recurse | % {remove-item -path $_.fullname}

    gci -Path c:\backup\tools -filter *.png -Recurse | % {copy-item -path $_.fullname -Destination C:\backuparchive\life}

    #ps find .sent example
    Get-ChildItem -Path @("e:\rlbobj","e:\scripts") -filter *.ps1 -Recurse | Select-String -pattern '.sent' | select path

    #find wells fargo for lily
    gci -path @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'fargo' | select Path

    # impact sales 4131 did they have work?

    #specific box count down from x days ago
    $x = 7
    $e = 'e:\'
    $match = '4131'
    for ($i=$x; $i -ge 0; $i--) {
    $dt = (Get-Date).AddDays(-$i)
    $ymd = Get-Date $dt -Format yyMMdd
    $boxfolder1 = "$e$ymd"
    #write-host "looking in $boxfolder1"
    foreach($_ in (ls -path $boxfolder1 | ? {$_.PSIsContainer -eq $true -and  $_.Name -match $match }| Sort-Object ))
    {
    $boxfolder2 = "$boxfolder1\$_"
    $boxfolder2
    }
    }

    #find login for lily
    Get-ChildItem -path @("e:\rlbobj","e:\scripts") -Filter *.bat    | Select-String -pattern 'mi15736 ' | select Path

    # find atom projects saved

    get-content -path C:\Users\shane.null\.atom\projects.cson | Select-String -list -pattern \\
    select-string C:\Users\shane.null\.atom\projects.cson -pattern title -list

    #find the sc


    # find ocr
    locations.txt
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'FixDatAccount3421.ps1 ' |select Path

    # find what creates boxusage.txt its E:\rlbobj\rlb243.bat or E:\rlbobj\GetInstitutionList.bat
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'boxusage.txt' |select Path

    # atom project imports
    ls -Path C:\backup\tools\1000-4000 -Directory | select fullname | clip

    # find tacoma ftp login
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '1013-tacoma' |select Path


    #14878
    X:\Drive Recovery
    – Submitted by Jill Barnett on 2016-06-14 at 01:28 PM

    Check to see most recent copy of following files:

    Coupon Implementation\Check Digit Test\check digit.xls

    RLMS\PMN ACH Migration\PMN ACH to Pedestal Conversion.xlsx

    #find in journal?
    gci -Path C:\backup\core\journal  | Select-String -pattern "4611" | select path

    # find mongoose files
    $mongoose = gci -path c:\ -r -filter mong*
    $m2 = ls '\\10.0.0.142\c$' -Filter mong* -r

    # find wbadmin
    gci -path c:\backup -r | select-string -pattern "wbadmin" | select path

    # generate some dates to resend
    $x = 38
    for ($i=$x; $i -ge 0; $i--){
    $dt = (Get-Date).AddDays(-$i)
    $ymd = Get-Date $dt -Format yyMMdd
    $ymd
    }

    # find powershell on wiki, etc.

    ls -Path \\10.0.0.142\c$\inetpub\wwwroot\dokuwiki\data\pages\ -Filter *.txt -r | select-string "<code>" | % {copy-item $_.path -Destination C:\backup\dev\samples}

    ls -Path \\10.0.0.142\c$\inetpub\wwwroot\dokuwiki\data\pages\ -Filter *.txt -r | select-string "wbadmin" | % {copy-item $_.path -Destination C:\backup\dev\samples}

    ls -path @("e:\rlbobj","e:\scripts") -include *.bat,*.ps1,*.vbs -r |  % {copy-item $_.fullname -Destination C:\backup\dev\samples}

    # find winscp session

    ls -path e:\rlbobj -filter send*.bat | select-string "winscp"
    ls -path e:\rlbobj -filter send*.bat | select-string "pass"

    # find map drives

    $map = ls -Path c:\backup -Filter *.bat -r | Select-String "net use" | select path
    $map | clip
    ls -Path c:\backup -Filter *.bat -r | Select-String "net use" | % {copy-item $_.path -destination  C:\backup\dev\map-drives\examples}

    # message board where's data
    ls -path C:\backup\core\message-board\expressjs-messageboard-master -r | select-string "asdfasdf"
    # find most recently edited file
    gci C:\backup\core\message-board\expressjs-messageboard-maste -r | sort LastWriteTime | select -last 1

    # kodak search
    $kfinds = gci c:\backup -r | select-string "kodak" | Get-Unique | select path
    $kfinds | clip

    # find kodak software

    $zfinds = gci c:\backup -filter *.ps1 -r | select-string "'network-scanner-result'" | Get-Unique | select path

    # list sites on \\10.0.0.22\c$\inetpub
    gci \\10.0.0.22\c$\inetpub -directory | select name | clip

    # find gpg

    ls c:\backup -Directory -filter *encr* -r | select fullname


    # find usage for sendmail.ps1
    ls e:\scripts -File -include *.bat,*.ps1 -r| select-string "e:\scripts\email\sendemail*"
    steven used it, attachments needs single quotes, to address does not?  just needs , seperation

    # find sentinel

    $sentfinds = gci e:\rlbobj -filter *.bat | select-string ".sent" | Get-Unique | select path
    $sentfinds | clip

    # building 4612 prep need afs date mmddyyyy
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'mmddyyyy' |select Path
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'set visiondir' |select Path
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'moveit' |select Path
    moveit

    # find tacoma ftp login
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '' |select Path

    # demo of auto documentation
    ls e:\tools\steven\batch-tests -filter *.bat  | Select-String -pattern 'BLAH.EXE' |select Path

    # splf http://wiki/doku.php?id=it:winscp
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'winscp' |select Path

    # 4886 leading zeros come from file:///E:/rlbobj/whs81800.exe what calls it

    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'whs81800' |select Path

    # find iso
    $isos = gci c:\ -filter *.iso -r
    $isos | select fullname | clip

    # find examples to fix 8446 9402
    $orphanboxes = ls \\sea-r320-30\c$\24x7\Output -r | select-string -pattern '.dat does not exist'

    # rlb37520
    $3752 = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'rlb37520' |select Path
    $3752 | clip

    # find jekyll journal temp
    ls c:\backup -Filter *.ps1 -r | select-string -Pattern 'site.data.tickets' | select Path


    $env:path.split(“;”)

    # script broke, find C:\backuparchive
    ls C:\backuparchive -filter markdown*.ps1 -r

    # helen find some afs doc? afs install w/ derog snohomish 1st quarter attachment maybe
    $starts = ls \\10.0.0.142\c$\inetpub\wwwroot\dokuwiki\data\pages  -Filter start.txt -r
    $starts | % {$_}


    # find getbusinessdate
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '@if "%2"==""' |select Path


    # find RLB51760

    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'RLB51760' |select Path


    # find idahostatesman ftp
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'idahostatesman' |select Path
    Get-ChildItem -Path  'Y:\Customer Accounts' -Filter *.doc | Select-String -pattern 'idahostatesman' |select Path

    # lilly evergreen won't send 64.122.205.115
    Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '64.122.205.115' |select Path

    # find qc report
    $match = "Pedestal_QC_Report*"
    Get-ChildItem -Path  e: -file -Filter *.pdf -Recurse |? {$_.PSIsContainer -eq $false -and  $_.Name -match $match }
    Get-ChildItem -Path  y:\ -file -Filter Pedestal_QC_Report*.pdf -Recurse
    Get-ChildItem -Path  y:\ -file -Filter *.pdf -Recurse | % {$_ | ac c:\temp\ypdfs.md}
    $match = "Pedestal_QC_Report*"
    Get-ChildItem -Path  e: -file -Filter *.pdf -Recurse |? {$_.PSIsContainer -eq $false -and  $_.Name -match $match } | % {$_ | ac c:\temp\ypdfs.md}

    # find delete timebombs
    $bombs = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '\*\.\*' |select Path

    $timebombs = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern '@rd' |select Path
    $timebombs | clip
    # find ftp in imp folder file:///Y:/Customer%20Accounts/Boise/Implementation/FTP%20at%20Boise%20City.txt
    $ftp = Get-ChildItem 'Y:\Customer Accounts\*' -file -Filter *.txt -r
    $ftp | ? {$_.name -match 'ftp'} | select fullname

    # find jobs that didn't run on 20161114
    $14outs = Get-ChildItem -Path '\\10.0.0.142\c$\inetpub\wwwroot\static\scheduler_logs' -r -filter *20161114*.out
    $14outs.name | clip
    $dirs = Get-ChildItem -Path '\\10.0.0.142\c$\inetpub\wwwroot\static\scheduler_logs' -Directory
    $dirs.name | Sort-Object name | clip

    get-content '\\sea-2020-63\prod\_scheduler_research_20161114\eliminated.md' | % {start-process http://wiki:8000/scheduler_logs/$_}
    get-content '\\sea-2020-63\prod\_scheduler_research_20161114\in-timeframe.md' | % {start-process http://wiki:8000/scheduler_logs/$_}

    # find MI15736 for ftpplus can't connect to fiserv
    $fiservscripts = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern "mi15736" |select Path


    # kodak canon 180 driver software for afs Directory: Y:\Temp\bill.fricke\Documents\Canon CR-180 Installation Code
    #    and Setup Doc\Canon CR-180 Installation Code and Setup Doc
    Get-ChildItem -Path y:\ -Directory -Recurse *SU180*
    # activation key?
    Get-ChildItem -Path \\itstore\ -file -Recurse *180*

    # find job rms4554.bat
    $4554iswhere = ls \\sea-r320-30\c$\24x7\Output -r | select-string -pattern 'rms4554.bat'

    # find the pc document script
    $wikipcscript =
    ls C:\backup\dev -r | select-string -pattern 'seattle.xml'

    # find vision ip

    $visionmatch = ls y:\ -r *visionip* | out-file c:\temp\visionip.md
    $visionmatch | select fullname

    # pic for exchange done had to do the snapin this is in dev/exchange now

    ls \\itstore\apps -filter *pic*.ps1 -r | select fullname
    ls \\mail1\c$ -filter *pic*.ps1 -r | select fullname

    # afsimport calls

    get-content -Path e:\rlbobj\afsimport.bat | Select-String -pattern '@call' | select line | clip
    get-content -Path e:\rlbobj\afsimport.bat | Select-String -pattern '(e):\\' | select line | clip

    # bootstrap css
    ls -Path C:\users\shane.null\flask-cookie -r | select-string -Pattern '@brand-primary' | select filename


    # random kodak crap

    #backups

    invoke-item C:\backup\core\jobs\kodakbackup.ps1
    invoke-item C:\backup\kodak\kodak.ps1

    # installs


    [xml]$netmap = Get-Content  'C:\backup\seattle.xml'
    $netmap.'network-scanner-result'.devices.item | ? {$_.apps -like '*KODAK Capture Pro Software*'} | select hostname


    #kodak apps backup one
    if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"}
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
    $source = '\\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps'
    $target = "E:\tools\Infrastructure\kodaks\backups\rlb221-scan007-apps.zip"
    sz a -mx=9 $target $source


    # backup function
    function backup-kodak($computer)
    {
    if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"}
    set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
    $source = "\\$computer\c$\kodak\xvcs6c\Apps"
    $target = "E:\tools\Infrastructure\kodaks\backups\$computer`-apps.zip"
    sz a -mx=9 $target $source
    }
    $computer ='rlb221-scan007.rli.local'
    $computer = 'rlb220-test001.rli.local'
    $computer = 'rlb220-scan003.rli.local'
    $computer = 'rlb220-index008.rli.local'
    backup-kodak($computer)

    #info

    \\rlb220-test001.rli.local\c$\kodak\xvcs6c\Apps
    \\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps
    \\rlb220-scan003.rli.local\c$\kodak\xvcs6c\Apps
    \\rlb220-index008.rli.local\c$\Kodak\xvcs6c\Apps

    $installs = get-content C:\backup\kodak\installs.md
    foreach($_ in $installs)
    {
    Get-ChildItem -Path $_
    }

    gci \\rlb220-test001.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-test001.rli.local.txt
    gci \\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb221-scan007.rli.local.txt
    gci \\rlb220-scan003.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-scan003.rli.local.txt
    gci \\rlb220-index008.rli.local\c$\Kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-index008.rli.local.txt
    gci \\rlb220-scan002.rli.local\C$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-scan002.rli.local.txt
    $ref = get-content 'E:\tools\Infrastructure\kodaks\rlb220-scan002.rli.local.txt'
    $1 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-index008.rli.local.txt'
    $2 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-scan003.rli.local.txt'
    $3 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-test001.rli.local.txt'
    $4 =  get-content 'E:\tools\Infrastructure\kodaks\rlb221-scan007.rli.local.txt'

    Compare-Object $ref $1
    Compare-Object $ref $2
    Compare-Object $ref $3
    Compare-Object $ref $4
    Compare-Object $ref $5

    # atom packages

    ls C:\Users\shane.null\.atom\packages -Directory | select name | clip

    #find c#
    ls \\rlb221-fs004\c$\ -filter *.cs -r

    # find this 3466 alert because they didn't think to include the script generating the email
    $noreadability = Get-ChildItem -Path  @("e:\rlbobj","e:\scripts") -Filter *.bat | Select-String -pattern 'ezpay' |select Path
    $noreadability | clip

    # fix apm

    Get-ItemProperty 'Registry::HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings' | Select-Object *Proxy*


    $proxies = (Get-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings').proxyServer
    [System.Net.WebProxy]::GetDefaultProxy() | select address

    $IESettings = Get-ItemProperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings'
    $Proxy = "http://$(($IESettings.ProxyServer.Split(';') | ? {$_ -match 'ttp='}) -replace '.*=')"
    Invoke-WebRequest 'http://some-site.com' -ProxyUseDefaultCredentials -Proxy $Proxy

    reg query "HKCU\software\microsoft\windows\currentversion\internet Settings" /v "ProxyServer"

    # find pdf cat

    $path = 'C:\Users\shane.null\Envs'
    ls $path -Filter pdfcat.py -r

    # find jills keepass
    ls e:\ -r -filter *.kbdx



# old junk


_model: page
---
title: powershell
---
body:

<https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-linux?view=powershell-6>

<https://kmyers.me/blog/chromeos/installing-microsoft-powershell-on-chrome-os-with-project-crostini/>

# powershell production snippets

## intro

```
get-verb
Get-Date | Get-Member -MemberType Method
Get-ChildItem Env:
```

```powershell
# show just your variables
function Get-UDVariable {
    get-variable | where-object {(@(
                "FormatEnumerationLimit",
                "MaximumAliasCount",
                "MaximumDriveCount",
                "MaximumErrorCount",
                "MaximumFunctionCount",
                "MaximumVariableCount",
                "PGHome",
                "PGSE",
                "PGUICulture",
                "PGVersionTable",
                "PROFILE",
                "PSSessionOption"
            ) -notcontains $_.name) -and `
        (([psobject].Assembly.GetType('System.Management.Automation.SpecialVariables').GetFields('NonPublic,Static') | Where-Object FieldType -eq ([string]) | ForEach-Object GetValue $null)) -notcontains $_.name
    }
}
```

## weird built ins

<https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_automatic_variables?view=powershell-6>




## making objects

```powershell
    $thisboxfolders = New-Object System.Collections.Generic.List[String]
    foreach($_ in $folders){
        Write-Host "checking $_"
        $workfolder = Get-ChildItem -Path $_ -Directory -Filter 5151
        if($workfolder){
            Write-Host $workfolder.FullName
            $thisboxfolders.Add($workfolder.fullname)
        }
    }
```

## make folders

```powershell
$folders = "build","docs","src","test" foreach($folder in $folders){ new-item -name "$folder" -Path "$yourFolder" -ItemType directory -Force $folder }

```

----

## folder + file crud

* [rosetta](http://rosettacode.org/wiki/File_input/output#PowerShell)

```
Get-Content $PWD\input.txt | Set-Content $PWD\output.txt
```


## naming

* [verbs](https://docs.microsoft.com/en-us/powershell/developer/cmdlet/approved-verbs-for-windows-powershell-commands)




----

> find stuff

powershell_app
$send = 'E:\rlbobj\sendJHEPSc21.bat'
$folder = 'E:\test_env\rli\e\sandbox\jheps\orig'
# backup orig
# Copy-Item -Path $send $folder

# time bug

```
$timebugs = Get-ChildItem -Path @("e:\rlbobj", "e:\scripts") -filter *.bat | Select-String -pattern '%timestamp'| select Path
$timebugs | Set-Clipboard
```




> hearbeat 

```powershell
function update-heartbeat {
    Write-Host "-----------"
    write-host ("({0}) start" -f $MyInvocation.MyCommand)
    # do stuff
    E:\rlbauto\HeartBeat\HB.HeartBeat.exe -token ShaneTest
    write-host ("({0}) end" -f $MyInvocation.MyCommand)
    Write-Host "-----------"
}
```

> make folders

```
$folders = "build","docs","src","test"
foreach($folder in $folders){
    new-item -name "$folder"  -Path "$yourFolder" -ItemType directory -Force
        $folder
}
```
* [making objects](https://kevinmarquette.github.io/2016-10-28-powershell-everything-you-wanted-to-know-about-pscustomobject/)

```powershell
$myObject = [PSCustomObject]@{
    Name     = 'Kevin'
    Language = 'Powershell'
    State    = 'Texas'
}
#
$myHashtable = @{
    Name     = 'Kevin'
    Language = 'Powershell'
    State    = 'Texas'
}

$myObject = New-Object -TypeName PSObject -Property $myHashtable

# 

$myObject | Add-Member -MemberType NoteProperty -Name `ID` -Value 'KevinMarquette'

$myObject.ID
```

> catch

<https://stackoverflow.com/questions/38419325/catching-full-exception-message/38421525>

> news junkie

    $uri = 'http://wiki:8000/automation/news/newsurls.csv'
    $gist = Invoke-RestMethod -Uri $uri
    $gist.split(",") | % {start-process $_}


    # run from a local list
    get-content C:\backup\dev\gist\links.md | % {start-process $_}
    # remote 
    # $news3 = 'https://gist.githubusercontent.com/shane0/193166ba211be5ce9d9f5546de391edc/raw/d59eef7ef1256c831f7b72e6d30980dab8543093/open-news-local-list.ps1'
    # [System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials; iex ((New-Object System.Net.WebClient).DownloadString($news3))

    start-process "https://www.google.com/search?q=start+a+timer+for+13+minutes&oq=start+a+timer&aqs=chrome.0.69i59j69i57.1988j0j7&sourceid=chrome&es_sm=93&ie=UTF-8"

    # download your news list and launch the urls
    $uri = 'https://raw.githubusercontent.com/shane0/news-junkie/master/src/newsurls.csv' # edit this to point to your own file
    $gist = Invoke-RestMethod -Uri $uri
    $gist.split(",") | % {start-process $_}

    # optional if you want to launch from a local file
    #get-content C:\newsurls.csv | % {start-process $_}

    # optional if you have proxy use this first
    #[System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials

    # one liner powershell v3 command
    #iwr 'https://raw.githubusercontent.com/shane0/news-junkie/master/src/git-news.ps1' | iex

    # another way
    #iex ((New-Object System.Net.WebClient).DownloadString('https://raw.githubusercontent.com/shane0/news-junkie/master/src/git-news.ps1'))

    # run from windows shell, not working for me yet
    #@powershell -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://raw.githubusercontent.com/shane0/news-junkie/master/src/git-news.ps1'))"

    # @powershell -NoProfile -ExecutionPolicy Bypass -Command  [System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials; iex ((New-Object System.Net.WebClient).DownloadString('https://raw.githubusercontent.com/shane0/news-junkie/master/src/git-news.ps1'))


> capture output 

<https://stackoverflow.com/questions/8423368/how-to-capture-output-in-a-variable-rather-than-a-logfile>

    $cmdOutput = &"Application.exe" 2>&1
    
    2>&1 - Includes the error stream in the output

    or

    $cmdOutput = &"Application.exe" | Out-String
    Write-Host $cmdOutput

> regex
<https://regex101.com/r/lQ7yZ7/1>

> here string expansions

    $myDate = Get-Date
    @"
    Hello world! ${myDate}
    '(this will appear as is)'
    !
    "@

> env var

    Invoke-Command -ComputerName $Server -ScriptBlock { ... }

    also on older version of windows comp mgmt remotely lets you right-click properties


> win ver

<https://stackoverflow.com/questions/7330187/how-to-find-the-windows-version-from-the-powershell-command-line>

    [Environment]::OSVersion

    [Environment]::OSVersion.Version -ge (new-object 'Version' 6,1)

    (Get-WmiObject -class Win32_OperatingSystem).Caption




> get var

    Get-Variable -Scope 0 >> localvar.txt
    Get-Variable -Scope 0 rep*

test if var def

    Test-Path variable:global:foo

> params w/ counts

#Set variables from arguments
$emailFrom=$args[0]
$emailTo= @($args[1].split(","))
$emailSubject=$args[2]
#Replaces "\n" from email body with carriage return and new line.
$emailBody=$args[3] -replace ("\\n", "`r`n")
$emailAttachment= @()

#Takes every argument starting from the 5th one and adds it to email attachment variable.
if($args.count -ge 5)
{
	$attachmentCount = 5
	do
	{
		$emailAttachment+=$args[$attachmentCount-1]
		$attachmentCount++
	}
	while ($attachmentCount -le $args.count)
}

> another run type

    $arg1 = "xxx"
    $arg2 = "$yyMMdd"
    $arg3 = "3576"
    $allArgs = @($arg1, $arg2, $arg3)
    $batch
    $CommandLine = "& $batch $allArgs"
    write-host "running $CommandLine"
    Invoke-Expression $CommandLine
    write-host "sleeping for 10 seconds"

> array to object for csv exporting

    $preparrayforcsv = @()
    $gooddates | ForEach-Object {
        $preparrayforcsv += [PSCustomObject]@{
            ColumnName = $_
        }
    }
    $preparrayforcsv | export-csv -path c:\array.csv -notypeinformation

# find crap

> fine ftp files with name ___

    # cathy w/ seafreeze chase key for 3528
    # no scripts with that name, checking ftp files
    # none

    $search = '\\rlbftp2\FTPSHARE\'
    $matches = Get-ChildItem -Path $search -Filter *3528* -Recurse | select fullname
    $matches | Out-File c:\temp\wtfudge\3528.txt

> find folder that got moved

people keep moving folders in ftp 2 wtfudge

    Get-ChildItem -Path \\rlbftp2\ftpshare\ -Directory -filter 1666-PierceCoSewer

> match reg

    $epayFile = Get-ChildItem $boxfolder | Where-Object {$_.Name -match "prod.RetailLockbox.remit.\d{14}"}

> email multiple in param

    $emailto = @("imp@retaillockbox.com", "support@retaillockbox.com", "rlockbox@retaillockbox.com")

    To         = $emailto

> 2018-07-12

delete microsoft crap

    Get-ChildItem -Path '\\hostname\c$\Windows\Temp\' -Filter cab* | ForEach-Object {$_.FullName} | Remove-Item

> search

    function Search-pattern
    {
    <#
        .SYNOPSIS
        Short Description
        .DESCRIPTION
        Detailed Description
        .EXAMPLE
        Search-pattern
        explains how to use the command
        can be multiple lines
        .EXAMPLE
        Search-pattern
        another example
        can have as many examples as you like
    #>
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory=$false, Position=0)]
        [System.String]
        $searchpath,
        
        [Parameter(Mandatory=$false, Position=1)]
        [System.String]
        $filter,
        
        [Parameter(Mandatory=$false, Position=2)]
        [System.String]
        $pattern
    )
    
    $matches = Get-ChildItem -Path $searchpath  -Filter $filter -Recurse | Select-String -pattern $pattern | Select-Object -ExpandProperty Path
    return $matches
    }

    $found = search-pattern -searchpath 'e:\scripts' -filter "*.ps1" -pattern "validate"
    $found


> count

	$tifcount = (Get-ChildItem $index -Recurse -Include *.tif | measure-object).count



> search folders with this and without that

`$dayswithtiff = Get-ChildItem 'L:\images' -Directory | Where-Object -FilterScript { $_.Name -match "^[0-9][0-9][0-1][1-2][0][1-4]$" } | ForEach-Object { Get-ChildItem $_.FullName } | Where-Object { (Get-ChildItem $_.FullName -Filter *.sent).count -eq 0 } | Where-Object { (Get-ChildItem $_.FullName -Filter *.tif).count -ne 0 }
`

# screenshots stuff

<pre class="line-numbers">
<code class="language-powershell">


# updates screenshots
start-process http://wiki:8000/screenshots/screenshots.html
invoke-item '\\10.0.0.142\c$\inetpub\wwwroot\static\screenshots\screenshots.ps1'
copy-withisoname "\\10.0.0.142\c$\inetpub\wwwroot\static\screenshots\screenshotdata.html"
Clear-Content "\\10.0.0.142\c$\inetpub\wwwroot\static\screenshots\screenshotdata.html"

$htmlbit = $null

$imagefiles = ls '\\10.0.0.142\c$\inetpub\wwwroot\static\screenshots\*' -Include *.png,*.jpg | select -ExpandProperty name
$imagefiles | % {
$htmlbit += "<a href=`"$_`" class=`"tip`" data-mode=`"top`" data-tip=`"$_`" ><p>$_</p><img alt=`"$_`" width=`"60%`" src=`"$_`"/></a>"
}
$htmlbit | ac "\\10.0.0.142\c$\inetpub\wwwroot\static\screenshots\screenshotdata.html"

# 2016-07-29 rename created

ls '\\10.0.0.142\c$\inetpub\wwwroot\static\screenshots\*' -Include *.png,*.jpg | Rename-Item -newname {$_.CreationTime.toString("yyyy.MM.dd.HH.mm") + ".jpg"}

ls c:\backup\tools\* -Include *.png,*.jpg -r | select fullname | clip
</code>
</pre>

> 2018-06-19

ftp folder moved accidentally

    $ftp = '\\rlbftp2\ftpshare'
    $waldo = '1013-tacoma'

    function findwaldodir($root){

    $dirs = Get-ChildItem -path $root -Directory 

    return $dirs
    }

    $dirlist = findwaldodir -root $ftp
    #$dirlist | select *
    $morefolder = $dirlist | ForEach-Object {Get-ChildItem -Directory -path $_.fullname} | select fullname
    Set-Clipboard $morefolder.fullname
# from ssh

<https://gist.github.com/thinkerbot/1121114>

run.bat

    # Notes:
    # 1) The input format must be specified to prevent ssh from hanging
    # 2) The full path to the script is required
    # 3) An SSH server must be running on the Windows box, naturally.  WinSSHD
    #    is free for personal, non-commercial use and worked for this example.
    #    See http://www.bitvise.com/winsshd
    #
    # You have to set the execution policy for scripts so that powershell has
    # permissions to run the script.  To do so, search for 'Powershell' under
    # the Start menu.  Then right-click and 'Run as administrator' so that you
    # have permissions to set permissions.  Then:
    #
    #   PS> Set-ExecutionPolicy RemoteSigned
    #
    # As a result you can run scripts you write but not unsigned ones from 
    # remote sources.  See http://technet.microsoft.com/en-us/library/ee176949.aspx
    #
    # To execute via ssh:
    #
    #   $ ssh user@host '"C:\path\to\run.bat"'
    #
    # Note the inner quotes are superfluous here but required if the path has
    # a space in it.
    powershell -InputFormat None -F "C:\script.ps1"

script.ps1

    # As per the bat file, put this at "C:\script.ps1"
    echo "hello world" 


# run once scheduled

`$t = New-ScheduledTaskTrigger -Once -At (get-date).AddSeconds(10); $t.EndBoundary = (get-date).AddSeconds(60).ToString('s'); Register-ScheduledTask -Force -TaskName JustTrying -Action (New-ScheduledTaskAction -Execute "powershell.exe" -Argument '-NoProfile -WindowStyle Hidden -command "& {dir}"') -Trigger $t -Settings (New-ScheduledTaskSettingsSet -DeleteExpiredTaskAfter 00:00:01)`

# dl and run

`(new-object net.webclient).DownloadFile('https://gist.githubusercontent.com/AndrewSav/c4fb71ae1b379901ad90/raw/23f2d8d5fb8c9c50342ac431cc0360ce44465308/SO33205298','local.ps1')`

`./local.ps1 "parameter title"`


that example

    param([string]$title)
    "Called with parameter: $title" | Write-Output

----

# from cmd or gist

`@powershell -NoProfile -ExecutionPolicy Bypass -Command  [System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials; iex ((New-Object System.Net.WebClient).DownloadString('https://gist.githubusercontent.com/jamiekt/1dbd0c3350bc32e04705/raw/9deed202217961ad8c19ab7f1ca4774d8f243775/profile.ps1'))`


# sign it

profile.ps1

    <#
    To run this every time you open a Powershell session pop this:
        https://gist.githubusercontent.com/jamiekt/1dbd0c3350bc32e04705/raw/9deed202217961ad8c19ab7f1ca4774d8f243775/profile.ps1
    into your Powershell profile
    #>
    (new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex

    "Installing PSReadLine"
    install-module PsReadLine

    function prompt
    {
        #http://sqlblog.com/blogs/jamie_thomson/archive/2014/08/26/time-your-commands-powershell-nugget.aspx for an explanation
        $host.ui.rawui.WindowTitle = (get-location)
        $lastCommand = Get-History -Count 1
        if($lastCommand) { Write-Host ("last command took") ($lastCommand.EndExecutionTime - $lastCommand.StartExecutionTime).TotalMilliseconds ("ms") }
        return ">"
    }

    # SIG # Begin signature block
    # MIIFuQYJKoZIhvcNAQcCoIIFqjCCBaYCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
    # gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
    # AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUsNvd36T6ApH1jQhdzFEXUvDK
    # vmygggNCMIIDPjCCAiqgAwIBAgIQbiDO4joXcptNrzC4V9pXUDAJBgUrDgMCHQUA
    # MCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9jYWwgQ2VydGlmaWNhdGUgUm9vdDAe
    # Fw0xNDEyMDEyMjI4MDlaFw0zOTEyMzEyMzU5NTlaMBoxGDAWBgNVBAMTD1Bvd2Vy
    # U2hlbGwgVXNlcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMWjFPCV
    # uw6kRbdm7kepxEdnAzK9vPfYCMvKPFsjbSwK55/uFFNbhcsXQ7bsyHmHCqSrewfI
    # csTK/IXo6rNAZSmddZM042yTuopN+21CPa/1sEoG+ivL1+9x8O8rEWpGABxit3vz
    # aiybvE0RtaPcvbo7n5tCKUxtjexxwciW/nESKhZzt/1yPW4jsDdeiiGgHTmnA0OD
    # J/gRi49v3XGv81ecEoPD58lZRpWpHJ3p3fnobtFa4niJHQnLQ+dsIR/paCRH4yGH
    # HNvyfv7QTOszkyu46mnzJvTWe48BVQ2HY91DCqnvF7c7NX5miTqHTpz9m26Sgvcm
    # pV4ERmbziz8PD5sCAwEAAaN2MHQwEwYDVR0lBAwwCgYIKwYBBQUHAwMwXQYDVR0B
    # BFYwVIAQoPTcdg6emi+5mzSB6H1U+aEuMCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwg
    # TG9jYWwgQ2VydGlmaWNhdGUgUm9vdIIQZ/H43bdpCZlAXs9v0hGSmTAJBgUrDgMC
    # HQUAA4IBAQBN55ZhDuGOpDmypbeaHNaCAySR7Dv8aKRWZbYEzwCF4fSSKVC59alw
    # /9Y9o0DoyNvVdb1ORmA72XST5ksrCsThf4875PXkF4FpVENX/hnLP5WIOBEO07R6
    # +OuB8b84jOig1yLBMuVhvzzid48fu8/+fYnBS04rK1BHbm+5P7Pc/Oz+V0YP2LHH
    # ao5GBkr9LCAI2jltOJS/JFXRz3ttwxN9NNnVY7QXGB8OuqfZ563KjxAI/oCexUU7
    # XjQMHLHKGOQBW+Eiii8ayrX0OUVCcx7vSNDxBflbLofsAGdda54vPt/alv/G9oDW
    # CzKP0F5hBivBXjRbNRcco0+2YltFsjsEMYIB4TCCAd0CAQEwQDAsMSowKAYDVQQD
    # EyFQb3dlclNoZWxsIExvY2FsIENlcnRpZmljYXRlIFJvb3QCEG4gzuI6F3KbTa8w
    # uFfaV1AwCQYFKw4DAhoFAKB4MBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJ
    # KoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQB
    # gjcCARUwIwYJKoZIhvcNAQkEMRYEFMen7MIU9tW7nyIXVQtgE26t2CDQMA0GCSqG
    # SIb3DQEBAQUABIIBAAk51/C8anQP6IT3mto7QaFNWXkPrFHcEWaZ2E8nS0H5M4gP
    # eWL0OrCIhOdHrqNx5S6/aHhIwRBWR8QOdIbi6cGvGj5lj3eSBxalsburnt1C4W/b
    # XK4HFNYXKT34JiJjhIE7SgM6pUJBHTEXgsUq01Q2rWrc3KdJWAx1ghPdEHldjl6l
    # hFvKV7RQur88Pb2M9yDvEaC6lXQW/xVhySYPwCn/9n8lPwGwsz+5o/mIDfWynABl
    # GUQ3V5EJnWqzP9XMjqnYz2MPAQN0RjVJYSuGDB+EY0/8CNpJhar/rGFZ/Z5H3bpg
    # 06cLfN3//UOkPXXvrn/0bq5465vIELozN3ulXBo=
    # SIG # End signature block


# profile.ps1

    <#
    To download an up-to-date profile every time you open Powershell pop the code below into your Powershell profile. 
    The script that it downloads and executes can be seen at https://gist.github.com/jamiekt/1dbd0c3350bc32e04705#file-powershellprofile-ps1

    To edit your Powershell profile enter the following into Powershell:
        if(-not (test-path $profile)) {new-item $profile -Type File -Force}; notepad $profile
    #>
    $clnt = new-object System.Net.WebClient
    $url = "https://gist.githubusercontent.com/jamiekt/1dbd0c3350bc32e04705/raw/3a8a40eb0512c39da24920ea9f58d10f694a21a7/powershellprofile.ps1"
    $folder = "c:\temp"
    mkdir $folder -force
    $infile = "$folder\inprofile.ps1"
    $outfile = "$folder\outprofile.ps1"
    $clnt.DownloadFile($url,$infile)
    (gc $infile) | %{$_.split("`n")} | Out-File $outfile #replace LFs with CRLFs
    . $outfile
    # SIG # Begin signature block
    # MIIFuQYJKoZIhvcNAQcCoIIFqjCCBaYCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
    # gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
    # AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUNlyklVEQEih8OTREGVehQzVB
    # pYagggNCMIIDPjCCAiqgAwIBAgIQbiDO4joXcptNrzC4V9pXUDAJBgUrDgMCHQUA
    # MCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwgTG9jYWwgQ2VydGlmaWNhdGUgUm9vdDAe
    # Fw0xNDEyMDEyMjI4MDlaFw0zOTEyMzEyMzU5NTlaMBoxGDAWBgNVBAMTD1Bvd2Vy
    # U2hlbGwgVXNlcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMWjFPCV
    # uw6kRbdm7kepxEdnAzK9vPfYCMvKPFsjbSwK55/uFFNbhcsXQ7bsyHmHCqSrewfI
    # csTK/IXo6rNAZSmddZM042yTuopN+21CPa/1sEoG+ivL1+9x8O8rEWpGABxit3vz
    # aiybvE0RtaPcvbo7n5tCKUxtjexxwciW/nESKhZzt/1yPW4jsDdeiiGgHTmnA0OD
    # J/gRi49v3XGv81ecEoPD58lZRpWpHJ3p3fnobtFa4niJHQnLQ+dsIR/paCRH4yGH
    # HNvyfv7QTOszkyu46mnzJvTWe48BVQ2HY91DCqnvF7c7NX5miTqHTpz9m26Sgvcm
    # pV4ERmbziz8PD5sCAwEAAaN2MHQwEwYDVR0lBAwwCgYIKwYBBQUHAwMwXQYDVR0B
    # BFYwVIAQoPTcdg6emi+5mzSB6H1U+aEuMCwxKjAoBgNVBAMTIVBvd2VyU2hlbGwg
    # TG9jYWwgQ2VydGlmaWNhdGUgUm9vdIIQZ/H43bdpCZlAXs9v0hGSmTAJBgUrDgMC
    # HQUAA4IBAQBN55ZhDuGOpDmypbeaHNaCAySR7Dv8aKRWZbYEzwCF4fSSKVC59alw
    # /9Y9o0DoyNvVdb1ORmA72XST5ksrCsThf4875PXkF4FpVENX/hnLP5WIOBEO07R6
    # +OuB8b84jOig1yLBMuVhvzzid48fu8/+fYnBS04rK1BHbm+5P7Pc/Oz+V0YP2LHH
    # ao5GBkr9LCAI2jltOJS/JFXRz3ttwxN9NNnVY7QXGB8OuqfZ563KjxAI/oCexUU7
    # XjQMHLHKGOQBW+Eiii8ayrX0OUVCcx7vSNDxBflbLofsAGdda54vPt/alv/G9oDW
    # CzKP0F5hBivBXjRbNRcco0+2YltFsjsEMYIB4TCCAd0CAQEwQDAsMSowKAYDVQQD
    # EyFQb3dlclNoZWxsIExvY2FsIENlcnRpZmljYXRlIFJvb3QCEG4gzuI6F3KbTa8w
    # uFfaV1AwCQYFKw4DAhoFAKB4MBgGCisGAQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJ
    # KoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQB
    # gjcCARUwIwYJKoZIhvcNAQkEMRYEFI4v913peXnwvwU/z0dHPsfREYFEMA0GCSqG
    # SIb3DQEBAQUABIIBAEsfKPE3HHRISWlXglVDxveGSXKkDswTo8QcnRat3HY0+lli
    # /RcL0DSNQIPDu3F/aCYejhL49D8wE9bld0o5YBHSzM6gt/m9Qy4i+WA+4BsXpDpL
    # YxMWjKe4J1Rh+6mNRTEUoKZktzHUxgxWRvN41a6XtQcQtFqsPbLa8D3l6smR9zlw
    # oE4kGh0QqNhBmSbaopkf8brCjmBLuSnvFIxs9dlosgYe0uCr8B7BoWlnB9PjkMai
    # cZcyckcyUnyfMfIHrUfSGARB9VKjAhT9M4pDmM0oNhuMmScdTWkmqeVpKUyTAF8q
    # rpE2OnyMXcXaPmf2nJyAzw9mqlWZoBUW2FTagNA=
    # SIG # End signature block

# sign.bat

    powershell.exe -NoProfile -command "$certs = Get-ChildItem cert:\CurrentUser\My -codesign; if ($certs -eq $null ){ exit 1};"
    echo. %ERRORLEVEL%
    if %ERRORLEVEL% == 0 goto SIGNSCRIPTS

    PUSHD C:\Windows\system32
    makecert -n "CN=PowerShell Local Certificate Root" -a sha1 -eku 1.3.6.1.5.5.7.3.3 -r -sv root.pvk root.cer -ss Root -sr localMachine
    makecert -pe -n "CN=PowerShell User" -ss MY -a sha1 -eku 1.3.6.1.5.5.7.3.3 -iv root.pvk -ic root.cer
    POPD

    :SIGNSCRIPTS
    powershell.exe -NoProfile -command "Get-ChildItem .\ -Recurse | Where-Object {$_.Extension -eq '.ps1'} | foreach { Set-AuthenticodeSignature $_.Fullname @(Get-ChildItem cert:\CurrentUser\My -codesign)[0]}"
    powershell.exe -NoProfile -command "Get-ChildItem .\ -Recurse | Where-Object {$_.Extension -eq '.psm1'} | foreach { Set-AuthenticodeSignature $_.Fullname @(Get-ChildItem cert:\CurrentUser\My -codesign)[0]}"

----


<pre class="line-numbers">
<code class="language-sql">

</code>
</pre>

<pre class="line-numbers">
<code class="language-powershell">

# ls e:\180129 -r >> C:\check\2\results.txt

ls e:\ -r >> C:\check\2\results2.txt

# $MyVariable = Get-ChildItem e:\ -directory -Recurse | Where-Object {$_.name -like "180129"} -Directory | % { $_.fullname }
# $MyVariable >> C:\check\2\result3.txt
# Get-ChildItem e:\ -directory -Recurse | Where-Object {$_.name -like "180129"} | select >> C:\check\2\result3.txt
Get-ChildItem e:\ -directory -Recurse | Where-Object {$_.name -like "180129"} | select FullName



ls e:\ -directory  -filter 180129 -Recurse | select fullname


</code>
</pre>
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2018-01-31 10:49
           
        # write download filenames to scheduler output 



        $yymmdd = Get-Date -Format yyMMdd

        $workfolder = "e:\" + $yymmdd + "\"

        $box = 3602

        $localrmsfolder = $workfolder + $box + "\RMS\"

        $filecheck = Get-ChildItem $localrmsfolder 

        foreach($_ in $filecheck){

            Write-Host $_.fullname

        }

# checking for files and alerting


        $collection = (Get-ChildItem -path $path -recurse | Where { $_.Name.ToString().Contains($formatDate + "4470") -and $_.Name.ToString().Length -eq 16 }) | Get-ChildItem | Where { $_.Name.Contains(".tif") }

        if (-not($collection.Count -gt 0)) {
            Write-Host "There no were files"
            powershell "\\sea-2020-63\prod\scripts\email\sendemail.ps1 'noreply@retaillockbox.com' 'nick@retaillockbox.com' 'Operations Alert - Batch listing image files scanned under 4470-ESDICESA not found: $formatdate' 'Supervisors,\n\nNo batch listing image files scanned under 4470-ESDICESA were found at the required time.\n\nPlease verify there was no work.'"
        } ELSE
        {
            Write-Host "There were files"
        }

icon clickable for noobs

`cmd /k E:\scripts\homestreet\manual_send.bat`

        @setlocal EnableDelayedExpansion
        @ECHO -----------------------------------------------
        @ECHO " ______                  _
        @ECHO "(_____ \             _  (_)
        @ECHO " _____) )___   ___ _| |_ _ ____   ____
        @ECHO "|  ____// _ \ /___|_   _) |  _ \ / _  |
        @ECHO "| |    | |_| |___ | | |_| | | | ( (_| |
        @ECHO "|_|     \___/(___/   \__)_|_| |_|\___ |
        @ECHO "                                (_____|
        @ECHO.
        @ECHO %DATE% %TIME%
        @ECHO.

        @REM Tokenize current date.
        @for /f "tokens=1,2,3,4 delims=/ " %%a in ( 'date/t' ) do @set x=%%d
        @for /f "tokens=1,2,3,4 delims=/ " %%a in ( 'date/t' ) do @set m=%%b
        @for /f "tokens=1,2,3,4 delims=/ " %%a in ( 'date/t' ) do @set d=%%c

        @REM Passes argument to set YYYYMMDD variable or uses current date if blank
        @if "%1"=="" (
            @set YYYYMMDD=%x%%m%%d%
        ) else (
            @set YYYYMMDD=%1
        )

        @SET YYMMDD=%YYYYMMDD:~2%

        @SET BOX=4612
        @SET WORKPATH=e:\%YYMMDD%\%BOX%
        @SET POSTINGFILE=FIL%BOX%.dat
        @SET OUTPOSTINGFILE=L0213181
        @SET EPAYSENTINEL=EPAY.SENT
        @set WINSCP_RESULT=""
        @copy %WORKPATH%\%POSTINGFILE% %WORKPATH%\%OUTPOSTINGFILE%

        @SET LOGFILE=%WORKPATH%\posting_log.txt

        @ECHO Verifying required files exist

        @IF NOT EXIST %WORKPATH%\%EPAYSENTINEL% (
            @ECHO # Missing EPAY sentinel file %WORKPATH%\%EPAYSENTINEL%
            @ECHO ABORT
            @ECHO Rerun the EPAY job
            @GOTO :EOF
        ) ELSE (
            @ECHO - EPAY Sentinel file found
        )

        @if not exist %WORKPATH%\%POSTINGFILE% (
            @ECHO # Missing posting file.
            @ECHO ABORT - %WORKPATH%\%POSTINGFILE% does not exist.
            @GOTO :EOF
        ) ELSE (
            @ECHO - Posting file found
        )

        @if not exist %WORKPATH%\POSTING.SENT (
            @ECHO Sending filename %OUTPOSTINGFILE%
            @call :Logit
            @echo posting file SENT
        @echo you can now close this window

            @GOTO :EOF
        ) else (
            @echo "Skipping posting file send, delete %WORKPATH%\POSTING.SENT if you want to resend posting file"
            @GOTO :EOF
        )
        @GOTO :EOF

        :Logit
        @ECHO Running %0 >>%LOGFILE%
        @ECHO %DATE% %TIME% >>%LOGFILE%
        @ECHO For BOX:%BOX% and Processing Date:%YYYYMMDD% >>%LOGFILE%
        @ECHO Executed as User: %USERNAME% >>%LOGFILE%
        @ECHO Executed from Computer: %COMPUTERNAME% >>%LOGFILE%

        "C:\Program Files (x86)\WinSCP\WinSCP.com" ^
        /log="%WORKPATH%\PostingWinSCP.log" /ini=nul ^
        /command ^
            "open ftpes://blah/  -hostkey=*" ^
            "cd blah" ^
            "put %WORKPATH%\%POSTINGFILE% %OUTPOSTINGFILE%" ^
            "exit"


        @set WINSCP_RESULT=%ERRORLEVEL%

        @if %WINSCP_RESULT% equ 0 (

            @ECHO "Done at %DATE% %TIME%" >>%WORKPATH%\POSTING.SENT
            @ECHO "%WINSCP_RESULT%" >>%WORKPATH%\POSTING.SENT    
            ) else (
            @echo winscp failed or timed out - contact IT
        )

        @GOTO :EOF
        @endlocal

# get functions

        $pre = Get-Command
        . c:\flowstate\work\web\flask_sites\skeleton\projects\falcon\task-2018-02-07\test\refactor.ps1
        $post = Get-Command
        Compare-Object -ReferenceObject $pre -DifferenceObject $post

# scheduler intervals

        06:10,06:20,06:30,06:40,06:50,07:10,07:20,07:30,07:40,07:50,08:10,08:20,08:30,08:40,08:50,09:10,09:20,09:30,09:40,09:50,10:10,10:20,10:30,10:40,10:50,11:10,11:20,11:30,11:40,11:50,12:10,12:20,12:30,12:40,12:50,13:10,13:20,13:30,13:40,13:50,14:10,14:20,14:30,14:40,14:50,15:10,15:20,15:30,15:40,15:50,16:10,16:20,16:30,16:40,16:50,17:10,17:20,17:30,17:40,17:50,18:10,18:20,18:30,18:40,18:50
        

# rename extensions


<pre class="line-numbers">
<code class="language-powershell">
ls c:\flowstate\work\web\flask_sites\skeleton\projects\cli_p\python\3\ -Filter *.md -Recurse 

Get-ChildItem c:\flowstate\work\web\flask_sites\skeleton\projects\*.md -Recurse 

# | rename-item -newname { [io.path]::ChangeExtension($_.name, "c") }
</code>
</pre>


collect standard templates here

<pre class="line-numbers">
<code class="language-batch">
code -n c:\flowstate\work\web\flask_sites\skeleton\projects\powershell
</code>
</pre>



> 2017-12-13

alert for remote ftp file missing

<pre class="line-numbers">
<code class="language-batch">
@ECHO Running %0

@SET SCRIPT=E:\scripts\4242\achalert\4242alert.ps1
@powershell -NoProfile -ExecutionPolicy Bypass %SCRIPT% 
</code>
</pre>


<pre class="line-numbers">
<code class="language-powershell">
# petrocard - old cyclone job on colo-win7 that uploads ach/htm files fails frequently so this emails helpdesk if the files aren't there
# if you get this alert, either run that job again or upload these files to paystation ftp /petrocard

# \\colo-win7\c$\RLMS\170525\00016\170525_00016_ACH.dat
# \\colo-win7\c$\RLMS\170525\00016\170525_00016_ACH.htm

$yymmdd = get-date -Format yyMMdd


$value = "$yymmdd`_00016*"
$matchfile = $value
write-host "matchfile is $matchfile"


try
{
    # Load WinSCP .NET assembly
    Set-Location C:\winscp
    Add-Type -Path "WinSCPnet.dll"
    
    # Setup session options
    $sessionOptions = New-Object WinSCP.SessionOptions -Property @{
        Protocol = [WinSCP.Protocol]::Sftp
        HostName = "sftp2.paystation.com"
        UserName = "username"
        Password = "nope"
        SshHostKeyFingerprint = ""
    }
 
    $session = New-Object WinSCP.Session
 
    try
    {
        # Connect
        $session.Open($sessionOptions)
 
# Get list of files in the directory
        $directoryInfo = $session.ListDirectory("/")
    
        # Select files matching matchfile
        $files = $directoryInfo.Files | Where-Object { $_.Name -Like $matchfile }
        # Any file matched?
        if ($files)
        {
            foreach ($fileInfo in $files)
            {
                $content = ($fileInfo.Name)
                    write-host $content 
            }
        }
        else
        {
            Write-Host ("No files matching {0} found" -f $matchfile)
            write-host "sending email alert"
                $param = @{
                    SmtpServer = '10.0.0.12'
                    From = 'SUPPORT@retaillockbox.com'
                    To = 'helpdesk@retaillockbox.com'
                    Subject = "URGENT 4242 Petrocard ach job failed"
                    Body = "<html><body>Petrocard ach files didn't upload to paystation from colo-win7<br></body></html>"
                    BodyAsHtml = $true
                }
                Send-MailMessage @param
                write-host "files missing"
        }
    }
    finally
    {
        # Disconnect, clean up
        $session.Dispose()
    }
 
    exit 0
}
catch [Exception]
{
    Write-Host ("Error: {0}" -f $_.Exception.Message)
    exit 1
}
Stop-Process -id $pid -Force
Exit
</code>
</pre>


> 2017-11-16

found an old example

<pre class="line-numbers">
<code class="language-batch">
function do-stuff {
  <#
  .SYNOPSIS
  .EXAMPLE
  #>
  [CmdletBinding()]
  param
  (
     [Parameter(Mandatory=$true,HelpMessage='to pass date use yymmdd',ValueFromPipeline=$True)]
     [ValidatePattern('(?# date uses yymmdd)^[0-9][0-9]([0][1-9]|[1][1-2])[0-3][0-9]$')]
     [String] $yymmdd
  )
  begin {
    #   setup stuff
  }
  process {
    #  do stuff
  }
  end {
    #   test worked check error output alerts emails
  }
}
</code>
</pre>

> 2017-11-14

templates coming from rms project

----

<http://wiki/doku.php?id=it:appsupport:examples:snippets:snippets>


# logging

    :::powershell
    function Get-ScriptFull
    {
      Split-Path $script:MyInvocation.MyCommand.Definition
      $MyInvocation.ScriptName.Replace((Split-Path $MyInvocation.ScriptName),'').TrimStart('')
    }
    $scriptrunning = Get-ScriptFull
    write-host "running $scriptrunning"


# modules


<http://wiki/doku.php?id=it:appsupport:start>

new-customer (powershell modules)
new-ticketfolder (powershell modules)
new-dwcustomer (itdoc)
copy-withisoname
dcheck
update-schedulers
get-tukftp
get-jhftp
check-petro

get-modules | select exportedcommands | clip

    ExportedCommands
-    ----------------
    {[compress-folder, compress-folder], [new-backup, new-backup]}
    {[ConvertTo-Pdf, ConvertTo-Pdf]}
    {[ConvertTo-Tif, ConvertTo-Tif]}
    {[check-petro, check-petro], [check-petro2, check-petro2], [dcheck, dcheck],...
    {[new-dticketfolder, new-dticketfolder], [new-ticketfolder, new-ticketfolder]}
    {[Get-IseSnippet, Get-IseSnippet], [Import-IseSnippet, Import-IseSnippet], [...
    {[Add-Computer, Add-Computer], [Add-Content, Add-Content], [Checkpoint-Compu...
    {[ConvertFrom-SecureString, ConvertFrom-SecureString], [ConvertTo-SecureStri...
    {[Add-Member, Add-Member], [Add-Type, Add-Type], [Clear-Variable, Clear-Vari...
    {[Connect-WSMan, Connect-WSMan], [Disable-WSManCredSSP, Disable-WSManCredSSP...
    {[New-Customer, New-Customer], [New-dwCustomer, New-dwCustomer]}

> module snippet

    :::powershell
    function do-stuff {
      <#
      .SYNOPSIS
      .EXAMPLE
      #>
      [CmdletBinding()]
      param
      (
         [Parameter(Mandatory=$true,HelpMessage='to pass date use yymmdd',ValueFromPipeline=$True)]
         [ValidatePattern('(?# date uses yymmdd)^[0-9][0-9]([0][1-9]|[1][1-2])[0-3][0-9]$')]
         [String] $yymmdd
      )
      begin {
        #   setup stuff
      }
      process {
        #  do stuff
      }
      end {
        #   test worked check error output alerts emails
      }
    }

## general

### call exe with parameters

> options

<https://gist.github.com/magnetikonline/f880b0cb063c54568ade7073f935bbd1>

    :::powershell
    Set-StrictMode -Version Latest

    $cmdPath = "$PSScriptRoot\EchoArgs.exe"
    $cmdArgList = @(
    	"-switch", `
    	"-key1","value",
    	"-key2","value with spaces"
    	"/D","/S"
    	"another argument with spaces"
    )

    & $cmdPath $cmdArgList

    & $cmdPath $cmdArgList >"$PSScriptRoot\output.txt"


> options

    :::powershell
    $arg0 = "e:\$yymmdd\$_\"
    $arg1 = "$yymmdd"
    $arg2 = "$_"
    $allArgs = @($arg0,$arg1,$arg2)
    #$rlm200 = 'E:\rlbobj\rlm20000.exe'
    $rlm200 = 'e:\rlbobj\test.exe'
    $CommandLine = "& $rlm200 $allArgs"
    #Invoke-Expression $CommandLine e:\rlbobj\rlm20000 e:\160226\70260\ 160226 70260

> options

<https://stackoverflow.com/questions/1673967/how-to-run-an-exe-file-in-powershell-with-parameters-with-spaces-and-quotes>

    :::powershell
    $Command = "E:\X64\Xendesktop Setup\XenDesktopServerSetup.exe"
    $Parms = "/COMPONENTS CONTROLLER,DESKTOPSTUDIO,DESKTOPDIRECTOR,LICENSESERVER,STOREFRONT /PASSIVE /NOREBOOT /CONFIGURE_FIREWALL /NOSQL"

    $Prms = $Parms.Split(" ")
    & "$Command" $Prms

> options

<https://stackoverflow.com/questions/25187048/run-executable-from-powershell-script-with-parameters>

    :::powershell
    Start-Process -FilePath "C:\Program Files\MSBuild\test.exe" -ArgumentList "/genmsi/f $MySourceDirectory\src\Deployment\Installations.xml"


> options dates


    :::powershell
    Write-Host 'Get data from service'

    $path = 'D:\DataService'

    Push-Location $path

    $Date     = Get-Date
    $DateFrom = $Date.ToString('yyyy-MM-dd HH:mm:ss')
    $DateTo   = $Date.AddDays(-1).ToString('yyyy-MM-dd')

    & ReportGen.exe -ReportType Data -DateFrom $DateFrom $DateTo


kicker

    @set REQ=%0

    @REM Tokenize current date.
    @for /f "tokens=1,2,3,4 delims=/ " %%a in ( 'date/t' ) do @set x=%%d
    @for /f "tokens=1,2,3,4 delims=/ " %%a in ( 'date/t' ) do @set m=%%b
    @for /f "tokens=1,2,3,4 delims=/ " %%a in ( 'date/t' ) do @set d=%%c


    @REM Passes argument to set YYYYMMDD variable or uses current date if blank
    @if "%1"=="" (
    	@set YYYYMMDD=%x%%m%%d%
    ) else (
    	@set YYYYMMDD=%1
    )

    @set YYMMDD=%YYYYMMDD:~2%

    @echo.
    @echo ==================================================
    @echo.
    @powershell E:\rlbauto\RLMSPaymentstoRetailWEB\RLMStoRetailWEB.ps1 '%YYMMDD%'

----

E:/rlbauto/RLMSPaymentstoRetailWEB/RLMStoRetailWEB.ps1

    :::powershell
    Param([string] $ShortDate)
    if(-not $ShortDate){$yymmdd = get-date -format yyMMdd} else {$yymmdd = $ShortDate}

    write-host "$yymmdd";
    $rlmsrliconfigfile = 'E:\accounts\rlm200.xml'
    [xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile

    foreach($xml_node in $rlmsrlixml.achinfo.box)
    {
        $WorkingFolder = "e:\$yymmdd\$_\";
        $BoxID = $xml_node.number;
    	$Space = " ";

        $PayType = $xml_node.Overrides.Type;
    	$MVTFile = $xml_node.mvtfile -replace 'yymmdd', $yymmdd;
    	$batch_params = $Space + $yymmdd + $Space + $MVTFile + $Space + $BoxID + $Space + $PayType

        #In order for this process to work, both the MVT File and Pay Type must be specified.
        IF(![string]::IsNullOrEmpty($MVTFile) -and ![string]::IsNullOrEmpty($PayType)) {
           Start-Process cmd.exe "/c E:\rlbauto\RLMSPaymentstoRetailWEB\RLMStoRetailWEB.bat $batch_params"
    	   sleep -Seconds 5
    	   Write-Host "RLMS2RetailWEB ran $batch_params";
        }
    }
    Stop-Process -id $pid -Force


## dates

    Param([string] $ShortDate)
    if(-not $ShortDate){$yymmdd = get-date -format yyMMdd} else {$yymmdd = $ShortDate}


    $today = get-date -Format yyyy-MM-dd
    $month = get-date -Format MM
    $monthname = (Get-Culture).DateTimeFormat.GetMonthName($month)
    $year = get-date -format yyyy

        $today = get-date -Format yyyy-MM-dd
    $day = get-date -Format dd
    $month = get-date -Format MM
    $monthname = (Get-Culture).DateTimeFormat.GetMonthName($month)
    $year = get-date -format yyyy


----

    [CmdletBinding()]
    param
    (
       [Parameter(Mandatory=$true,HelpMessage='to pass date use yymmdd')]
       [ValidatePattern('(?# date uses yymmdd)^[0-9][0-9]([0][1-9]|[1][1-2])[0-3][0-9]$')]
       [String] $yymmdd
    )


----

    :::powershell
    #First Thing First: Lets come to Script Directory

    Function Get-ScriptDirectory
    {
      $Invocation = (Get-Variable MyInvocation -Scope 1).Value
      Split-Path $Invocation.MyCommand.Path
    }

    cd (Get-ScriptDirectory)

## license key

    :::powershell
    (Get-WmiObject -query 'select * from SoftwareLicensingService').OA3xOriginalProductKey

## email

    :::powershell
    function emailtask($task){
    $param = @{
        SmtpServer = '10.0.0.12'
        From = 'SUPPORT@retaillockbox.com'
        To = $task.destination
        Subject = $task.path
        Body = "<html><body>$($task.type)<br></body></html>"
        BodyAsHtml = $true
    }
    Send-MailMessage @param
    }

## powershell cheater

[duplicate](http://127.0.0.1:8001/powershell-scripts/)


## web50k

    :::powershell
    $box = read-host -prompt 'enter box'
    [xml]$web5k = Get-Content  'E:\rlbobj\web50000.xml'
    $web5k.webimports.webimport | ? {$_.id -eq $box}


## job name

        :::powershell
        write-host "running $($MyInvocation.MyCommand.Definition)"


## job caller

        :::powershell
        @SET MERGESCRIPT=e:\rlbobj\some.ps1
        @powershell -NoProfile -ExecutionPolicy Bypass %MERGESCRIPT% %WORKPATH%\%POSTINGFILE% %WORKPATH%\%POSTINGFILE3% %WORKPATH%\%MERGEPOSTINGFILE%

## parameter simple

        :::powershell
        param(
          $File1,
          $File2,
          $FileOut
        )

##validation


    do
    {    
    $date= read-host "Please enter date (example 2015/11/09):"

    $date = $date -as [datetime]

    if (!$date) { "Not A valid date and time"}

    } while ($date -isnot [datetime])

    $date

date validation? which script is that in

        :::powershell$box =
        [xml]$web5k = Get-Content  'E:\rlbobj\web50000.xml'
        $id = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty id
        $custid = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty custid
        $conid = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty conid
        $srchtable = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -expandproperty srchtable
        $doctable = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty doctable
        $info = @"
        boxid = $id `r`n
        custid = $custid `r`n
        conid = $conid `r`n
        meta = $srchtable `r`n
        webdoc = $doctable `r`n
        "@
        $query1 = "use rliimages_com SELECT connectionname,connectionid FROM WebConnections where CustomerID = $custid"
        $libraries = Invoke-SqlCmd -ServerInstance "SEA-2950-22" -Database "rliimages_com" -Verbose -Query $query1
        $info | add-content $tempfile
        foreach($_ in $libraries){
        $values = $_ | select connectionname,connectionid
        "$values  " + " `r`n "  | add-content $tempfile
        }


```
PS C:\> [ValidateRange(1,10)] [int]$x = 1 
PS C:\>  $x = 11
The variable cannot be validated because the value 11 is not a valid value for the x variable.
At line:1 char:1 + $x = 11 

```

```
$box = 
[xml]$web5k = Get-Content  'E:\rlbobj\web50000.xml'
$id = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty id
$custid = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty custid
$conid = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty conid
$srchtable = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -expandproperty srchtable
$doctable = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty doctable
$info = @"
boxid = $id `r`n
custid = $custid `r`n
conid = $conid `r`n
meta = $srchtable `r`n
webdoc = $doctable `r`n
"@
$query1 = "use rliimages_com SELECT connectionname,connectionid FROM WebConnections where CustomerID = $custid"
$libraries = Invoke-SqlCmd -ServerInstance "SEA-2950-22" -Database "rliimages_com" -Verbose -Query $query1
$info | add-content $tempfile
foreach($_ in $libraries){
$values = $_ | select connectionname,connectionid
"$values  " + " `r`n "  | add-content $tempfile
}

```

## scheduler

update

    :::powershell
    robocopy \\sea-r320-30\c$\24x7\Output \\10.0.0.142\c$\inetpub\wwwroot\static\scheduler_logs /MIR /FFT /R:3 /W:10 /Z /NP /NDL /MT:16 /xd ".git"  /log:"E:\scripts\SystemsAdministration\monitors\logs\output_copy$isodate.log"


## business date

    :::batch
    e:\rlbobj\getbusinessdate.exe -offset=1

> find scripts using it

    $bidness = Get-ChildItem -Path e:\rlbobj\* -Filter *.bat | Select-String -SimpleMatch "@getbusinessdate" |select Path
    $bidness | Set-Clipboard
    $bidness | Out-File E:\tools\getbus\business_scripts_2.txt



## test

    :::powershell
    $item = 'E:\scripts\RMS\tests\newfolder\'
    if(!(test-path($($item)))){write-host "$item is gone"}

    if(-not (test-path($item))){"not there"} else {"there already"}

## xml and csv

slaughter example

    :::powershell
    <#
    customer: slaughter wholesale
    E:\rlbobj\afsimport.bat calls this script E:\rlbobj\fixaccount2483.ps1
    purpose: outputs a csv file that contains metadata from xml files (bile creates the xml, afs image merge creates the csv)
    usage example:
    fixaccount2483.ps1 "E:\150524\2483\" "E:\150524\2483\afs2483N.CSV" "E:\150524\2483\afs2483new.csv"
    #>
    # example of csv path E:\150524\2483\afs2483N.CSV
    # example of xml path E:\AFSImageMerge\150522\1166


        Param
        (
            # Param1 File Path to where the xml files are
            [Parameter(Mandatory=$true)]
            $thispath,

            # Param2 File Path to csv
             [Parameter(Mandatory=$true)]
            $CSVFile,

            # Param2 Output File Path
            [Parameter(Mandatory=$true)]
            $CSVFileOutPut
        )
    # collects list of xml files
    $xmlsources = Get-ChildItem -path $thispath -Filter *.xml | Where-Object {$_.name -notlike 'micr*'}
    # setup empty array
    $XMLarray.clear
    $XMLarray = @{}

    # populate array with data from the xml files
    foreach ($_ in $xmlsources)
    {
    [xml]$fix = Get-Content ($_.FullName)
    $t = ($fix.batch.page.id).count
    for($i=0; $i -le $t; $i++)
    {
    if($fix.batch.page.id[$i])
    {
    $XMLarray.Add($fix.batch.page.id[$i],$fix.batch.page.field.value[$i])
    }
    }
    }
    # insert the data into the index file, note this requires setting a header, then strips the headers and quotes which get inserted
    # to reuse this for other customers edit -header so the name column matches theirs
    ($InputFile = Import-Csv  $CSVFile -Header 1,2,3,name,4,5,6,7,8,9,10,11,12,13,14,tif,16  -Delimiter ',') | ForEach {
    $_.name = $XMLarray[$_.tif]
    }
    $InputFile | Export-Csv  $CSVFileOutPut  -Delimiter ',' -NoType
    $InputFile = Get-Content $CSVFileOutPut
    $InputFile = $InputFile[1..($InputFile.count - 1)]
    $InputFile.replace('"',"") > $CSVFileOutPut

    #----------------------------------------------------------
    #                   END Script Body
    #----------------------------------------------------------
    Stop-Process -id $pid -Force
    Exit

## combine text files

    :::powershell
    Get-Content -path C:\backup\customers\RLMS-2761Boise\2017-03-14\files\*.csv | Out-File .\Combined.txt



## modules

new-customer (workflow)
new-ticketfolder (workflow)
get-customer ?
zipit2 (now workflow)
pow-box (now launcher)


# deployment

This example finds machines that have quickbooks already and copies a new setup file to their machines.

    :::powershell
    #----------------------------------------------------------
    #              copying setup files to machines
    #----------------------------------------------------------
    # gather installed software using softperfect network scanner and export as xml
    # query that xml for machines that have quickbooks
    # copy quickbooks setup file to machines
    [xml]$netmap = Get-Content  'C:\backup\seattle.xml'
    $qcomputers = $netmap.'network-scanner-result'.devices.item | ? {$_.apps -like '*Quickbooks*'} | select hostname
    $setupfile =  "\\sea-r320-26.rli.local\c$\QuickBooks\16\Setup_QuickBooksEnterprise16.exe"
    #$setupfile = 'U:\16\Setup_QuickBooksEnterprise16.exe'
    foreach ($_ in $qcomputers)
    {
    [string]$host = $_.hostname
    if (!(Test-Path "`\`\$host`\c`$`\rli"))
    {
    write-hoste "does not exist"
    New-Item -path "`\`\$host`\c`$`\rli" -type directory -Force
    }
    if (Test-Path "`\`\$host`\c`$`\rli")
    {
    # copy the file to folders, example destination d:\vision\bank\123\parms
    # write-host "$file `\`\$host`\c`$`\rli"
    copy-item $setupfile "`\`\$host`\c`$`\rli"
    }
    }


# create remit conp file

    :::powershell
    #------------------------------------------------------------------------------------
    # Script Name: MakeCorrPDFImportFile.ps1
    # Description: Parses through the webimport.csv file and
    #              splits out the CORRPDF lines into another webimport file
    #              called webimportPDF.csv that will be used to import
    #              the Correspondence Only (Letters with no checks)
    #              into a different library in RetailWEB than the main
    #              working library.
    #
    # Usage: MakeCorrPDFImportFile.ps1 <YYMMDD> <BOXID>
    # Usage: MakeCorrPDFImportFile.ps1 160925 4144
    #
    # Author: Kim Poellot
    # Creation Date: 12/02/2016
    #
    # Modifications:
    #
    #------------------------------------------------------------------------------------

    Param([string] $ShortDate, [string] $BoxID)
    $BatchType = "CORRPDF";
    $WebImportFile = "e:\JH\" + $ShortDate + "\" + $BoxID + "\webimport.csv";
    $WebImportFilePDF = "e:\JH\" + $ShortDate + "\" + $BoxID + "\webimportPDF.csv";

    $WebImportFileOld = $WebImportFile + ".old";

    write-host "Running MakeCorrPDFImportFile.ps1 on $WebImportFile...";
    if(Test-Path $WebImportFile) {
    	if(Test-Path $WebImportFileOld) {
    		Remove-Item $WebImportFileOld
    	}
    	if(Test-Path $WebImportFilePDF) {
    		Remove-Item $WebImportFilePDF
    	}

    	Copy-Item $WebImportFile -destination $WebImportFileOld -Force
    	$InFile = New-Object System.IO.StreamReader $WebImportFileOld;
    	$OutFile1 = New-Object System.IO.StreamWriter $WebImportFile;
    	$OutFile2 = New-Object System.IO.StreamWriter $WebImportFilePDF;

    	while(!$InFile.EndOfStream)
    	{
    		$line=$InFile.ReadLine();
    		if($line.Contains("CORRPDF"))
    		{
    			$OutFile2.WriteLine($line.Trim());
    		}
    		else
    		{
    			$OutFile1.WriteLine($line.Trim());
    		}
    	}
    	$OutFile1.Flush();
    	$OutFile1.Close();
    	$OutFile1.Dispose();
    	$OutFile2.Flush();
    	$OutFile2.Close();
    	$OutFile2.Dispose();
    	$InFile.Close();

    }

    Write-Host "MakeCorrPDFImportFile.ps1 normal EOJ"


    #----------------------------------------------------------
    #                   END Script Body
    #----------------------------------------------------------
    #Stop-Process -id $pid -Force
    #Exit

----


<h2>
calling powershell from batch file
</h2>


new-customer
<br>
new-ticketfolder
<br>
get-customer
<br>
zipit2
<br>



get web5k info
<pre class="line-numbers"><code class="language-powershell">
$box = read-host -prompt 'enter box'
[xml]$web5k = Get-Content  'E:\rlbobj\web50000.xml'
$web5k.webimports.webimport | ? {$_.id -eq $box}
	</code>
</pre>


<h3>log name of what runs</h3>
<pre class="line-numbers"><code class="language-powershell">
write-host "running $($MyInvocation.MyCommand.Definition)"
	</code>
</pre>

<pre class="line-numbers"><code class="language-powershell">
@SET MERGESCRIPT=e:\rlbobj\some.ps1
@powershell -NoProfile -ExecutionPolicy Bypass %MERGESCRIPT% %WORKPATH%\%POSTINGFILE% %WORKPATH%\%POSTINGFILE3% %WORKPATH%\%MERGEPOSTINGFILE%

	</code>
</pre>


<h2>
simple parameters
</h2>

<pre class="line-numbers"><code class="language-powershell">


param(
  $File1,
  $File2,
  $FileOut
)

</code>
</pre>

<h2>
validating parameters
</h2>

<pre class="line-numbers"><code class="language-powershell">

$box =
[xml]$web5k = Get-Content  'E:\rlbobj\web50000.xml'
$id = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty id
$custid = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty custid
$conid = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty conid
$srchtable = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -expandproperty srchtable
$doctable = $web5k.webimports.webimport | ? {$_.id -eq $box} | select -ExpandProperty doctable
$info = @"
boxid = $id `r`n
custid = $custid `r`n
conid = $conid `r`n
meta = $srchtable `r`n
webdoc = $doctable `r`n
"@
$query1 = "use rliimages_com SELECT connectionname,connectionid FROM WebConnections where CustomerID = $custid"
$libraries = Invoke-SqlCmd -ServerInstance "SEA-2950-22" -Database "rliimages_com" -Verbose -Query $query1
$info | add-content $tempfile
foreach($_ in $libraries){
$values = $_ | select connectionname,connectionid
"$values  " + " `r`n "  | add-content $tempfile
}

</code>
</pre>

<h2>
module template</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
dates</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
calling executable with parameters</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
profile</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
file manipulation</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
string manipulations</h2>

<pre class="line-numbers"><code class="language-powershell">
PS C:\> [ValidateRange(1,10)] [int]$x = 1
PS C:\>  $x = 11
The variable cannot be validated because the value 11 is not a valid value for the x variable.
At line:1 char:1 + $x = 11


</code>
</pre>

<h2>
xml</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>


<h2>
csv</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
loops</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
html</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
advanced functions</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
ftp</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
encryption</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

<h2>
example</h2>

<pre class="line-numbers"><code class="language-powershell">



</code>
</pre>

#### 2017-05-03 15:24:40
# jack henry style dates



    ::powershell

    $TimeStamp = "{0:MMddyyhhmmss}" -f (Get-Date)


# double click

If you miss the functionality of allowing a user to double-click a Powershell script to execute use the following as a work-around:

Create a new shortcut For target enter “powershell.exe -noexit 'path-to-your-script'


## calling another


    function F1{write-host "Here it is";}
    function F2{write-host "Here it is again";}


    . C:\users\blah\desktop\functions.ps1
    #The spaces of the dots before C:\ needs to be there.  So the start of
    #path is .(space)C:\\

    F1
    F2

----


# permission

    Set-ExecutionPolicy RemoteSigned

# c# in ps

    $dirInfo = New-Object System.IO.DirectoryInfo("C:\Code")

    foreach ($file in $dirInfo.EnumerateFiles())
    {
        $file.Name
    }


# exit code

    if ($LastExitCode -ne 0) {
        throw "Command failed with exit code $LastExitCode."
    }

# gist

    :::powershell
    $gist = (ConvertFrom-Json(Invoke-WebRequest https://api.github.com/gists/09365fdec4cefd66e3c2).Content).files
    $gist.(($gist | Get-Member -Type NoteProperty).Name).content
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-07-27 14:06

$killthese = @("notepad","Launchy","MusicManager","ssh-agent","Dropbox","googledrivesync","opendns","flux","hpqtra08","OpenDNSUpdater")
$processlist3 = ps
foreach ($p in $processlist3){if ($killthese -match $p.name){$p.kill()}}


> 2017-09-13

E:\scripts\RetailWeb\manual_uploads\13447\rliaccounting_hr_create_index.ps1


importing manual things

    :::powershell
    # ticket http://sea-1750-79:9675/tickets/v2#unassigned_tickets/13447
    # accounting manual import
    # import P:\Joan's Payroll & HR Records\Terminations
    # "\\sea-r320-26\Accounting\Joan's Payroll & HR Records\Terminations\"
    # "C:\Users\shane.null\Documents\tickets\13447\Terminations"
    # to library rliaccthr

    $importFolder = "C:\Users\shane.null\Documents\tickets\13447\Terminations"
    $index = New-Item "C:\Users\shane.null\Documents\tickets\13447\Terminations\import.csv" -type file

    $FolderName = "Terminations"
    $ProcDate = Get-Date -format "yyyyMMdd"
    $Year = Get-Date -format "yyyy"
    $BoxName = '2014_terminations'

    $AdditionalInfo = ls $importFolder | % { $_.FullName}

    foreach ($_ in $AdditionalInfo){

    "$ProcDate~$Year~$foldername~$BoxName~$_" >> $index

    # write-host $ProcDate + "~" + $Year + "~" +  $foldername + "~" + $BoxName + "~" + $_
    # $_ >> $index
    }

> 2017-09-15

validate csv

    :::powershell
    $stuff = 'E:\image_scans\RLIAccounting\2015RLIACCOUNTING2.csv'
    $stuff2 = 'E:\image_scans\RLIAccounting\2015RLIACCOUNTING.csv'
    function validate-images($csvpath){
        $data = Import-Csv $stuff -Delimiter ~ -Header d,y,f,meh,image
        $data.image | % {test-path($_) }
    }

    # validate-images($stuff)

    validate-images($stuff2)

<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-10-03 14:05
           
`get-pssnapin -registered`
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-10-26 14:40
           
snapins missing on old computers



[instructions](https://amionrails.wordpress.com/2016/06/03/how-to-install-missing-powershell-snap-ins/)
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-10-26 14:41
           
Open up the following sites and download the following



https://www.microsoft.com/en-us/download/details.aspx?id=16978



Make sure you are downloading 64bit version for 64-bit powershell version



Microsoft System CLR Types for SQL Server 2008 R2



X64 Package (SQLSysClrTypes.msi)



Microsoft Windows PowerShell Extensions for SQL Server 2008 R2



X64 Package (PowerShellTools.msi)



Microsoft SQL Server 2008 R2 Shared Management Objects



X64 Package (SharedManagementObjects.msi)
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-10-31 16:07
           
 get version 

<pre class="line-numbers">

<code class="language-sql">

$PSVersionTable.PSVersion

</code>

</pre>


<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-10-31 16:25
           
[sql snapin](https://amionrails.wordpress.com/2016/06/03/how-to-install-missing-powershell-snap-ins/)
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-10-31 16:26
           
    https://goo.gl/SNTLvg
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-10-31 16:52
           
<pre class="line-numbers">

<code class="language-sql">

([Environment]::UserDomainName + "\" + [Environment]::UserName) 



"$env:userdomain\$env:username" 



[Security.Principal.WindowsIdentity]::GetCurrent().Name 





# runas /user:domain\user "powershell e:\test.ps1"

</code>

</pre>
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-11-01 12:32
           
<pre class="line-numbers">

<code class="language-powershell">

write-host $profile

New-Item -path $profile -type file -force

Test-Path -Path $profile

</code>

</pre>
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2017-11-01 12:35
           
<pre class="line-numbers">

<code class="language-powershell">

# open my stuff - this goes in your profile it will open those scripts when you open ise

$scripts = Get-Content C:\backup\home\dev\powershell\startupscripts.md

$script = $scripts.split(",") 

$script | % {Invoke-Item $_}

</code>

</pre>

     
new template



`c:\flowstate\work\web\flask_sites\skeleton\projects-old\cookiecutter\powershell_app`

> old crap

# run home from url

start-process https://chocolatey.org/install

invoke-item C:\backup\home\home.ps1

@powershell -NoProfile -ExecutionPolicy Bypass -Command iwr https://gist.githubusercontent.com/shane0/5044915030e44d00a99e/raw/247a37e23441294f82a2a3d47df8a936d4518aa5/news.ps1 -UseBasicParsing | iex

@powershell -NoProfile -ExecutionPolicy Bypass -Command [System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials; iex ((New-Object System.Net.WebClient).DownloadString('https://gist.githubusercontent.com/shane0/5044915030e44d00a99e/raw/247a37e23441294f82a2a3d47df8a936d4518aa5/news.ps1'))

@powershell -NoProfile -ExecutionPolicy Bypass -Command "[System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials; iex ((New-Object System.Net.WebClient).DownloadString('https://gist.githubusercontent.com/shane0/5044915030e44d00a99e/raw/247a37e23441294f82a2a3d47df8a936d4518aa5/news.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin


> old kanban.ps1

        invoke-item C:\backup\home\kanban.ps1

        # this is a cleanup file, its messy...  
        # stuff it does
        # copies done from timeline to done, copies everything but done to timelinefixed, you then copy/paste cleaned content to timeline...
        # todo: one line this crap
        # it also makes a backup until this is less confusing... 

        # in use
        # archive done items
        $yyyymmdd = get-date -Format yyyy-MM-dd
        "> $yyyymmdd " | ac c:\backup\home\done.md
        gc C:\backup\home\timeline.md | select-string "#DONE:*" | SELECT line -ExpandProperty line | ac C:\backup\home\done.md

        # backup timeline.md
        function copy-withisoname($scriptname){
        $scriptname = read-host -prompt 'enter full path of script'
        $isodate = get-date -Format yyyy-MM-dd
        $renamedfilename = "$scriptname" + "." + "$isodate"
        copy-item -path $scriptname -Destination $renamedfilename
        write-host "created " + "$renamedfilename" + ""
        $renamedfilename | clip
        }
        copy-withisoname 
        C:\backup\home\timeline.md

        clear-content C:\backup\home\timelinefixed.md

        # remove done from timeline
        $tempvar = 'C:\backup\home\timeline.md'
        Get-Content $tempvar | select-string "#DONE:" -NotMatch | ac C:\backup\home\timelinefixed.md
        #manual copy paste, not sure why the file is locked?

        # working




        # researching
        get-content $tempvar | select-string "#DONE:" -notmatch | set-content $tempvar -Force # broke


        # move notes?

        get-content C:\backup\home\timeline.md | select-string "#NOTE:"
        C:\backup\home\projects.md


        get-content c:\new\temp_*.txt | select-string -pattern 'H`|159' -notmatch | Out-File c:\new\newfile.txt

        # remove done from timeline
        $tempvar = 'C:\backup\home\timeline.md'
        get-content $tempvar | select-string "#DONE:" -notmatch | set-content 'c:\backup\home\timelinefixed.md' -Force

        read all lines in file | only select lines that do not match XXL | write remaining lines back to file

        $error.Clear()
        $tempvar = 'C:\backup\home\timeline.md'
        get-content $tempvar | select-string "#DONE:" -notmatch | set-content $tempvar -Forceif($error.Count -eq 0) {
        # do something useful
        }
        else {
        # do something that doesn't involve spamming oneself
        }

        # unlock file
        openfiles /local on

        $lockedFile="C:\Windows\System32\wshtcpip.dll"
        Get-Process | foreach{$processVar = $_;$_.Modules | foreach{if($_.FileName -eq $lockedFile){$processVar.Name + " PID:" + $processVar.id}}}

> 

----
# vm host

    get-vm | select name, state| export-csv -Path vmlist.csv

# csv

## remove columns

```powershell
$source = "C:\backup\home\projects\tickets\ss.csv"
$destination = "C:\backup\home\projects\tickets\sscleaned.csv"
(Import-CSV $source  |
    Select "Ticket Number","Subject","Date Created","Last UPdate" |
    ConvertTo-Csv -NoTypeInformation) |
    # Select-Object -Skip 1) -replace '"' |
    Set-Content $destination
```

## Import-CSV match regex


```powershell
$grabcsv = import-csv E:\tools\steven\powershell\csv\example2.csv -Header 1,2,3,4,5 -Delimiter ~
$grabcsv | select *
$grabcsv.1 | ? {$_ -match "10138262\d{2}"} # get column one where matches regex
$grabcsv | ? {$_.5 -eq "0000002951"} # get the row where column 5 equals
$grabcsv | sort-object -Property $_.1  # sort by column 1
$grabcsv.4 -replace(0,"") # strip 0's
$grabcsv | Sort-Object -Property '3' -Unique | select '1' | Measure-Object # column 3 count unique only
$grabcsv | select '5' # column 5 via pipe vs inline


```


> regex 

    "I am a string" -match '\bstr'       # true
    "I am a string" -replace 'a\b','no'  # I am no string


example of xml parameters in command

```powershell
<#
Automation
job host:
job software:
job folder:
job name:
job time:
job called by:
job calls:
start-process "http://sea-1750-79:9675/tickets/v2#recently_updated/14362"
start-process "http://wiki/doku.php?id=it:software:rlms2retailweb"
start-process http://powershell.com/cs/blogs/tips/archive/2013/07/19/using-validatepattern-attribute.aspx
This can be used as a module

#>
```

```
function Import-RlmsToRliAll {
  <#
  .SYNOPSIS
  runs e:\rlbobj\rlm20000.exe for all boxes in E:\Accounts\rlm200.xml
  .EXAMPLE
  Import-RlmsToRliAll 160226
  .PARAMETER
  yymmdd
  #>
  [CmdletBinding()]
  param
  (
     [Parameter(Mandatory=$true,HelpMessage='to pass date use yymmdd')]
     [ValidatePattern('(?# date uses yymmdd)^[0-9][0-9]([0][1-9]|[1][1-2])[0-3][0-9]$')]
     [String] $yymmdd
  )
  begin {
    #if(-not $yymmdd){$yymmdd = get-date -format yyMMdd}
    $rlmsrliconfigfile = 'E:\Accounts\rlm200_kim.xml'
    [xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile
    #$rlmsrlixml.achinfo.box.number
  }
  process {
        foreach($_ in $rlmsrlixml.achinfo.box.number)
        {
        #write-host "do stuff with e:\rlbobj\rms72514.bat xxx 20130905 72514"
        $arg0 = "e:\$yymmdd\$_\"
        $arg1 = "$yymmdd"
        $arg2 = "$_"
		$xxx = "xxx";
		$YYYYMMDD = "20$yymmdd";
        $allArgs = @($arg0,$arg1,$arg2)
        #$rlm200 = 'E:\rlbobj\rlm20000.exe'
		#I was planning to use Ronald Wastewater 4554 on 3/23/2016 and Doctors Clinic 72513 on 3/23/2016 for testing.
        $rlm200 = 'e:\rlbobj\test.exe'
        $CommandLine = "& $rlm200 $allArgs"

		if($arg2 -eq "4554")
		{
		foreach ($bename in $rlmsrlixml.achinfo.box) {
		    if($bename.number -eq $arg2){
		        $BatchFile = $arg0+$bename.batchfile;
				break;
		    }
		}
			#$BatchFile = $rlmsrlixml.achinfo.box.batchfile | ? { $rlmsrlixml.achinfo.box.number -eq $arg2 }
			write-host "do this $CommandLine | $BatchFile"
		}

        #Invoke-Expression $CommandLine e:\rlbobj\rlm20000 e:\160226\70260\ 160226 70260
        sleep -Seconds 1
        }
    }
}

#variable tests
#no variable
#Import-RlmsToRliAll
#invalid
#Import-RlmsToRliAll 20160101
#valid
Import-RlmsToRliAll 160704
write-host "";
write-host "Finished with script!"
<#
don't use these they're here for research

# find customers
$rlmsrliconfigfile = 'E:\Accounts\rlm200.xml'
[xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile
#client
$rlmsrlixml.achinfo.box.client | clip
#box
$rlmsrlixml.achinfo.box.number

# find scripts

#search in files http://wiki/doku.php?id=it:software:rlms2retailweb
$pattern = 'rlm20000'
Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
.examples
e:\rlbobj\rlm20000 e:\160226\70260\ 160226 70260

@call e:\rlbobj\rlm20000 %RMSDATA% %DATE% %RBOX%
@set DATE=%FULLDATE:~2%
@set WORKPATH=e:\%DATE%\%RBOX%
@set RMSDATA=%WORKPATH%\rms%BOX%.dat
@set RBOX=70260


#>

```


run command w/ parameters 2

```powershell#------------------------------------------------------------------------------------
# Script Name: RLMS2RetailWEB2.ps1
# Description: Parses through the rlm200xml file and
#              creates the necessary files to import into RetalWEB.
#
# Usage: RLMS2RetailWEB2.ps1 <YYMMDD>
# Usage: RLMS2RetailWEB2.ps1 160323
#
# Author: Kim Poellot
# Creation Date: 10/17/2016
#
# Modifications:
#
#------------------------------------------------------------------------------------

Param([string] $ShortDate)
if(-not $ShortDate){$yymmdd = get-date -format yyMMdd} else {$yymmdd = $ShortDate}

write-host "$yymmdd";

$rlmsrliconfigfile = 'E:\Accounts\rlm200_kim.xml'
##I was planning to use Ronald Wastewater 4554 on 3/23/2016 and Doctors Clinic 72513 on 3/23/2016 for testing.
[xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile

foreach($_ in $rlmsrlixml.achinfo.box.number)
{
    #write-host "do stuff with e:\rlbobj\rlm20000 e:\$yymmdd\$_\ $yymmdd $_ "
    $WorkingFolder = "e:\$yymmdd\$_\";
    #$arg1 = "$yymmdd"
    $BoxID = "$_";
	$Space = " ";
    #$allArgs = @($WorkingFolder,$arg1,$BoxID)

	#$PullScript="E:\rlbobj\RLMStoRetailWEB.bat";


	foreach ($xml_node in $rlmsrlixml.achinfo.box)
	{
	    if($xml_node.number -eq $BoxID)
		{
	        #$BatchFile = $WorkingFolder + $xml_node.batchfile;
			$PayType = $xml_node.Overrides.Type;
			$MVTFile = $xml_node.mvtfile -replace 'yymmdd', $yymmdd;
			$batch_params = $Space + $yymmdd + $Space + $MVTFile + $Space + $BoxID + $Space + $PayType

			Start-Process cmd.exe "/c E:\rlbobj\RLMStoRetailWEB.bat $batch_params"
			sleep -Seconds 5
			break;
	    }
	}
}
```

# old

```
<#
powershell cheatsheet
c:\backup\powershell.ps1
http://rlb-sysadmin002:85/grav-admin/cheatsheets/cheatsheets-item-2
#>
# http://powershell.com/cs/blogs/ebookv2/default.aspx

#----------------------------------------------------------
#                    installing and using powershell
#----------------------------------------------------------

$PSVersionTable.PSVersion
$Env:PSModulePath
Import-Module ServerManager
Add-WindowsFeature PowerShell-ISE


# profile

https://msdn.microsoft.com/en-us/library/windows/desktop/bb613488(v=vs.85).aspx

#update 2 to 30 cmd line
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%systemdrive%\chocolatey\bin
cinst powershell


#check policy on remote pc
New-PSSession –Computer sea-r320-30.rli.local | Get-ExecutionPolicy
#registry
reg query  HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell
Get-ExecutionPolicy -List | Format-Table -AutoSize


# get .net version

$PSVersionTable.CLRVersion

http://www.howtogeek.com/117192/how-to-run-powershell-commands-on-remote-computers/



#logging
#write the name of the script
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
#pslogging
Import-Module pslogging
Start-Log -LogPath 'E:\tools\Infrastructure\schedulers' -LogName pslog.txt -ScriptVersion 1
Write-LogInfo $MyInvocation.MyCommand.Definition
& "C:\Program Files\TeraCopy\TeraCopy.exe"  copy \\sea-r320-30\c$\24x7\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reports /OverwriteOlder /Close
& "C:\Program Files\TeraCopy\TeraCopy.exe"  copy \\204.15.228.182\c$\tools\reportsswiftcosched \\10.0.0.142\c$\inetpub\wwwroot\static\reportsswiftco  /OverwriteOlder /Close
& "C:\Program Files\TeraCopy\TeraCopy.exe"  copy \\10.0.0.72\c$\tools\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reportsoldscheduler  /OverwriteOlder /Close
stop-log -LogPath 'e:\tools\infrastructure\pslog_main.txt' -NoExit

#transcript
start-transcript -path c:\backup\transcript.txt
#help stop-log -examples
#log this filename http://stackoverflow.com/questions/817198/how-can-i-get-the-current-powershell-executing-file

# end
gci|select mode,attributes -u

Get-Module –ListAvailable
Import-Module ActiveDirectory

#find users in ad
http://www.tomsitpro.com/articles/powershell-active-directory-cmdlets,2-801.html
Import-Module ActiveDirectory
Get-ADDomain | clip

Get-ADUser something
Get-ADUser –Filter {Surname –eq “Jeremy”}
Get-ADUser –Filter {GivenName –eq “Jeremy”}
Get-ADPrincipalGroupMembership –Identity khess
#Remove-ADGroupMember –Identity “Domain Admins” –Member  “khess”
Get-ADPrincipalGroupMembership –Identity khess
New-ADUser –Name “Abby Jones” –GivenName Abby –Surname Jones –UserPrincipalName ajones@mw.local –SamAccountName ajones
Search-ADAccount –AccountDisabled –UserOnly |FT Name
Set-ADAccountPassword –Identity ajones –NewPassword (Read-Host –AsSecureString “New Password”) –OldPassword (Read-Host –AsSecureString “Old Password”)

Enable-ADAccount –Identity ajones
Search-ADAccount –LockedOut –UsersOnly |FT Name
Unlock-ADAccount –Identity khess
Get-ADUser -Filter * -SearchBase "ou=,dc=rli,dc=local"
Get-ADUser -Filter * -SearchBase "dc=rli,dc=local" | select samaccountname | clip
Get-ADGroupMember "production" | Format-Table samaccountname
##

#----------------------------------------------------------
#                    general syntax
#----------------------------------------------------------
get-verb

help something -online


$variable | select -first 1 -prop *

If (test) {execute when true}
If (test1 -Or text2) {execute when true}


I would recommend PowerShell remoting. But answer to question how? depends very much on environment (domain/ workgroup) and your rights (on server/ within domain).

Thats one command in AD environment:

Set-ADComputer -TrustedForDelegation $true -Identity <your target server>
And later:

Invoke-Command -ComputerName <target server> -ScriptBlock { your command }
#error handling
You have a couple of options. The easiest involve using the ErrorAction settings.

-Erroraction is a universal parameter for all cmdlets. If there are special commands you want to ignore you can use -erroraction 'silentlycontinue' which will basically ignore all error messages generated by that command.

If you want to ignore all errors in a script, you can use the system variable $ErrorAction and do the same thing: $ErrorActionPreference= 'silentlycontinue'

http://ss64.com/ps/syntax-automatic-variables.html

operators
http://ss64.com/ps/syntax-compare.html

# dl and run script
powershell -nop -c "iex(New-Object Net.WebClient).DownloadString('http://something.com')"
# run encoded https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/
$command = "Write-Host 'My voice is my passport, verify me.'" $bytes = [System.Text.Encoding]::Unicode.GetBytes($command) $encodedCommand = [Convert]::ToBase64String($bytes) powershell.exe -EncodedCommand $encodedCommand


# Create a hash table for the item and use double quotes
$myItem = @{
    type = 'Beer'
    price = '$6.00'
}

$myString = "The price of a $($myItem.type) is $($myItem.price)"
Write-Output $myString

# When our string contains double quotes

$myItem = @{
    name = 'Matt'
    age = 29
}

$myStringBackTicks = "The user `"$($myItem.name)`" is of age `"$($myItem.age)`""
$myStringDoubleQuotes = "The user ""$($myItem.name)"" is of age ""$($myItem.age)"""
Write-Output $myStringBackTicks
Write-Output $myStringDoubleQuotes
# another
$webData = @{
    name = 'Matt'
    fontcolor = 'red'
    type = 'beer'
    price = '6.00'
    age = 29
}


$htmlPage = "
<html>
<head>
<title>$($name.title)'s Web Page!</title>
</head>
<body>
<{0}>Welcome to $($webData.name)'s Web Page!</{0}>
<p>
<font color=""$($webData.fontcolor)"">$($webData.name)'s Age is ""$($webData.age)"".</font>
<p>
This means he can buy a $($webData.type) for `$$($webData.price)!
</body>
</html>
" -f 'h1'

Set-Content -Path C:\Temp\MyPage.html -Value $htmlPage


#see if customer sent us a duplicate filename used for water district
$ar = 'C:\ftproot\waterdistrict111\archive'
$in = 'C:\ftproot\waterdistrict111\inbound'

$ard = ls $ar -filter *.csv
$ind = ls $in -filter *.csv

compare-object $ard $ind -property name -includeEqual  | clip

compare-object $ard $ind -property name -includeEqual  | Where-Object { $_.SideIndicator -eq '==' }

C:\ftproot\waterdistrict111\archive\h2o_data_20160126.csv

#backup scheduler report with teracopy
teracopy.exe copy \\sea-r320-30\c$\24x7\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reports /OverwriteOlder /Close

#----------------------------------------------------------
#                find bad data from indexing
#----------------------------------------------------------
$validate8402folder = "E:\AFSImageMerge\160115\8402"
$xmlfiles = Get-ChildItem -Path $validate8402folder -filter *.xml | select fullname
foreach($_ in $xmlfiles)
{
[xml]$values = get-content $_.Fullname
$values.batch.page.field | ? {$_.id -eq 'Paid Amount'} | select value
}
#----------------------------------------------------------
#                image replacement
#----------------------------------------------------------
Please replace the image for DocID 647 with this image:
$20160224tif = 'L:\AfsImageMerge\160223\3656\2016022336560001\3656201602233656000100004.tif'
Copy-Item $20160224tif E:\tools\1000-4000\3656UW\20160224
import-module convertto-pdf
get-module
ConvertTo-Pdf E:\tools\1000-4000\3656UW\20160224
$20160224pdf = 'E:\tools\1000-4000\3656UW\20160224\3656201602233656000100004.pdf'
use rliimages_com
select DocumentPath from C000151WebDocuments
where documentid = 647 and
ConnectionID = 632
$20160224replacepath = '\\sea-NX3200-21\RetailWeb\dotcom\imgfldrs\3656\2016022322\3656201602233656000100004.pdf'
Copy-Item -path $20160224replacepath -destination E:\tools\1000-4000\3656UW\20160224\original
Remove-Item $20160224replacepath
copy-item $20160224pdf $20160224replacepath

<#----------------------------------------------------------
                   error handling
----------------------------------------------------------#>
https://rkeithhill.wordpress.com/2009/08/03/effective-powershell-item-16-dealing-with-errors/
<#----------------------------------------------------------
                    parameter examples
----------------------------------------------------------#>
#switch

$value = 1
switch ($value)
{
  1  { "Number 1" }
  2  { "Number 2" }
  3  { "Number 3" }
}

#what?


(delete webimport.sent)
afsimport.bat 3504 20150703
web50000.exe "E:\image_scans\RLIAccounting\2013 RLI Vendor - 1.txt" rliacct web50000.xml
some files ask for %0 which is afs use xxx
#----------------------------------------------------------
#                    grep  files and folders
#----------------------------------------------------------
dir -path "E:\rlbobj\" -filter *.ps1 | Select-String -pattern xml
$folder="E:\image_scans\RLIAccounting\2013 RLI Vendor - 2\"
Get-ChildItem  -path $folder | where-object {$_.extension -eq ".pdf"} | select fullname
Get-ChildItem -Path @("e:\rlbobj","e:\scripts") -filter '*1013*' | Select FullName
$d = get-date -format yyMMdd
Get-ChildItem  E:\151020\  | Where-Object {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object | select fullname >> c:\logs\$boxupload`.$d.txt
Get-ChildItem  E:\151020\  | Where-Object {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object | select name
#search previous dates
$e = 'e:\'
# name contains
Get-ChildItem -Path $path -Include *.dat -Recurse | Where { $_.Name.Contains($pattern) }
for ($i=10; $i -ge 1; $i--) {
$dt = (Get-Date).AddDays(-$i)
$ymd = Get-Date $dt -Format yyMMdd
$boxfolder1 = "$e$ymd"
$boxfolder1
foreach($_ in (ls -path $boxfolder1 | ? {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object ))
{
$boxfolder2 = "$boxfolder1\$_"
$boxfolder2
}
}
$regex.Matches('abc 1-2-3 abc 4-5-6') | foreach-object {$_.Value}
#find previous days work-around
#search previous dates
$e = 'e:\'
for ($i=10; $i -ge 1; $i--) {
$dt = (Get-Date).AddDays(-$i)
$ymd = Get-Date $dt -Format yyMMdd
$boxfolder1 = "$e$ymd"
#$boxfolder1
foreach($_ in (ls -path $boxfolder1 | ? {$_.PSIsContainer -eq $true -and  $_.Name -eq '8770' }| Sort-Object ))
{
$boxfolder2 = "$boxfolder1\$_"
$boxfolder2
}
}

#get time
Set-Location c:\test\
$colFiles = (get-childitem | Where {!$_.PSIsContainer})

ForEach ($i in $colFiles)
{
	Write-Host $i.CreationTime
}


#file owner

Get-ChildItem -path e:\160222\1852 | % {$_ | get-acl }

get-childitem "C:\windows\System32" -recurse | where {$_.extension -eq ".txt"} | % {
     Write-Host $_.FullName
}

#compare the content of two files
Compare-Object -IncludeEqual -ReferenceObject (Get-Content E:\160119\1165\160119_1165_ACH.dat) `
-DifferenceObject (Get-Content E:\160119\1165\copy\160119_1165_ACH.dat)


#rename file extensions
gci -path C:\backup\core\corewww -filter *.html | rename-item -newname {  $_.name  -replace '\.html','.shtml'  }


#----------------------------------------------------------
#                    file and folder
#----------------------------------------------------------



#backup scheduler report
teracopy.exe copy \\sea-r320-30\c$\24x7\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reports /OverwriteOlder /Close
#backup wiki
teracopy.exe copy \\webapps\c$\inetpub\wwwroot\dokuwiki\data\pages C:\Bitnami\dokuwiki-20150810-0\apps\dokuwiki\htdocs\data\pages /OverwriteOlder /Close
#wbadmin
wbadmin.exe START BACKUP -backupTarget:\\somewhere -systemState -include:C: -allCritical -quiet
http://blogs.technet.com/b/11/archive/2008/08/16/mounting-backup-file-created-by-windows-2008-server.aspx
mount or restore
using ntdsutil
or disk management / mount

different hardware
http://www.wbadmin.info/articles/howto-bare-metal-restores-windows-server-2008-backup.html
#db backup swiftco-vsa8xyg

OPTION EXPLICIT

Dim cn, cmd, diffType

Set cn = CreateObject("ADODB.Connection")
Set cmd = CreateObject("ADODB.Command")

IF Wscript.Arguments.length > 0 THEN
	diffType = Wscript.Arguments(0)

	'cn.Open "SWIFTCO-VSA8XYG\RLMSSQLEXPRESS", "sa", "T1stCitD"
	cn.Open "Provider=sqloledb;Data Source=SWIFTCO-VSA8XYG\RLMSSQLEXPRESS;User Id=sa;Password=nope;"
	Set cmd.ActiveConnection = cn
	cmd.CommandText = "sp_backupalldb"
	cmd.CommandType = 4 'adCmdStoredProc
	cmd.Parameters.Refresh ' Ask the server about the parameters for the stored proc
	cmd.Parameters(1) = diffType ' 1 = full diff, 0 = partial
	cmd.Execute
ELSE
	msgbox "This script requires an argument of 0 or 1, 0 being partial diff, 1 being full diff. ¿Comprende?"
	wscript.quit
END IF


$f = 'C:\WACPDF\Input'
New-Item $f -type directory
Invoke-Item $f

# write to top of file (data from temp file)

Add-Content -Path c:\temp\push\RAWlist.txt -Value (Get-Content "C:\temp\push\Pushlist.txt")
invoke-item  c:\temp\push\RAWlist.txt
invoke-item C:\temp\push\Pushlist.txt
new-item -path c:\temp\push\RAWlist.txt -type file -force
new-item -path C:\temp\push\Pushlist.txt -type file -force
Add-Content -Path c:\temp\push\RAWlist.txt -Value (Get-Content "C:\temp\push\Pushlist.txt")

http://www.howtogeek.com/128680/how-to-delete-move-or-rename-locked-files-in-windows/

#recently modified
$dir = "U:\"
$latest = Get-ChildItem -Path $dir -r -include *.qbw | Sort-Object LastAccessTime -Descending | Select-Object -First 25
$latest.name
#map drive
http://www.howtogeek.com/132354/how-to-map-network-drives-using-powershell/
New-PSDrive –Name “E” –PSProvider FileSystem –Root “\\sea-2020-63\prod”

net use M: \\touchsmart\Share /Persistent:Yes
net use m: /delete /yes

# directory to xml, use for backup script...
$configFile = 'C:\backup\dev\alert\alert.xml'
$directory = "c:\backup\dev\alert\test\rlbobj.xml"
Get-ChildItem -Recurse e:\rlbobj | Export-CliXML $directory

Get-ChildItem -Recurse c:\backup | Export-CliXML $directory


#----------------------------------------------------------
#                    sym links http://www.howtogeek.com/howto/16226/complete-guide-to-symbolic-links-symlinks-on-windows-or-linux/
#----------------------------------------------------------
fsutil behavior query SymlinkEvaluation
mklink /H “C:\Users\Matthew\Desktop\ebook.pdf”  “C:\Before You Call Tech Support.pdf”
<#
#----------------------------------------------------------
remote registry
#>
sc \\computername start remoteregistry
\\RLB221-FS002\c$\Program Files\WinSCP
#----------------------------------------------------------
#                    wmi
#----------------------------------------------------------

# rebecca's computer is slow, she's got 32 bit
gwmi win32_operatingsystem  -ComputerName RLB-IMP002 | select osarchitecture
gwmi win32_processor -ComputerName RLB-IMP002 | select -first 1 | select addresswidth
#----------------------------------------------------------
#                    remote commands
#----------------------------------------------------------
http://www.howtogeek.com/117192/how-to-run-powershell-commands-on-remote-computers/
open pshell as admin to enable remoting Enable-PSRemoting -Force
psexec \\PC-Name -u user -p password -i -c setup.exe /S /v"ALLUSERS=1 /qb"
#----------------------------------------------------------
#                    ad
#----------------------------------------------------------
Get-ADUser shane.null -Properties * | Get-Member

Get-ADGroupMember -identity "IT" -Recursive | Get-ADUser -Property DisplayName | Select Name,ObjectClass,DisplayName﻿

Get-ADGroupMember "ops" | Format-Table samaccountname
#servers
Get-ADComputer -Filter {OperatingSystem -Like "Windows Server*"} -Property * | Format-Table Name,OperatingSystem,OperatingSystemServicePack -Wrap -Auto
#not server
Get-ADComputer -Filter {OperatingSystem -NotLike "*server*"} -Property * | Format-Table Name,OperatingSystem,OperatingSystemServicePack -Wrap -Auto

Get-ADComputer -Filter * -Property * | Select-Object Name,OperatingSystem,OperatingSystemServicePack,OperatingSystemVersion | Export-CSV E:\scripts\cheatsheets\AllWindows.csv -NoTypeInformation -Encoding UTF8
invoke-item E:\scripts\cheatsheets\AllWindows.csv


Get-ADComputer -Filter * -Property * | Select-Object Name | Export-CSV c:\backup\monitors\devices.csv -NoTypeInformation -Encoding UTF8
invoke-item c:\backup\monitors\devices.csv
# test map drives are up
Get-WmiObject -Class Win32_MappedLogicalDisk | % {[System.IO.Directory]::Exists("$($_.name)\")}
############# gt map remote
$ComputerName = "afs-vsu"

gwmi win32_mappedlogicaldisk -ComputerName $ComputerName | select SystemName,Name,ProviderName,SessionID | foreach {
 $disk = $_
 $user = gwmi Win32_LoggedOnUser -ComputerName $ComputerName | where { ($_.Dependent.split("=")[-1] -replace '"') -eq $disk.SessionID} | foreach {$_.Antecedent.split("=")[-1] -replace '"'}
 $disk | select Name,ProviderName,@{n="MappedTo";e={$user} }
}

###############
$computer =

get-content -path C:\Users\shane.null\Desktop\vpn\computers.csv
Get-WmiObject -Class win32_systemenclosure -ComputerName $computer |  Where-Object { $_.chassistypes -eq 9 -or $_.chassistypes -eq 10-or $_.chassistypes -eq 14})
#who is logged in
$SERVER = rlb-sysadmin002
query user /server:$SERVER

#logoff user
$server = hostname
$username = $env:USERNAME
$session = ((quser /server:$server | ? { $_ -match $username }) -split ' +')[2]

logoff $session /server:$server

#----------------------------------------------------------
#                    remote cmd / office license
#----------------------------------------------------------
open a cmd as admin on your computer

psexec \\hostname cmd

 #t hen you're on their machine, run these two commands to register the key and activate it

C:\Windows\System32\cscript.exe "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /inpkey:asdf-asdf-asdf-asdf-asdf

C:\Windows\System32\cscript "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /act

C:\Windows\System32\cscript.exe "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /inpkey:P4B6J-JFPRR-RGQY6-JPWQB-C49T6
C:\Windows\System32\cscript "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /act


P4B6J-JFPRR-RGQY6-JPWQB-C49T6

C:\Windows\System32\cscript.exe "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /inpkey:86QQY-KRTDC-7JDTT-7733P-VH2K7
#----------------------------------------------------------
#                    laptop search
#----------------------------------------------------------
Get-WmiObject -Class win32_systemenclosure -ComputerName $computer |  Where-Object { $_.chassistypes -eq 9 -or $_.chassistypes -eq 10-or $_.chassistypes -eq 14})

Function Get-Laptop
{
 Param(
 [string]$computer = rlb220-test001.rli.local
 )
 $isLaptop = $false
 if(Get-WmiObject -Class win32_systemenclosure -ComputerName $computer |
    Where-Object { $_.chassistypes -eq 9 -or $_.chassistypes -eq 10 `
    -or $_.chassistypes -eq 14})
   { $isLaptop = $true }
 if(Get-WmiObject -Class win32_battery -ComputerName $computer)
   { $isLaptop = $true }
 $isLaptop
}

# end function Get-Laptop

# *** entry point to script ***

If(get-Laptop) { "it's a laptop" }
else { "it's not a laptop"}
#----------------------------------------------------------
#                    alerts
#----------------------------------------------------------


@e:\rlbobj\bmail -s 10.0.0.12 -p 25 -h -t fernando.salazar@retaillockbox.com -f RLBView@retaillockbox.com -a "%BOX% was sent on %YYYYMMDD%." -b "Shane, please verify this is working." >nul 2>&1

#----------------------------------------------------------
#                    software inventory
#----------------------------------------------------------

https://gallery.technet.microsoft.com/scriptcenter/Get-InstalledSoftware-Get-5607a465?tduid=(44a95684895b35d6f52023560348a780)(256380)(2459594)(TnL5HPStwNw-fS7b4k.jGUdhXqcx6xGNgw)()


#dot source the script (or add to your profile or a custom module):
    . "\\path\to\Get-InstalledSoftware.ps1"

#Get help on Get-ADGroupMembers
    Get-Help Get-InstalledSoftware -Full

#Pull all software from c-is-ts-91, c-is-ts-92
    Get-InstalledSoftware c-is-ts-91, c-is-ts-92

#Pull software with publisher matching microsoft and displayname matching lync from c-is-ts-91
    "c-is-ts-91" | Get-InstalledSoftware -DisplayName lync -Publisher microsoft

#Pull software with publisher matching Microsoft and DisplayName starting with Microsoft Office from c-is-ts-91 and c-is-ts-92
    Get-InstalledSoftware -ComputerName c-is-ts-91, c-is-ts-92 -DisplayName '^Microsoft Office' -Publisher Microsoft | Format-Table -AutoSize

#Pull software with published matching Citrix.  Note the uninstall strings we could use to automate uninstall
    Get-InstalledSoftware -ComputerName c-is-ts-91 -Publisher citrix | Format-Table -AutoSize


Write-Host STarting
$computers = Import-Csv "C:\Temp\computerlist.csv"

$array = @()

foreach($pc in $computers){
	Write-Host "PC name is $pc"
    $computername=$pc.computername

    #Define the variable to hold the location of Currently Installed Programs

    $UninstallKey="SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"

    #Create an instance of the Registry Object and open the HKLM base key

    $reg=[microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine',$computername)

    #Drill down into the Uninstall key using the OpenSubKey Method

    $regkey=$reg.OpenSubKey($UninstallKey)

    #Retrieve an array of string that contain all the subkey names

    $subkeys=$regkey.GetSubKeyNames()

    #Open each Subkey and use GetValue Method to return the required values for each

    foreach($key in $subkeys){

        $thisKey=$UninstallKey+"\\"+$key

        $thisSubKey=$reg.OpenSubKey($thisKey)

        $obj = New-Object PSObject

        $obj | Add-Member -MemberType NoteProperty -Name "ComputerName" -Value $computername

        $obj | Add-Member -MemberType NoteProperty -Name "DisplayName" -Value $($thisSubKey.GetValue("DisplayName"))

        $obj | Add-Member -MemberType NoteProperty -Name "DisplayVersion" -Value $($thisSubKey.GetValue("DisplayVersion"))

        $obj | Add-Member -MemberType NoteProperty -Name "InstallLocation" -Value $($thisSubKey.GetValue("InstallLocation"))

        $obj | Add-Member -MemberType NoteProperty -Name "Publisher" -Value $($thisSubKey.GetValue("Publisher"))

        $array += $obj

    }

}

$array | Where-Object { $_.DisplayName } | select ComputerName, DisplayName, DisplayVersion, Publisher | ft -auto
#----------------------------------------------------------
#                    text manipulations
#----------------------------------------------------------
$idx = 'c:\index.csv'
(gc $idx).replace('.tif','.pdf')|sc $idx
open firewall to internal servers

psexec \\machine cmd

netsh advfirewall firewall add rule name="Firewall Off IP x Incoming" dir=in action=allow protocol=ANY remoteip=1.1.1.1
netsh advfirewall firewall add rule name="Firewall Off IP x Outcoming" dir=out action=allow protocol=ANY remoteip=1.1.1.1

#----------------------------------------------------------
#                    get bus date http://wiki/doku.php?id=it:software:getbusinessdate
#----------------------------------------------------------
E:\rlbobj>getbusinessdate.exe
Usage getbusinessdate.exe /offset=n | /IsFirst | /IsLast [/date=yyyymmdd] [/xml=
path]
/offset = positive or negative date offset
/IsFirst or /IsLast will return 1 if it is the First or Last business day of the
 month, 0 if not
/date = date to calculate offset, current date used if ommitted
/xml = path to holidays.xml, e:\rlbobj\holidays.xml used if ommitted
E:\rlbobj\getbusinessdate.exe
example for next day used in a batch file

@e:\rlbobj\getbusinessdate /offset=+1 /date=%YYYYMMDD%
#----------------------------------------------------------
#                 Removal of Work older than 31 days
#----------------------------------------------------------
Automatic Removal of Work older than 31 days

Open e:\scripts\maintenance\rlbftp2_file_delete.bat in a text editor.
Add another line to the script to include the following:

  robocopy \\rlbftp2\c$\ftpshare\%Directory Name%\ \\rlbftp2\c$\oldftpfiles\old_ftp_files\%Directory Name%\ /move /minage:31 /E /Z /R:2 /NP \\

Replace %Directory Name% so the full path should reflect the new Virtual Root you created earlier.
This will replace
  robocopy \\rlbftp2\c$\ftpshare\%Directory Name%\ \\rlbftp2\c$\oldftpfiles\old_ftp_files\%Directory Name%\ /move /minage:31 /E /Z /R:2 /NP \\
#----------------------------------------------------------
#              wiki backup and system backup
#----------------------------------------------------------
@ rem system backup
wbadmin.exe START BACKUP -backupTarget:\\SEA-R610-28\X -systemState -include:C: -allCritical -quiet

 @rem backup dokuwiki pages
@robocopy \\10.0.0.142\backup\wiki\data *.* \\drobo\common\Projects\wiki_backups\backups\wiki\data *.* /s /r:2 /w:3 /z /mir
@robocopy \\10.0.0.142\backup\wiki\data *.* \\rlb-sysadmin002\c$\backup\bak\wiki\data *.* /s /r:2 /w:3 /z /mir
@robocopy \\10.0.0.142\backup\wiki\data *.* C:\Bitnami\dokuwiki-20150810-0\apps\dokuwiki\htdocs\data *.* /s /r:2 /w:3 /z /mir

#----------------------------------------------------------
#              date and time
#----------------------------------------------------------
get-date -format yyMMdd
( get-date ).DayOfWeek | gm -f
(Get-Date).DayOfWeek
( get-date ).DayOfWeek.value__
[Int] (Get-Date).DayOfWeek  # returns 0 through 6 for current day of week
Get-Date -UFormat %u
(get-date).dayofyear
(Get-Date).AddDays(-1)
(Get-Date).AddDays(-0).ToString('yymmdd')
$a = Get-Date
"Date: " + $a.ToShortDateString()
"Time: " + $a.ToShortTimeString()
$yesterday = get-date.AddDays(-1)
$a = (Get-Date).AddDays(7)
Get-Date $a -Format yyMMdd
$a = Get-Date (get-date).adddays(7) -format yyyyMMddd
$time = get-date -format hh:mm
#----------------------------------------------------------
#              searching files
#----------------------------------------------------------
# swiftco files that call copssh
Get-ChildItem -Path C:\RLMS\apps\* -include *.bat,*.ps1, *.vbs | select-string cop | select path

#----------------------------------------------------------
#              zip
#----------------------------------------------------------
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
$Source = "Y:\IT\inventory\swr_baseline"
$Target = "Y:\IT\inventory\swr_baseline_copy.zip"
sz a -mx=9 $Target $Source


#----------------------------------------------------------
#              what is this?
#----------------------------------------------------------
$colTextFiles = get-childitem e:\temp10050 -filter *.txt -recurse | % { $_.FullName}

foreach ($i in $colTextFiles)
{
	Add-Content e:\temp10050\merge.txt "`n$i`n"
	$fileStuff = Get-Content $i
	Add-Content e:\temp10050\merge.txt $fileStuff
}
#----------------------------------------------------------
#              executables
#----------------------------------------------------------
http://edgylogic.com/blog/powershell-and-external-commands-done-right/

$arg1 = "xxx"
$arg2 = "$yyMMdd"
$allArgs = @($arg1, $arg2)
$batch
#write-host $batch $allArgs
& $batch $allArgs

#calling powershell
If you miss the functionality of allowing a user to double-click a Powershell script to execute use the following as a work-around:
Create a new shortcut For target enter “powershell.exe -noexit 'path-to-your-script'

powershell.exe -noexit 'path-to-your-script'

-WindowStyle Hidden

-NoProfile

#----------------------------------------------------------
#              ftp
#----------------------------------------------------------

Import-Module PSFTP

$FTPServer = 'ftp.host.com'
$FTPUsername = 'username'
$FTPPassword = 'password'
$FTPSecurePassword = ConvertTo-SecureString -String $FTPPassword -asPlainText -Force
$FTPCredential = New-Object System.Management.Automation.PSCredential($FTPUsername,$FTPSecurePassword)


Set-FTPConnection -Credentials $FTPCredential -Server $FTPServer -Session MySession -UsePassive
$Session = Get-FTPConnection -Session MySession

Get-FTPChildItem -Session $Session -Path /htdocs #-Recurse

#----------------------------------------------------------
#            networking plus  softperfect network scanner
#----------------------------------------------------------
#log all installed software
(from softperfect xml)
$data = [xml]$netmap = Get-Content 'c:\backup\current.xml'
foreach($_ in $data.'network-scanner-result'.devices.item.hostname )
{
$cname = $_
Get-WmiObject -Class Win32_Product -ComputerName $cname | Select-Object -Property Name > "Y:\IT\inventory\swr_baseline\$cname.txt"
}


test-connection rlbftp2.rli.local

$socket = new-object Net.Sockets.TcpClient
$socket.Connect("rlbftp2.rli.local",21)
$socket.Connected


#----------------------------------------------------------
#              copying setup files to machines
#----------------------------------------------------------
# gather installed software using softperfect network scanner and export as xml
# query that xml for machines that have quickbooks
# copy quickbooks setup file to machines
[xml]$netmap = Get-Content  'C:\backup\seattle.xml'
$qcomputers = $netmap.'network-scanner-result'.devices.item | ? {$_.apps -like '*Quickbooks*'} | select hostname
$setupfile =  "\\sea-r320-26.rli.local\c$\QuickBooks\16\Setup_QuickBooksEnterprise16.exe"
#$setupfile = 'U:\16\Setup_QuickBooksEnterprise16.exe'
foreach ($_ in $qcomputers)
{
[string]$host = $_.hostname
if (!(Test-Path "`\`\$host`\c`$`\rli"))
{
write-hoste "does not exist"
New-Item -path "`\`\$host`\c`$`\rli" -type directory -Force
}
if (Test-Path "`\`\$host`\c`$`\rli")
{
# copy the file to folders, example destination d:\vision\bank\123\parms
# write-host "$file `\`\$host`\c`$`\rli"
copy-item $setupfile "`\`\$host`\c`$`\rli"
}
}


New-Item -path "$boxfolder`\job.sent" -type file
Invoke-Item "$boxfolder`\job.sent"


#----------------------------------------------------------
#              reading content of files
#----------------------------------------------------------

$reader = [System.IO.File]::OpenText("C:\backup\tickets\installs\adminshares.txt")
try {
    for(;;) {
        $line = $reader.ReadLine()
        if ($line -eq $null) { break }
        # process the line
        $line | gm
    }
}
finally {
    $reader.Close()
}

#----------------------------------------------------------
#              csv validation
#----------------------------------------------------------
C:\backup\csv_validation.ps1

# example of pulling tif paths out of index and testing path
$tpath.clear
$indx.clear
$matches.clear

$tpath = '\E:\\\d{6}\\\d{4}\\docs\\\d{3}\\s\d{10}\.tif'
#$indx = 'E:\151102\1619\afs1619.CSV'
$indx = 'E:\151102\1619\afs1619N.CSV'
$matches = select-string -Path $indx -Pattern $tpath  -AllMatches | % { $_.Matches } | % { $_.Value }
foreach($_ in $matches)
{
$_ + " = " + (test-path $_)
}

# example of pulling tif paths out of index and testing path
$tpath = '\D:\\\d{6}\\\d{4}\\docs\\\d{3}\\s\d{10}\.tif'
$indx = 'C:\backup\test.csv'
$matches = select-string -Path $indx -Pattern $tpath  -AllMatches | % { $_.Matches } | % { $_.Value }
$matches.Count
foreach($_ in $matches)
{
test-path $_ | select *
}

#----------------------------------------------------------
#              string manipulations
#----------------------------------------------------------
http://www.lazywinadmin.com/2013/10/powershell-get-substring-out-of-string.html
"OU=MTL1,OU=CORP,DC=FX,DC=LAB" | get-member
("OU=MTL1,OU=CORP,DC=FX,DC=LAB").split(',')
"OU=MTL1,OU=CORP,DC=FX,DC=LAB" -split ','
("OU=MTL1,OU=CORP,DC=FX,DC=LAB" -split ',')[0]
("OU=MTL1,OU=CORP,DC=FX,DC=LAB" -split ',')[0].substring(3)

Solutions proposed by readers

Jay
'OU=MTL1,OU=CORP,DC=FX,DC=LAB' -match '(?<=(^OU=))\w*(?=(,))'
$matches[0]

Robert Westerlund
"OU=MTL1,OU=CORP,DC=FX,DC=LAB" -match "^OU=(?<MTL1>[^,]*)"
$matches["MTL1"]


$M = gci -path C:\server.2.log | Select-String -pattern "something"
foreach ($_ in $M)
{
#match up to 9 digits in brackets
$_ -match "[\[]{1}\s[0-9]{0,9}[\]]{1}"
$matches.Values
}
#----------------------------------------------------------
#              security
#----------------------------------------------------------

netsh http show sslcert
#----------------------------------------------------------
#              bginfo
#----------------------------------------------------------

C:\Windows\Bginfo.exe C:\tools\rliserver.bgi  /timer:0 /nolicprompt /silent


bginfo
- Create a new custom field of type WMI Query

- In the Path field type: SELECT LastBootUpTime FROM Win32_OperatingSystem

- Add the custom field

#----------------------------------------------------------
#              email
#----------------------------------------------------------


https://4sysops.com/archives/send-mailmessage-paramaters-and-examples/

<https://raw.githubusercontent.com/JurgenVM/Cheatsheets/master/cheatsheets/powershell.md>

# Must have commands
| Command							| Meaning
| :---								| :---
| `gsv | clip`						| Clips output of `gsv` to the clipboard


# Basic commands
| Command							| Meaning
| :---								| :---
| `cls` & `Clear-Host`				| clear the screen
| `cd`								| change directory
| `cd\` 							| go to root directory
| `dir` & `ls` & `Get-Childitem` 	| shows whats in the directory
| `cat` & `Get-Content`				| output the file
| `Copy` & `cp` & `Copy-Item`		| copy the file
| `Measure-command {Get-Process}`	| Shows you the execution time

# Help system
| Command															| Meaning
| :---																| :---
| `Update-Help -Force`												| Update the help
| `Save-Help`														| Save the help to a local file
| `man`																| Scrollable
| `help`															| Alias
| `Get-Help "*keyword*"` 											| Get help
| `Get-Help Get-*` 													| Get all commands which start with "Get-"
| `Get-Help`														| `-ShowWindow`
| 																	| `-Detailed`
|																	| `-Examples`
| 																	| `-Full`
| 																	| `-Online`
| 																	| `-ShowCommand`
| `Get-Help -Category Provider`										|
| `Get-verb`														| Get the verbs used in PowerShell
| `Get-Alias -Definition get-process` 								| Get all aliases for `Get-Process`
| `Get-Help About_*`												| Get all the about topics
| `Get-Process | Get-Member`										| Get all the methods, properties,... of the object
| `Get-Command -Module AD*`											| Get all commands from modules which start with AD
| `Get-Help about* | Out-GridView –PassThru | Get-Help –ShowWindow` | This will display a grid view with all about topics to choose from. Select one, and click OK, to view the help file.

# Export

| Command			| Meaning
| :---				| :---
| `Export-csv`		|
| `Export-Clixml`	|
| `Out-File`		|
| `Out-Printer`		|
| `Out-GridView`	|
| `Out-File`		|
|					|
| `ConvertTo-Csv`	|
| `ConvertTo-Html`	|

# Pipeline

**Example: you want to check if a service is running on all the computers in the AD <br />**

| Command								| Meaning
| :---									| :---
| `Get-ADComputer -Filter * 	| gm`	| Check what object you are working with (in this case `ADComputer`)
| `Get-Help Get-Service -ShowWindow`	|
| 										| Search for `-InputObject` and see what it accepts (in this case `ServiceController` != `ADComputer`)
|										| `-ComputerName` supports `ByPropertyName`
| `Get-ADComputer -Filter *`			| Check for propertyname
|										| `-Name` gives you the name of the computer

`Get-ADComputer -Filter * -Name | Select -Property @{n(ame)='ComputerName';e(xpression)={$_.name}} 	| Get-Service -Name bits`<br />

**Example: command does not accept pipeline input (Get-WmiObject)<br />**

`Get-WmiObject -class win32_bios -ComputerName (Get-ADComputer -Filter *).Name`

_Or_

`Get-ADComputer -Filter * | Get-WmiObject win32_bios -Computername {$_.Name}`

# Create new property

| Command																| Meaning
| :---																	| :---
| `Select -Property @{n(ame)='ComputerName';e(xpression)={$_.name}}`	| Creates a new property called `ComputerName` from the existing property `Name`
| `Select -ExpandProperty name`											| Return the property in a array

# WmiObject & CimInstance

# Remoting
| Command																| Meaning
| :---																	| :---
|`Invoke-Command -ComputerName dc {Restart-Computer}`					| Execute the command on specific computers
| `icm dc {Restart-Computer}`											| Short version

# Execution Policy

| Command			| Meaning
| :---				| :---
| `Restricted`		|
| `Unrestricted`	| Run any script **(not recommended)**
| `AllSigned`		|
| `RemoteSigned`	| Only run scripts that are remote signed
| `Bypass`			|
| `Undefined`		|

# Powershell Remote Session

| Command										| Meaning
| :---											| :---
| `Get-PSSession`								| Get open sessions
| `$sessions = New PSSession -ComputerName dc`	| Open session
| `icm -Session $sessions {...}`				| Execute command in script block to all sessions

# Parallelism

**Parallel:** <br />
 `icm -Computername (Get-Content servers.txt) { ... }` <br />

**Serial:** <br />
 `foreach ($s in (Get-Content servers.txt) ) { icm -Computername $s { ... }}`

# Scripting

| Command													| Meaning
| :---														| :---
| `CTRL + J`												| Helps you build a script
| `$var = 1`												| Declares new variabele `var` with value `1`
| `param($ComputerName)`									| Create new parameter
| `[ValidateSet("a","b","c")][string]$x = "a"`				| Value of $x **must** be a value of the ValidateSet
| `"this is $var"`											| `"` Resolves the value of $var
| `'this is $var'`											| `'` Does **not** resolve the value
| `"The value of &#96;$var is $var"`						| the &#96; (`&#96;`) prevents that the value is resolved
| `@' '@`													| Multiline string, usefull for `New-Snippet`
| `-ErrorAction`											|
| `-ErrorVariabele`											|

######################################
# yaml

Import-Module C:\backup\apps\PowerYaml-master\PowerYaml.psm1

$yaml = Get-Yaml -FromFile (Resolve-Path C:\backup\apps\PowerYaml-master\sample.yml)
$yaml.parent.child



######################################
#    monitoring
######################################

https://code.google.com/p/internetconnectivitymonitor/

# group policy
see effective
http://www.howtogeek.com/116184/how-to-see-which-group-policies-are-applied-to-your-pc-and-user-account/
monitor
https://social.technet.microsoft.com/forums/windowsserver/en-US/9a65b012-e4fd-427f-a4ce-d77823e4f14c/find-group-policy-change-history
# add domain to local admin
http://social.technet.microsoft.com/wiki/contents/articles/7833.how-to-make-a-domain-user-the-local-administrator-for-all-pcs.aspx
#locally
add the domain user to the local administrator group,
to do this right click on computer go to manage then expand the system tools tab,
then go to users and groups, on selecting groups go to the administrators group right click on it
and go to properties go to add and type in the domain user you need to add.
######################################
#    outlook
######################################

public sharing
https://technet.microsoft.com/en-us/library/aa998834(v=exchg.141).aspx


######################################
#    compare
######################################



$all = get-content 'c:\trash\all'
$internal = get-content 'C:\trash\internal'
$deactivates = Compare-Object $all $internal  | Where-Object { $_.SideIndicator -eq '<=' }
$deactivates >> 'C:\trash\deactivate.txt'

(get-content 'c:\trash\all').count

(get-content 'C:\trash\internal').count


######################################
#    ip
######################################

$input_path = ‘c:\ps\ip_addresses.txt’
$output_file = ‘c:\ps\extracted_ip_addresses.txt’
$regex = ‘\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b’
select-string -Path $input_path -Pattern $regex -AllMatches | % { $_.Matches } | % { $_.Value } > $output_file
######################################
http://www.hanselman.com/blog/ScottHanselmans2014UltimateDeveloperAndPowerUsersToolListForWindows.aspx
######################################
#    internal app examples
######################################


web50000.exe E:\Images\151221\10063\manual\manualupload.csv 10063 web50000.xml

afsimport.bat box yyyymmdd

http://www.retaillockbox.supportsystem.com/scp/tickets.php?id=3711

00152
C:\RLMS\apps>createCycloneach2.bat 00152 12 22 2015


######################################
#    calling function
######################################

===Calling functions from another script===
Let's say that you have one script that has the functions that you want to use called function.ps1.
In another script, say testfunctions.ps1 you can call the functions in function.ps1.  Here is how.

Make a ps1 file with the following contents and call the file function.ps1
<code>
function F1{write-host "Here it is";}
function F2{write-host "Here it is again";}
</code>

Next write another ps1 file with the following contents and call the file testfunctions.ps1
<code>
. C:\users\darren.hoehna\desktop\functions.ps1 #The spaces of the dots before C:\ needs to be there.  So the start of
#path is .(space)C:\\

F1
F2
</code>

When you run testfunctions.bat this should be the output:\\
Here it is\\
Here it is again

-----
[[it:retailwebroot | Back to RetailWEB Index]]\\


######################################
#    sleep in batch
######################################
example E:\rlbobj\epay3576.ach.bat
@set SCRIPTF=%temp%\~%BOX%scp.vbs
:Cont

@echo wscript.sleep 10000 >%SCRIPTF%
@start /w wscript.exe %SCRIPTF%
@del %SCRIPTF%


######################################
#    qdir
######################################
"%Program Files%\Q-Dir\Q-Dir.exe" "Z: \"

######################################
#    rdp
######################################
see if its enabled

netsh advfirewall firewall show rule name="Remote Desktop (TCP-In)"
https://support.pertino.com/hc/en-us/articles/200488829-How-to-enable-remote-desktop-from-Windows-command-line-CMD-
##############################
# http://wiki/doku.php?id=it:serverinfo:hosted:swiftco-vsa8xyg
##############################
# to generate this list
Get-ChildItem -Path C:\RLMS\apps\* -include *.bat,*.ps1, *.vbs | select-string cop | select path | out-file c:\rlms\apps\calls_cop.txt

##############

$folder = 'c:\backup'
$filter = '*.*'
$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}
$onCreated = Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated -Action {
$name = $Event.SourceEventArgs.Name
$timeStamp = $Event.TimeGenerated
Send-MailMessage -To shane.null@retaillockbox.com -From shane.null@retaillockbox.com  `
-Subject "File Added" -Body "$name was uploaded on $timestamp" -SmtpServer '10.0.0.12'
}


###################
# sql


function Invoke-SQL {
    param(
        [string] $dataSource = "SEA-2950-22.\SQLEXPRESS",
        [string] $database = "MasterData",
        [string] $sqlCommand = $(throw "Please specify a query.")
      )

    $connectionString = "Data Source=$dataSource; " +
            "Integrated Security=SSPI; " +
            "Initial Catalog=$database"

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($sqlCommand,$connection)
    $connection.Open()

    $adapter = New-Object System.Data.sqlclient.sqlDataAdapter $command
    $dataset = New-Object System.Data.DataSet
    $adapter.Fill($dataSet) | Out-Null

    $connection.Close()
    $dataSet.Tables

}


################


get db names
dir SQLSERVER:\\SQL\sea-2950-22.rli.local\Default\Databases | select name


Using SQL with Powershell

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection #Do not modify.  This makes an object that holds the
connection to the SQL Server
$SqlConnection.ConnectionString = "Data Source=SEA-TESTDB\RLITEST;Initial Catalog=grouphealth;Persist Security Info=True;
User ID=<User ID>;Password=<Password>;"


$SqlCmd = New-Object System.Data.SqlClient.SqlCommand #Do not modify.  This makes the object that holds the SQL Query
$SqlCmd.CommandText = "select DISTINCT City from grouphealth.dbo.statementdetail" #This is the command that you want SQL to do.
$SqlCmd.Connection = $SqlConnection

$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter #Don't modify.  This makes a new object that holds the
adapter for the SQL server.
$SqlAdapter.SelectCommand = $SqlCmd

$DataSet = New-Object System.Data.DataSet
$numberOfRows = $SqlAdapter.Fill($DataSet)

$SqlConnection.Close() #Always make sure to close the connection
To access data in a cell:

 $DataSet.Tables[0].Rows[0]["City"]

 #find active directory users
  Get-Module –ListAvailable

  #Detailed error message
trap
{
   throw  ($_ | format-list * -force | out-string)
}
#Detailed error message
try{
    #run your code here
}
catch
{
   throw  ($_ | format-list * -force | out-string)
}
############################################################
# 2498 esd cheater test-remove remove files function
############################################################
<#frequent tasks for ach duplicates
#open qdir
$qdirpath = gci -path e:\tools -filter *.qdr -r | ? {$_.name -match "$box*"} | select fullname
if(test-path $qdirpath.FullName){invoke-item $qdirpath.fullname}

#give a date or use today
#$yymmdd = read-host -prompt 'input date yymmdd'
$yymmdd = get-date -Format yyMMdd
#zip a copy of the folder
$box = 2498

# calling backup script, run Backupe to create copy.zip
. c:\backup\backupefolder.ps1
Backupe
pause #pause before removing
$boxfolder = 'e:\' + $yymmdd + '\' + $box

#remove files to be recreated
test-path
E:\160302\2498\160302_2498E:\160302\2498\160302_2498
$htm = $boxfolder + '\' + $yymmdd + '_' + $box + '_ACH.htm'
$ACH =  $boxfolder + '\' + $yymmdd + '_' + $box + '_ACH.dat'
$xml =  $boxfolder + '\' + $yymmdd + '_' + $box + '_Output.xml'

function test-remove
{
param($filepath)
if(Test-Path($filepath)){
write-host "removed $filepath"
Remove-Item -Path $filepath}
else{write-host "$filepath doesn't exist"}
}
test-remove $htm
test-remove $ACH
test-remove $xml

#>



#module that runs exe for box numbers in xml file, validates yymmdd
<pre class="line-numbers">
<code class="language-powershell">


<#
Automation
job host:
job software:
job folder:
job name:
job time:
job called by:
job calls:
start-process "http://sea-1750-79:9675/tickets/v2#recently_updated/14362"
start-process "http://wiki/doku.php?id=it:software:rlms2retailweb"
validate parameter tips
start-process http://powershell.com/cs/blogs/tips/archive/2013/07/19/using-validatepattern-attribute.aspx
This can be used as a module
#>



function Import-RlmsToRliAll {
  <#
  .SYNOPSIS
  runs e:\rlbobj\rlm20000.exe for all boxes in E:\Accounts\rlm200.xml
  .EXAMPLE
  Import-RlmsToRliAll 20160226
  .PARAMETER
  yymmdd
  #>
  [CmdletBinding()]
  param
  (
     [Parameter(Mandatory=$true,HelpMessage='to pass date use yymmdd')]
     [ValidatePattern('(?# date uses yymmdd)^[0-9][0-9]([0][1-9]|[1][1-2])[0-3][0-9]$')]
     [String] $yymmdd
  )
  begin {
    #if(-not $yymmdd){$yymmdd = get-date -format yyMMdd}
    $rlmsrliconfigfile = 'E:\Accounts\rlm200.xml'
    [xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile
    #$rlmsrlixml.achinfo.box.number
  }
  process {
        foreach($_ in $rlmsrlixml.achinfo.box.number)
        {
        #write-host "do stuff with e:\rlbobj\rlm20000 e:\$yymmdd\$_\ $yymmdd $_ "
        $arg0 = "e:\$yymmdd\$_\"
        $arg1 = "$yymmdd"
        $arg2 = "$_"
        $allArgs = @($arg0,$arg1,$arg2)
        #$rlm200 = 'E:\rlbobj\rlm20000.exe'
        $rlm200 = 'e:\rlbobj\test.exe'
        $CommandLine = "& $rlm200 $allArgs"
        write-host "do this $CommandLine "
        #Invoke-Expression $CommandLine e:\rlbobj\rlm20000 e:\160226\70260\ 160226 70260
        sleep -Seconds 5
        }
    }
}

#variable tests
#no variable
#Import-RlmsToRliAll
#invalid
#Import-RlmsToRliAll 20160101
#valid
#Import-RlmsToRliAll 160101

<#
info gathering

# find customers
$rlmsrliconfigfile = 'E:\Accounts\rlm200.xml'
[xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile
#client
$rlmsrlixml.achinfo.box.client | clip
#box
$rlmsrlixml.achinfo.box.number

# find scripts

#search in files http://wiki/doku.php?id=it:software:rlms2retailweb
$pattern = 'rlm20000'
Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
.examples
e:\rlbobj\rlm20000 e:\160226\70260\ 160226 70260

@call e:\rlbobj\rlm20000 %RMSDATA% %DATE% %RBOX%
@set DATE=%FULLDATE:~2%
@set WORKPATH=e:\%DATE%\%RBOX%
@set RMSDATA=%WORKPATH%\rms%BOX%.dat
@set RBOX=70260


#>




</code>
</pre>

----

invoke-item C:\backup\home\projects\tickets\tickets.ps1

# clean ss export

$source = "C:\backup\home\projects\tickets\ss.csv"
$destination = "C:\backup\home\projects\tickets\sscleaned.csv"
(Import-CSV $source  |
    Select "Ticket Number","Subject","Date Created","Last Updated","id","category","duration","done","content_type","object_id","1","2" |
    ConvertTo-Csv -NoTypeInformation) |
    # Select-Object -Skip 1) -replace '"' |
    Set-Content $destination

# create csv

$csvContents = @() # Create the empty array that will eventually be the CSV file

$row = New-Object System.Object # Create an object to append to the array
$row | Add-Member -MemberType NoteProperty -Name "User" -Value "John Doe" # create a property called User. This will be the User column
$row | Add-Member -MemberType NoteProperty -Name "UserID" -Value "JD01" # create a property called UserID. This will be the UserID column
$row | Add-Member -MemberType NoteProperty -Name "PC" -Value "PC01" # create a property called PC. This will be the PC column



$csvContents += $row # append the new data to the array

$csvContents | Export-CSV -NoTypeInformation -Path C:\backup\home\projects\tickets\user.csv

#The code above will result in the following output
#
##TYPE System.Object
#"UserName","UserID","PC"
#"John Doe","JD01","PC01"
#
#Related Posts


----

param(
  $firstparameter
)
$passparameter = $firstparameter + 1

$mdy = get-date -format "MMM-d-yyyy"
write-host $firstparameter $mdy

Start-Process "E:\tools\steven\batch-tests\2016-11-30\fix.bat" $passparameter


----
@set inputparameter=%1
@set rundate=%2




echo parameter for batch file is %1

@SET MERGESCRIPT=E:\tools\steven\batch-tests\2016-11-30\fix.ps1
echo calling %MERGESCRIPT%
@powershell -NoProfile -ExecutionPolicy Bypass %MERGESCRIPT% %inputparameter%

@SET MERGESCRIPT2=E:\tools\steven\batch-tests\2016-11-30\fix2.ps1
@powershell -NoProfile -ExecutionPolicy Bypass %MERGESCRIPT2% %inputparameter%

----

# param passing

REM this calls powershell with a parameter
REM then powershell calls this with a new parameter
REM usage 
REM test.bat 1 
REM watch for new powershell window to open, it increments the parameter and loops back

SET p1=%1
pause
echo %p1%

@SET script=C:\backup\home\projects\2016-11-30\parameterpass\fix2.ps1
@powershell -NoProfile -ExecutionPolicy Bypass %script% %p1%



----
param(
  $firstparameter
)
$passparameter = $firstparameter + 1

$mdy = get-date -format "yyMMdd"
write-host $firstparameter $mdy

Start-Process "C:\backup\home\projects\2016-11-30\parameterpass\test.bat" $passparameter
----



#backups

invoke-item C:\backup\core\jobs\kodakbackup.ps1
invoke-item C:\backup\kodak\kodak.ps1

# installs


[xml]$netmap = Get-Content  'C:\backup\seattle.xml'
$netmap.'network-scanner-result'.devices.item | ? {$_.apps -like '*KODAK Capture Pro Software*'} | select hostname


#kodak apps backup one
if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe" 
$source = '\\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps'
$target = "E:\tools\Infrastructure\kodaks\backups\rlb221-scan007-apps.zip"
sz a -mx=9 $target $source 


# backup function
function backup-kodak($computer)
{
if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {throw "$env:ProgramFiles\7-Zip\7z.exe needed"} 
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe" 
$source = "\\$computer\c$\kodak\xvcs6c\Apps"
$target = "E:\tools\Infrastructure\kodaks\backups\$computer`-apps.zip"
sz a -mx=9 $target $source 
}
$computer ='rlb221-scan007.rli.local'
$computer = 'rlb220-test001.rli.local'
$computer = 'rlb220-scan003.rli.local'
$computer = 'rlb220-index008.rli.local'
backup-kodak($computer)

#info

\\rlb220-test001.rli.local\c$\kodak\xvcs6c\Apps
\\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps
\\rlb220-scan003.rli.local\c$\kodak\xvcs6c\Apps
\\rlb220-index008.rli.local\c$\Kodak\xvcs6c\Apps

$installs = get-content C:\backup\kodak\installs.md
foreach($_ in $installs)
{
Get-ChildItem -Path $_
}

gci \\rlb220-test001.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-test001.rli.local.txt
gci \\rlb221-scan007.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb221-scan007.rli.local.txt
gci \\rlb220-scan003.rli.local\c$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-scan003.rli.local.txt
gci \\rlb220-index008.rli.local\c$\Kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-index008.rli.local.txt
gci \\rlb220-scan002.rli.local\C$\kodak\xvcs6c\Apps | select name |  out-file E:\tools\Infrastructure\kodaks\rlb220-scan002.rli.local.txt
$ref = get-content 'E:\tools\Infrastructure\kodaks\rlb220-scan002.rli.local.txt'
$1 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-index008.rli.local.txt'
$2 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-scan003.rli.local.txt'
$3 =  get-content 'E:\tools\Infrastructure\kodaks\rlb220-test001.rli.local.txt'
$4 =  get-content 'E:\tools\Infrastructure\kodaks\rlb221-scan007.rli.local.txt'

Compare-Object $ref $1
Compare-Object $ref $2
Compare-Object $ref $3
Compare-Object $ref $4
Compare-Object $ref $5

----

> 2018-04-05

fs cant send 5164 crash says 

search for afstc.consoleimp

`Get-ChildItem -Path \\sea-r320-30\c$\24x7\Output\ -r | Select-String -pattern 'afstc' |select Path`

# call python with params

`c:\flowstate\work\web\flask_sites\skeleton\projects\python_cli_boilerplate\powershell`
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2018-05-01 15:56
           
<https://github.com/Jaykul/Reflection>

# do things with file paths in a csv

<pre class="line-numbers">
<code class="language-powershell">

$scripts = 'scripts.csv'
$scriptsdata = Import-Csv -Path $scripts

$scriptsdata | ForEach-Object {  
    Copy-Item -Path $_.path -Destination '.\scripts\'
}
</code>
</pre>
<br><i class="fa fa-calendar" aria-hidden="true"></i> 2018-08-02 16:21
           
<https://stackoverflow.com/questions/33123337/itextsharp-to-merge-pdf-files-in-powershell>

# TODO:10 clean this +cheatsheets
snippets: pscom
<#
Automation
job host: sea-r320-30
job software: 24x7 scheduler
job folder: qa scripts
job name: 3576 kappes ach alert
job time: 9:10am
job called by:
job calls:
#>

<#

powershell cheatsheet
c:\backup\powershell.ps1
http://rlb-sysadmin002:85/grav-admin/cheatsheets/cheatsheets-item-2
#>
# http://powershell.com/cs/blogs/ebookv2/default.aspx

#----------------------------------------------------------
#                    installing and using powershell
#----------------------------------------------------------

$PSVersionTable.PSVersion
$Env:PSModulePath
Import-Module ServerManager
Add-WindowsFeature PowerShell-ISE


# profile

https://msdn.microsoft.com/en-us/library/windows/desktop/bb613488(v=vs.85).aspx

#update 2 to 30 cmd line
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%systemdrive%\chocolatey\bin
cinst powershell


#check policy on remote pc
New-PSSession –Computer sea-r320-30.rli.local | Get-ExecutionPolicy
#registry
reg query  HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell
Get-ExecutionPolicy -List | Format-Table -AutoSize


# get .net version

$PSVersionTable.CLRVersion

http://www.howtogeek.com/117192/how-to-run-powershell-commands-on-remote-computers/


#get more detail on functions
get-command mkdir | Select-Object -ExpandProperty definition

#logging
#write the name of the script
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition
#pslogging
Import-Module pslogging
Start-Log -LogPath 'E:\tools\Infrastructure\schedulers' -LogName pslog.txt -ScriptVersion 1
Write-LogInfo $MyInvocation.MyCommand.Definition
& "C:\Program Files\TeraCopy\TeraCopy.exe"  copy \\sea-r320-30\c$\24x7\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reports /OverwriteOlder /Close
& "C:\Program Files\TeraCopy\TeraCopy.exe"  copy \\204.15.228.182\c$\tools\reportsswiftcosched \\10.0.0.142\c$\inetpub\wwwroot\static\reportsswiftco  /OverwriteOlder /Close
& "C:\Program Files\TeraCopy\TeraCopy.exe"  copy \\10.0.0.72\c$\tools\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reportsoldscheduler  /OverwriteOlder /Close
stop-log -LogPath 'e:\tools\infrastructure\pslog_main.txt' -NoExit

#transcript
start-transcript -path c:\backup\transcript.txt
#help stop-log -examples
#log this filename http://stackoverflow.com/questions/817198/how-can-i-get-the-current-powershell-executing-file

# end
gci|select mode,attributes -u

Get-Module –ListAvailable
Import-Module ActiveDirectory

#find users in ad
http://www.tomsitpro.com/articles/powershell-active-directory-cmdlets,2-801.html
Import-Module ActiveDirectory
Get-ADDomain | clip

Get-ADUser something
Get-ADUser –Filter {Surname –eq “Jeremy”}
Get-ADUser –Filter {GivenName –eq “Jeremy”}
Get-ADPrincipalGroupMembership –Identity khess
#Remove-ADGroupMember –Identity “Domain Admins” –Member  “khess”
Get-ADPrincipalGroupMembership –Identity khess
New-ADUser –Name “Abby Jones” –GivenName Abby –Surname Jones –UserPrincipalName ajones@mw.local –SamAccountName ajones
Search-ADAccount –AccountDisabled –UserOnly |FT Name
Set-ADAccountPassword –Identity ajones –NewPassword (Read-Host –AsSecureString “New Password”) –OldPassword (Read-Host –AsSecureString “Old Password”)

Enable-ADAccount –Identity ajones
Search-ADAccount –LockedOut –UsersOnly |FT Name
Unlock-ADAccount –Identity khess
Get-ADUser -Filter * -SearchBase "ou=,dc=rli,dc=local"
Get-ADUser -Filter * -SearchBase "dc=rli,dc=local" | select samaccountname | clip
Get-ADGroupMember "production" | Format-Table samaccountname
##

#----------------------------------------------------------
#                    general syntax
#----------------------------------------------------------
get-verb

help something -online


$variable | select -first 1 -prop *

If (test) {execute when true}
If (test1 -Or text2) {execute when true}


I would recommend PowerShell remoting. But answer to question how? depends very much on environment (domain/ workgroup) and your rights (on server/ within domain).

Thats one command in AD environment:

Set-ADComputer -TrustedForDelegation $true -Identity <your target server>
And later:

Invoke-Command -ComputerName <target server> -ScriptBlock { your command }
#error handling
You have a couple of options. The easiest involve using the ErrorAction settings.

-Erroraction is a universal parameter for all cmdlets. If there are special commands you want to ignore you can use -erroraction 'silentlycontinue' which will basically ignore all error messages generated by that command.

If you want to ignore all errors in a script, you can use the system variable $ErrorAction and do the same thing: $ErrorActionPreference= 'silentlycontinue'

http://ss64.com/ps/syntax-automatic-variables.html

operators
http://ss64.com/ps/syntax-compare.html

# dl and run script
powershell -nop -c "iex(New-Object Net.WebClient).DownloadString('http://something.com')"
# run encoded https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/
$command = "Write-Host 'My voice is my passport, verify me.'" $bytes = [System.Text.Encoding]::Unicode.GetBytes($command) $encodedCommand = [Convert]::ToBase64String($bytes) powershell.exe -EncodedCommand $encodedCommand


# Create a hash table for the item and use double quotes
$myItem = @{
    type = 'Beer'
    price = '$6.00'
}

$myString = "The price of a $($myItem.type) is $($myItem.price)"
Write-Output $myString

# When our string contains double quotes

$myItem = @{
    name = 'Matt'
    age = 29
}

$myStringBackTicks = "The user `"$($myItem.name)`" is of age `"$($myItem.age)`""
$myStringDoubleQuotes = "The user ""$($myItem.name)"" is of age ""$($myItem.age)"""
Write-Output $myStringBackTicks
Write-Output $myStringDoubleQuotes
# another
$webData = @{
    name = 'Matt'
    fontcolor = 'red'
    type = 'beer'
    price = '6.00'
    age = 29
}


$htmlPage = "
<html>
<head>
<title>$($name.title)'s Web Page!</title>
</head>
<body>
<{0}>Welcome to $($webData.name)'s Web Page!</{0}>
<p>
<font color=""$($webData.fontcolor)"">$($webData.name)'s Age is ""$($webData.age)"".</font>
<p>
This means he can buy a $($webData.type) for `$$($webData.price)!
</body>
</html>
" -f 'h1'

Set-Content -Path C:\Temp\MyPage.html -Value $htmlPage


#see if customer sent us a duplicate filename used for water district
$ar = 'C:\ftproot\waterdistrict111\archive'
$in = 'C:\ftproot\waterdistrict111\inbound'

$ard = ls $ar -filter *.csv
$ind = ls $in -filter *.csv

compare-object $ard $ind -property name -includeEqual  | clip

compare-object $ard $ind -property name -includeEqual  | Where-Object { $_.SideIndicator -eq '==' }

C:\ftproot\waterdistrict111\archive\h2o_data_20160126.csv

#backup scheduler report with teracopy
teracopy.exe copy \\sea-r320-30\c$\24x7\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reports /OverwriteOlder /Close

#----------------------------------------------------------
#                find bad data from indexing
#----------------------------------------------------------
$validate8402folder = "E:\AFSImageMerge\160115\8402"
$xmlfiles = Get-ChildItem -Path $validate8402folder -filter *.xml | select fullname
foreach($_ in $xmlfiles)
{
[xml]$values = get-content $_.Fullname
$values.batch.page.field | ? {$_.id -eq 'Paid Amount'} | select value
}
#----------------------------------------------------------
#                image replacement
#----------------------------------------------------------
Please replace the image for DocID 647 with this image:
$20160224tif = 'L:\AfsImageMerge\160223\3656\2016022336560001\3656201602233656000100004.tif'
Copy-Item $20160224tif E:\tools\1000-4000\3656UW\20160224
import-module convertto-pdf
get-module
ConvertTo-Pdf E:\tools\1000-4000\3656UW\20160224
$20160224pdf = 'E:\tools\1000-4000\3656UW\20160224\3656201602233656000100004.pdf'
use rliimages_com
select DocumentPath from C000151WebDocuments
where documentid = 647 and
ConnectionID = 632
$20160224replacepath = '\\sea-NX3200-21\RetailWeb\dotcom\imgfldrs\3656\2016022322\3656201602233656000100004.pdf'
Copy-Item -path $20160224replacepath -destination E:\tools\1000-4000\3656UW\20160224\original
Remove-Item $20160224replacepath
copy-item $20160224pdf $20160224replacepath

<#----------------------------------------------------------
                   error handling
----------------------------------------------------------#>
https://rkeithhill.wordpress.com/2009/08/03/effective-powershell-item-16-dealing-with-errors/
<#----------------------------------------------------------
                    parameter examples
----------------------------------------------------------#>
#switch

$value = 1
switch ($value)
{
  1  { "Number 1" }
  2  { "Number 2" }
  3  { "Number 3" }
}

#what?


(delete webimport.sent)
afsimport.bat 3504 20150703
web50000.exe "E:\image_scans\RLIAccounting\2013 RLI Vendor - 1.txt" rliacct web50000.xml
some files ask for %0 which is afs use xxx
#----------------------------------------------------------
#                    grep  files and folders
#----------------------------------------------------------

#search multiple filetypes
#find passwords in scripts
$passwordexposedinscripts = gci -Path @("e:\rlbobj\*","e:\scripts\*")  -Include @("*.bat","*.ps1") -r | Select-String pwd -Context 2


dir -path "E:\rlbobj\" -filter *.ps1 | Select-String -pattern xml
$folder="E:\image_scans\RLIAccounting\2013 RLI Vendor - 2\"
Get-ChildItem  -path $folder | where-object {$_.extension -eq ".pdf"} | select fullname
Get-ChildItem -Path @("e:\rlbobj","e:\scripts") -filter '*1013*' | Select FullName
$d = get-date -format yyMMdd
Get-ChildItem  E:\151020\  | Where-Object {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object | select fullname >> c:\logs\$boxupload`.$d.txt
Get-ChildItem  E:\151020\  | Where-Object {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object | select name
#search previous dates
$e = 'e:\'
# name contains
Get-ChildItem -Path $path -Include *.dat -Recurse | Where { $_.Name.Contains($pattern) }
for ($i=10; $i -ge 1; $i--) {
$dt = (Get-Date).AddDays(-$i)
$ymd = Get-Date $dt -Format yyMMdd
$boxfolder1 = "$e$ymd"
$boxfolder1
foreach($_ in (ls -path $boxfolder1 | ? {$_.PSIsContainer -eq $true -and  $_.Name -match '^\b[1-8]\d{3}\b' }| Sort-Object ))
{
$boxfolder2 = "$boxfolder1\$_"
$boxfolder2
}
}
$regex.Matches('abc 1-2-3 abc 4-5-6') | foreach-object {$_.Value}
#find previous days work-around
#search previous dates
$e = 'e:\'
for ($i=10; $i -ge 1; $i--) {
$dt = (Get-Date).AddDays(-$i)
$ymd = Get-Date $dt -Format yyMMdd
$boxfolder1 = "$e$ymd"
#$boxfolder1
foreach($_ in (ls -path $boxfolder1 | ? {$_.PSIsContainer -eq $true -and  $_.Name -eq '8770' }| Sort-Object ))
{
$boxfolder2 = "$boxfolder1\$_"
$boxfolder2
}
}

#get time
Set-Location c:\test\
$colFiles = (get-childitem | Where {!$_.PSIsContainer})

ForEach ($i in $colFiles)
{
	Write-Host $i.CreationTime
}


#file owner

Get-ChildItem -path e:\160222\1852 | % {$_ | get-acl }

get-childitem "C:\windows\System32" -recurse | where {$_.extension -eq ".txt"} | % {
     Write-Host $_.FullName
}

#compare the content of two files
Compare-Object -IncludeEqual -ReferenceObject (Get-Content E:\160119\1165\160119_1165_ACH.dat) `
-DifferenceObject (Get-Content E:\160119\1165\copy\160119_1165_ACH.dat)


#rename file extensions
gci -path C:\backup\core\corewww -filter *.html | rename-item -newname {  $_.name  -replace '\.html','.shtml'  }

#compare days
$StartDate=(GET-DATE)

$EndDate=[datetime]”03/22/2016 00:00”

NEW-TIMESPAN –Start $StartDate –End $EndDate
#----------------------------------------------------------
#                    file and folder
#----------------------------------------------------------
# rename files with sequential names
Dir *.jpg | ForEach-Object  -begin { $count=1 }  -process { rename-item $_ -NewName "image$count.jpg"; $count++ }

# End

$yymmdd = get-date -Format yyMMdd
$yyyymmdd = get-date -Format yyyyMMdd
$mmddyyyy = get-date -Format MMddyyyy
$box = 1234

if(-not (test-path("E:\$yymmdd"))){
New-Item -ItemType Directory -Force -Path "E:\$yymmdd"}

if(-not (test-path("E:\$yymmdd\$box"))){
New-Item -ItemType Directory -Force -Path "E:\$yymmdd\$box"}



#backup scheduler report
teracopy.exe copy \\sea-r320-30\c$\24x7\reports \\10.0.0.142\c$\inetpub\wwwroot\static\reports /OverwriteOlder /Close
#backup wiki
teracopy.exe copy \\webapps\c$\inetpub\wwwroot\dokuwiki\data\pages C:\Bitnami\dokuwiki-20150810-0\apps\dokuwiki\htdocs\data\pages /OverwriteOlder /Close
#wbadmin
wbadmin.exe START BACKUP -backupTarget:\\somewhere -systemState -include:C: -allCritical -quiet
http://blogs.technet.com/b/11/archive/2008/08/16/mounting-backup-file-created-by-windows-2008-server.aspx
mount or restore
using ntdsutil
or disk management / mount

different hardware
http://www.wbadmin.info/articles/howto-bare-metal-restores-windows-server-2008-backup.html
#db backup swiftco-vsa8xyg

OPTION EXPLICIT

Dim cn, cmd, diffType

Set cn = CreateObject("ADODB.Connection")
Set cmd = CreateObject("ADODB.Command")

IF Wscript.Arguments.length > 0 THEN
	diffType = Wscript.Arguments(0)

	'cn.Open "SWIFTCO-VSA8XYG\RLMSSQLEXPRESS", "sa", "T1stCitD"
	cn.Open "Provider=sqloledb;Data Source=SWIFTCO-VSA8XYG\RLMSSQLEXPRESS;User Id=sa;Password=T1stCitD;"
	Set cmd.ActiveConnection = cn
	cmd.CommandText = "sp_backupalldb"
	cmd.CommandType = 4 'adCmdStoredProc
	cmd.Parameters.Refresh ' Ask the server about the parameters for the stored proc
	cmd.Parameters(1) = diffType ' 1 = full diff, 0 = partial
	cmd.Execute
ELSE
	msgbox "This script requires an argument of 0 or 1, 0 being partial diff, 1 being full diff. ¿Comprende?"
	wscript.quit
END IF


$f = 'C:\WACPDF\Input'
New-Item $f -type directory
Invoke-Item $f

# write to top of file (data from temp file)

Add-Content -Path c:\temp\push\RAWlist.txt -Value (Get-Content "C:\temp\push\Pushlist.txt")
invoke-item  c:\temp\push\RAWlist.txt
invoke-item C:\temp\push\Pushlist.txt
new-item -path c:\temp\push\RAWlist.txt -type file -force
new-item -path C:\temp\push\Pushlist.txt -type file -force
Add-Content -Path c:\temp\push\RAWlist.txt -Value (Get-Content "C:\temp\push\Pushlist.txt")

http://www.howtogeek.com/128680/how-to-delete-move-or-rename-locked-files-in-windows/

#recently modified
$dir = "U:\"
$latest = Get-ChildItem -Path $dir -r -include *.qbw | Sort-Object LastAccessTime -Descending | Select-Object -First 25
$latest.name
#map drive
http://www.howtogeek.com/132354/how-to-map-network-drives-using-powershell/
New-PSDrive –Name “E” –PSProvider FileSystem –Root “\\sea-2020-63\prod”

net use M: \\touchsmart\Share /Persistent:Yes
net use m: /delete /yes

# directory to xml, use for backup script...
$configFile = 'C:\backup\dev\alert\alert.xml'
$directory = "c:\backup\dev\alert\test\rlbobj.xml"
Get-ChildItem -Recurse e:\rlbobj | Export-CliXML $directory

Get-ChildItem -Recurse c:\backup | Export-CliXML $directory


#----------------------------------------------------------
#                    sym links http://www.howtogeek.com/howto/16226/complete-guide-to-symbolic-links-symlinks-on-windows-or-linux/
#----------------------------------------------------------
fsutil behavior query SymlinkEvaluation
mklink /H “C:\Users\Matthew\Desktop\ebook.pdf”  “C:\Before You Call Tech Support.pdf”
<#
#----------------------------------------------------------
remote registry
#>
sc \\computername start remoteregistry
\\RLB221-FS002\c$\Program Files\WinSCP
#----------------------------------------------------------
#                    wmi
#----------------------------------------------------------

# rebecca's computer is slow, she's got 32 bit
gwmi win32_operatingsystem  -ComputerName RLB-IMP002 | select osarchitecture
gwmi win32_processor -ComputerName RLB-IMP002 | select -first 1 | select addresswidth
#----------------------------------------------------------
#                    remote commands
#----------------------------------------------------------
http://www.howtogeek.com/117192/how-to-run-powershell-commands-on-remote-computers/
open pshell as admin to enable remoting Enable-PSRemoting -Force
psexec \\PC-Name -u user -p password -i -c setup.exe /S /v"ALLUSERS=1 /qb"
#----------------------------------------------------------
#                    ad
#----------------------------------------------------------
Get-ADUser shane.null -Properties * | Get-Member

Get-ADGroupMember -identity "IT" -Recursive | Get-ADUser -Property DisplayName | Select Name,ObjectClass,DisplayName﻿

Get-ADGroupMember "ops" | Format-Table samaccountname
#servers
Get-ADComputer -Filter {OperatingSystem -Like "Windows Server*"} -Property * | Format-Table Name,OperatingSystem,OperatingSystemServicePack -Wrap -Auto
#not server
Get-ADComputer -Filter {OperatingSystem -NotLike "*server*"} -Property * | Format-Table Name,OperatingSystem,OperatingSystemServicePack -Wrap -Auto

Get-ADComputer -Filter * -Property * | Select-Object Name,OperatingSystem,OperatingSystemServicePack,OperatingSystemVersion | Export-CSV E:\scripts\cheatsheets\AllWindows.csv -NoTypeInformation -Encoding UTF8
invoke-item E:\scripts\cheatsheets\AllWindows.csv


Get-ADComputer -Filter * -Property * | Select-Object Name | Export-CSV c:\backup\monitors\devices.csv -NoTypeInformation -Encoding UTF8
invoke-item c:\backup\monitors\devices.csv
# test map drives are up
Get-WmiObject -Class Win32_MappedLogicalDisk | % {[System.IO.Directory]::Exists("$($_.name)\")}
############# gt map remote
$ComputerName = "afs-vsu"

gwmi win32_mappedlogicaldisk -ComputerName $ComputerName | select SystemName,Name,ProviderName,SessionID | foreach {
 $disk = $_
 $user = gwmi Win32_LoggedOnUser -ComputerName $ComputerName | where { ($_.Dependent.split("=")[-1] -replace '"') -eq $disk.SessionID} | foreach {$_.Antecedent.split("=")[-1] -replace '"'}
 $disk | select Name,ProviderName,@{n="MappedTo";e={$user} }
}

###############
$computer =

get-content -path C:\Users\shane.null\Desktop\vpn\computers.csv
Get-WmiObject -Class win32_systemenclosure -ComputerName $computer |  Where-Object { $_.chassistypes -eq 9 -or $_.chassistypes -eq 10-or $_.chassistypes -eq 14})
#who is logged in
$SERVER = rlb-sysadmin002
query user /server:$SERVER

#logoff user
$server = hostname
$username = $env:USERNAME
$session = ((quser /server:$server | ? { $_ -match $username }) -split ' +')[2]

logoff $session /server:$server

#----------------------------------------------------------
#                    remote cmd / office license
#----------------------------------------------------------
open a cmd as admin on your computer

psexec \\hostname cmd

 #t hen you're on their machine, run these two commands to register the key and activate it

C:\Windows\System32\cscript.exe "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /inpkey:asdf-asdf-asdf-asdf-asdf

C:\Windows\System32\cscript "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /act

C:\Windows\System32\cscript.exe "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /inpkey:P4B6J-JFPRR-RGQY6-JPWQB-C49T6
C:\Windows\System32\cscript "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /act


P4B6J-JFPRR-RGQY6-JPWQB-C49T6

C:\Windows\System32\cscript.exe "C:\Program Files\Microsoft Office\Office14\ospp.vbs" /inpkey:86QQY-KRTDC-7JDTT-7733P-VH2K7
#----------------------------------------------------------
#                    laptop search
#----------------------------------------------------------
Get-WmiObject -Class win32_systemenclosure -ComputerName $computer |  Where-Object { $_.chassistypes -eq 9 -or $_.chassistypes -eq 10-or $_.chassistypes -eq 14})

Function Get-Laptop
{
 Param(
 [string]$computer = rlb220-test001.rli.local
 )
 $isLaptop = $false
 if(Get-WmiObject -Class win32_systemenclosure -ComputerName $computer |
    Where-Object { $_.chassistypes -eq 9 -or $_.chassistypes -eq 10 `
    -or $_.chassistypes -eq 14})
   { $isLaptop = $true }
 if(Get-WmiObject -Class win32_battery -ComputerName $computer)
   { $isLaptop = $true }
 $isLaptop
}

# end function Get-Laptop

# *** entry point to script ***

If(get-Laptop) { "it's a laptop" }
else { "it's not a laptop"}
#----------------------------------------------------------
#                    alerts
#----------------------------------------------------------


@e:\rlbobj\bmail -s 10.0.0.12 -p 25 -h -t fernando.salazar@retaillockbox.com -f RLBView@retaillockbox.com -a "%BOX% was sent on %YYYYMMDD%." -b "Shane, please verify this is working." >nul 2>&1

#----------------------------------------------------------
#                    software inventory
#----------------------------------------------------------

https://gallery.technet.microsoft.com/scriptcenter/Get-InstalledSoftware-Get-5607a465?tduid=(44a95684895b35d6f52023560348a780)(256380)(2459594)(TnL5HPStwNw-fS7b4k.jGUdhXqcx6xGNgw)()


#dot source the script (or add to your profile or a custom module):
    . "\\path\to\Get-InstalledSoftware.ps1"

#Get help on Get-ADGroupMembers
    Get-Help Get-InstalledSoftware -Full

#Pull all software from c-is-ts-91, c-is-ts-92
    Get-InstalledSoftware c-is-ts-91, c-is-ts-92

#Pull software with publisher matching microsoft and displayname matching lync from c-is-ts-91
    "c-is-ts-91" | Get-InstalledSoftware -DisplayName lync -Publisher microsoft

#Pull software with publisher matching Microsoft and DisplayName starting with Microsoft Office from c-is-ts-91 and c-is-ts-92
    Get-InstalledSoftware -ComputerName c-is-ts-91, c-is-ts-92 -DisplayName '^Microsoft Office' -Publisher Microsoft | Format-Table -AutoSize

#Pull software with published matching Citrix.  Note the uninstall strings we could use to automate uninstall
    Get-InstalledSoftware -ComputerName c-is-ts-91 -Publisher citrix | Format-Table -AutoSize


Write-Host STarting
$computers = Import-Csv "C:\Temp\computerlist.csv"

$array = @()

foreach($pc in $computers){
	Write-Host "PC name is $pc"
    $computername=$pc.computername

    #Define the variable to hold the location of Currently Installed Programs

    $UninstallKey="SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall"

    #Create an instance of the Registry Object and open the HKLM base key

    $reg=[microsoft.win32.registrykey]::OpenRemoteBaseKey('LocalMachine',$computername)

    #Drill down into the Uninstall key using the OpenSubKey Method

    $regkey=$reg.OpenSubKey($UninstallKey)

    #Retrieve an array of string that contain all the subkey names

    $subkeys=$regkey.GetSubKeyNames()

    #Open each Subkey and use GetValue Method to return the required values for each

    foreach($key in $subkeys){

        $thisKey=$UninstallKey+"\\"+$key

        $thisSubKey=$reg.OpenSubKey($thisKey)

        $obj = New-Object PSObject

        $obj | Add-Member -MemberType NoteProperty -Name "ComputerName" -Value $computername

        $obj | Add-Member -MemberType NoteProperty -Name "DisplayName" -Value $($thisSubKey.GetValue("DisplayName"))

        $obj | Add-Member -MemberType NoteProperty -Name "DisplayVersion" -Value $($thisSubKey.GetValue("DisplayVersion"))

        $obj | Add-Member -MemberType NoteProperty -Name "InstallLocation" -Value $($thisSubKey.GetValue("InstallLocation"))

        $obj | Add-Member -MemberType NoteProperty -Name "Publisher" -Value $($thisSubKey.GetValue("Publisher"))

        $array += $obj

    }

}

$array | Where-Object { $_.DisplayName } | select ComputerName, DisplayName, DisplayVersion, Publisher | ft -auto
#----------------------------------------------------------
#                    text manipulations
#----------------------------------------------------------
$idx = 'c:\index.csv'
(gc $idx).replace('.tif','.pdf')|sc $idx
open firewall to internal servers

psexec \\machine cmd

netsh advfirewall firewall add rule name="Firewall Off IP x Incoming" dir=in action=allow protocol=ANY remoteip=1.1.1.1
netsh advfirewall firewall add rule name="Firewall Off IP x Outcoming" dir=out action=allow protocol=ANY remoteip=1.1.1.1

#----------------------------------------------------------
#                    get bus date http://wiki/doku.php?id=it:software:getbusinessdate
#----------------------------------------------------------
E:\rlbobj>getbusinessdate.exe
Usage getbusinessdate.exe /offset=n | /IsFirst | /IsLast [/date=yyyymmdd] [/xml=
path]
/offset = positive or negative date offset
/IsFirst or /IsLast will return 1 if it is the First or Last business day of the
 month, 0 if not
/date = date to calculate offset, current date used if ommitted
/xml = path to holidays.xml, e:\rlbobj\holidays.xml used if ommitted
E:\rlbobj\getbusinessdate.exe
example for next day used in a batch file

@e:\rlbobj\getbusinessdate /offset=+1 /date=%YYYYMMDD%
#----------------------------------------------------------
#                 Removal of Work older than 31 days
#----------------------------------------------------------
Automatic Removal of Work older than 31 days

Open e:\scripts\maintenance\rlbftp2_file_delete.bat in a text editor.
Add another line to the script to include the following:

  robocopy \\rlbftp2\c$\ftpshare\%Directory Name%\ \\rlbftp2\c$\oldftpfiles\old_ftp_files\%Directory Name%\ /move /minage:31 /E /Z /R:2 /NP \\

Replace %Directory Name% so the full path should reflect the new Virtual Root you created earlier.
This will replace
  robocopy \\rlbftp2\c$\ftpshare\%Directory Name%\ \\rlbftp2\c$\oldftpfiles\old_ftp_files\%Directory Name%\ /move /minage:31 /E /Z /R:2 /NP \\
#----------------------------------------------------------
#              wiki backup and system backup
#----------------------------------------------------------
@ rem system backup
wbadmin.exe START BACKUP -backupTarget:\\SEA-R610-28\X -systemState -include:C: -allCritical -quiet

 @rem backup dokuwiki pages
@robocopy \\10.0.0.142\backup\wiki\data *.* \\drobo\common\Projects\wiki_backups\backups\wiki\data *.* /s /r:2 /w:3 /z /mir
@robocopy \\10.0.0.142\backup\wiki\data *.* \\rlb-sysadmin002\c$\backup\bak\wiki\data *.* /s /r:2 /w:3 /z /mir
@robocopy \\10.0.0.142\backup\wiki\data *.* C:\Bitnami\dokuwiki-20150810-0\apps\dokuwiki\htdocs\data *.* /s /r:2 /w:3 /z /mir

#----------------------------------------------------------
#              date and time
#----------------------------------------------------------
get-date -format yyMMdd
( get-date ).DayOfWeek | gm -f
(Get-Date).DayOfWeek
( get-date ).DayOfWeek.value__
[Int] (Get-Date).DayOfWeek  # returns 0 through 6 for current day of week
Get-Date -UFormat %u
(get-date).dayofyear
(Get-Date).AddDays(-1)
(Get-Date).AddDays(-0).ToString('yymmdd')
$a = Get-Date
"Date: " + $a.ToShortDateString()
"Time: " + $a.ToShortTimeString()

$a = (Get-Date).AddDays(7)
Get-Date $a -Format yyMMdd
$a = Get-Date (get-date).adddays(7) -format yyyyMMddd
$time = get-date -format hh:mm
#----------------------------------------------------------
#              searching files
#----------------------------------------------------------
# swiftco files that call copssh
Get-ChildItem -Path C:\RLMS\apps\* -include *.bat,*.ps1, *.vbs | select-string cop | select path

#----------------------------------------------------------
#              zip
#----------------------------------------------------------
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"
$Source = "Y:\IT\inventory\swr_baseline"
$Target = "Y:\IT\inventory\swr_baseline_copy.zip"
sz a -mx=9 $Target $Source


#----------------------------------------------------------
#              what is this?
#----------------------------------------------------------
$colTextFiles = get-childitem e:\temp10050 -filter *.txt -recurse | % { $_.FullName}

foreach ($i in $colTextFiles)
{
	Add-Content e:\temp10050\merge.txt "`n$i`n"
	$fileStuff = Get-Content $i
	Add-Content e:\temp10050\merge.txt $fileStuff
}
#----------------------------------------------------------
#              executables
#----------------------------------------------------------
http://edgylogic.com/blog/powershell-and-external-commands-done-right/

$arg1 = "xxx"
$arg2 = "$yyMMdd"
$allArgs = @($arg1, $arg2)
$batch
#write-host $batch $allArgs
& $batch $allArgs

#calling powershell
If you miss the functionality of allowing a user to double-click a Powershell script to execute use the following as a work-around:
Create a new shortcut For target enter “powershell.exe -noexit 'path-to-your-script'

powershell.exe -noexit 'path-to-your-script'

-WindowStyle Hidden

-NoProfile

#----------------------------------------------------------
#              ftp
#----------------------------------------------------------

Import-Module PSFTP

$FTPServer = 'ftp.host.com'
$FTPUsername = 'username'
$FTPPassword = 'password'
$FTPSecurePassword = ConvertTo-SecureString -String $FTPPassword -asPlainText -Force
$FTPCredential = New-Object System.Management.Automation.PSCredential($FTPUsername,$FTPSecurePassword)


Set-FTPConnection -Credentials $FTPCredential -Server $FTPServer -Session MySession -UsePassive
$Session = Get-FTPConnection -Session MySession

Get-FTPChildItem -Session $Session -Path /htdocs #-Recurse

#----------------------------------------------------------
#            networking plus  softperfect network scanner
#----------------------------------------------------------
#log all installed software
(from softperfect xml)
$data = [xml]$netmap = Get-Content 'c:\backup\current.xml'
foreach($_ in $data.'network-scanner-result'.devices.item.hostname )
{
$cname = $_
Get-WmiObject -Class Win32_Product -ComputerName $cname | Select-Object -Property Name > "Y:\IT\inventory\swr_baseline\$cname.txt"
}


test-connection rlbftp2.rli.local

$socket = new-object Net.Sockets.TcpClient
$socket.Connect("rlbftp2.rli.local",21)
$socket.Connected


#----------------------------------------------------------
#              copying setup files to machines
#----------------------------------------------------------
# gather installed software using softperfect network scanner and export as xml
# query that xml for machines that have quickbooks
# copy quickbooks setup file to machines
[xml]$netmap = Get-Content  'C:\backup\seattle.xml'
$qcomputers = $netmap.'network-scanner-result'.devices.item | ? {$_.apps -like '*Quickbooks*'} | select hostname
$setupfile =  "\\sea-r320-26.rli.local\c$\QuickBooks\16\Setup_QuickBooksEnterprise16.exe"
#$setupfile = 'U:\16\Setup_QuickBooksEnterprise16.exe'
foreach ($_ in $qcomputers)
{
[string]$host = $_.hostname
if (!(Test-Path "`\`\$host`\c`$`\rli"))
{
write-hoste "does not exist"
New-Item -path "`\`\$host`\c`$`\rli" -type directory -Force
}
if (Test-Path "`\`\$host`\c`$`\rli")
{
# copy the file to folders, example destination d:\vision\bank\123\parms
# write-host "$file `\`\$host`\c`$`\rli"
copy-item $setupfile "`\`\$host`\c`$`\rli"
}
}


New-Item -path "$boxfolder`\job.sent" -type file
Invoke-Item "$boxfolder`\job.sent"


#----------------------------------------------------------
#              reading content of files
#----------------------------------------------------------

$reader = [System.IO.File]::OpenText("C:\backup\tickets\installs\adminshares.txt")
try {
    for(;;) {
        $line = $reader.ReadLine()
        if ($line -eq $null) { break }
        # process the line
        $line | gm
    }
}
finally {
    $reader.Close()
}

#----------------------------------------------------------
#              csv validation
#----------------------------------------------------------
C:\backup\csv_validation.ps1

# example of pulling tif paths out of index and testing path
$tpath.clear
$indx.clear
$matches.clear

$tpath = '\E:\\\d{6}\\\d{4}\\docs\\\d{3}\\s\d{10}\.tif'
#$indx = 'E:\151102\1619\afs1619.CSV'
$indx = 'E:\151102\1619\afs1619N.CSV'
$matches = select-string -Path $indx -Pattern $tpath  -AllMatches | % { $_.Matches } | % { $_.Value }
foreach($_ in $matches)
{
$_ + " = " + (test-path $_)
}

# example of pulling tif paths out of index and testing path
$tpath = '\D:\\\d{6}\\\d{4}\\docs\\\d{3}\\s\d{10}\.tif'
$indx = 'C:\backup\test.csv'
$matches = select-string -Path $indx -Pattern $tpath  -AllMatches | % { $_.Matches } | % { $_.Value }
$matches.Count
foreach($_ in $matches)
{
test-path $_ | select *
}

#----------------------------------------------------------
#              string manipulations
#----------------------------------------------------------
http://www.lazywinadmin.com/2013/10/powershell-get-substring-out-of-string.html
"OU=MTL1,OU=CORP,DC=FX,DC=LAB" | get-member
("OU=MTL1,OU=CORP,DC=FX,DC=LAB").split(',')
"OU=MTL1,OU=CORP,DC=FX,DC=LAB" -split ','
("OU=MTL1,OU=CORP,DC=FX,DC=LAB" -split ',')[0]
("OU=MTL1,OU=CORP,DC=FX,DC=LAB" -split ',')[0].substring(3)

Solutions proposed by readers

Jay
'OU=MTL1,OU=CORP,DC=FX,DC=LAB' -match '(?<=(^OU=))\w*(?=(,))'
$matches[0]

Robert Westerlund
"OU=MTL1,OU=CORP,DC=FX,DC=LAB" -match "^OU=(?<MTL1>[^,]*)"
$matches["MTL1"]


$M = gci -path C:\server.2.log | Select-String -pattern "something"
foreach ($_ in $M)
{
#match up to 9 digits in brackets
$_ -match "[\[]{1}\s[0-9]{0,9}[\]]{1}"
$matches.Values
}
#----------------------------------------------------------
#              security
#----------------------------------------------------------

netsh http show sslcert
#----------------------------------------------------------
#              bginfo
#----------------------------------------------------------

C:\Windows\Bginfo.exe C:\tools\rliserver.bgi  /timer:0 /nolicprompt /silent


bginfo
- Create a new custom field of type WMI Query

- In the Path field type: SELECT LastBootUpTime FROM Win32_OperatingSystem

- Add the custom field

#----------------------------------------------------------
#              email
#----------------------------------------------------------


https://4sysops.com/archives/send-mailmessage-paramaters-and-examples/

<https://raw.githubusercontent.com/JurgenVM/Cheatsheets/master/cheatsheets/powershell.md>

# Must have commands
| Command							| Meaning
| :---								| :---
| `gsv | clip`						| Clips output of `gsv` to the clipboard


# Basic commands
| Command							| Meaning
| :---								| :---
| `cls` & `Clear-Host`				| clear the screen
| `cd`								| change directory
| `cd\` 							| go to root directory
| `dir` & `ls` & `Get-Childitem` 	| shows whats in the directory
| `cat` & `Get-Content`				| output the file
| `Copy` & `cp` & `Copy-Item`		| copy the file
| `Measure-command {Get-Process}`	| Shows you the execution time

# Help system
| Command															| Meaning
| :---																| :---
| `Update-Help -Force`												| Update the help
| `Save-Help`														| Save the help to a local file
| `man`																| Scrollable
| `help`															| Alias
| `Get-Help "*keyword*"` 											| Get help
| `Get-Help Get-*` 													| Get all commands which start with "Get-"
| `Get-Help`														| `-ShowWindow`
| 																	| `-Detailed`
|																	| `-Examples`
| 																	| `-Full`
| 																	| `-Online`
| 																	| `-ShowCommand`
| `Get-Help -Category Provider`										|
| `Get-verb`														| Get the verbs used in PowerShell
| `Get-Alias -Definition get-process` 								| Get all aliases for `Get-Process`
| `Get-Help About_*`												| Get all the about topics
| `Get-Process | Get-Member`										| Get all the methods, properties,... of the object
| `Get-Command -Module AD*`											| Get all commands from modules which start with AD
| `Get-Help about* | Out-GridView –PassThru | Get-Help –ShowWindow` | This will display a grid view with all about topics to choose from. Select one, and click OK, to view the help file.

# Export

| Command			| Meaning
| :---				| :---
| `Export-csv`		|
| `Export-Clixml`	|
| `Out-File`		|
| `Out-Printer`		|
| `Out-GridView`	|
| `Out-File`		|
|					|
| `ConvertTo-Csv`	|
| `ConvertTo-Html`	|

# Pipeline

**Example: you want to check if a service is running on all the computers in the AD <br />**

| Command								| Meaning
| :---									| :---
| `Get-ADComputer -Filter * 	| gm`	| Check what object you are working with (in this case `ADComputer`)
| `Get-Help Get-Service -ShowWindow`	|
| 										| Search for `-InputObject` and see what it accepts (in this case `ServiceController` != `ADComputer`)
|										| `-ComputerName` supports `ByPropertyName`
| `Get-ADComputer -Filter *`			| Check for propertyname
|										| `-Name` gives you the name of the computer

`Get-ADComputer -Filter * -Name | Select -Property @{n(ame)='ComputerName';e(xpression)={$_.name}} 	| Get-Service -Name bits`<br />

**Example: command does not accept pipeline input (Get-WmiObject)<br />**

`Get-WmiObject -class win32_bios -ComputerName (Get-ADComputer -Filter *).Name`

_Or_

`Get-ADComputer -Filter * | Get-WmiObject win32_bios -Computername {$_.Name}`

# Create new property

| Command																| Meaning
| :---																	| :---
| `Select -Property @{n(ame)='ComputerName';e(xpression)={$_.name}}`	| Creates a new property called `ComputerName` from the existing property `Name`
| `Select -ExpandProperty name`											| Return the property in a array

# WmiObject & CimInstance

# Remoting
| Command																| Meaning
| :---																	| :---
|`Invoke-Command -ComputerName dc {Restart-Computer}`					| Execute the command on specific computers
| `icm dc {Restart-Computer}`											| Short version

# Execution Policy

| Command			| Meaning
| :---				| :---
| `Restricted`		|
| `Unrestricted`	| Run any script **(not recommended)**
| `AllSigned`		|
| `RemoteSigned`	| Only run scripts that are remote signed
| `Bypass`			|
| `Undefined`		|

# Powershell Remote Session

| Command										| Meaning
| :---											| :---
| `Get-PSSession`								| Get open sessions
| `$sessions = New PSSession -ComputerName dc`	| Open session
| `icm -Session $sessions {...}`				| Execute command in script block to all sessions

# Parallelism

**Parallel:** <br />
 `icm -Computername (Get-Content servers.txt) { ... }` <br />

**Serial:** <br />
 `foreach ($s in (Get-Content servers.txt) ) { icm -Computername $s { ... }}`

# Scripting

| Command													| Meaning
| :---														| :---
| `CTRL + J`												| Helps you build a script
| `$var = 1`												| Declares new variabele `var` with value `1`
| `param($ComputerName)`									| Create new parameter
| `[ValidateSet("a","b","c")][string]$x = "a"`				| Value of $x **must** be a value of the ValidateSet
| `"this is $var"`											| `"` Resolves the value of $var
| `'this is $var'`											| `'` Does **not** resolve the value
| `"The value of &#96;$var is $var"`						| the &#96; (`&#96;`) prevents that the value is resolved
| `@' '@`													| Multiline string, usefull for `New-Snippet`
| `-ErrorAction`											|
| `-ErrorVariabele`											|

######################################
# yaml

Import-Module C:\backup\apps\PowerYaml-master\PowerYaml.psm1

$yaml = Get-Yaml -FromFile (Resolve-Path C:\backup\apps\PowerYaml-master\sample.yml)
$yaml.parent.child



######################################
#    monitoring
######################################

https://code.google.com/p/internetconnectivitymonitor/

# group policy
see effective
http://www.howtogeek.com/116184/how-to-see-which-group-policies-are-applied-to-your-pc-and-user-account/
monitor
https://social.technet.microsoft.com/forums/windowsserver/en-US/9a65b012-e4fd-427f-a4ce-d77823e4f14c/find-group-policy-change-history
# add domain to local admin
http://social.technet.microsoft.com/wiki/contents/articles/7833.how-to-make-a-domain-user-the-local-administrator-for-all-pcs.aspx
#locally
add the domain user to the local administrator group,
to do this right click on computer go to manage then expand the system tools tab,
then go to users and groups, on selecting groups go to the administrators group right click on it
and go to properties go to add and type in the domain user you need to add.
######################################
#    outlook
######################################

public sharing
https://technet.microsoft.com/en-us/library/aa998834(v=exchg.141).aspx


######################################
#    compare
######################################



$all = get-content 'c:\trash\all'
$internal = get-content 'C:\trash\internal'
$deactivates = Compare-Object $all $internal  | Where-Object { $_.SideIndicator -eq '<=' }
$deactivates >> 'C:\trash\deactivate.txt'

(get-content 'c:\trash\all').count

(get-content 'C:\trash\internal').count


######################################
#    ip
######################################

$input_path = ‘c:\ps\ip_addresses.txt’
$output_file = ‘c:\ps\extracted_ip_addresses.txt’
$regex = ‘\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b’
select-string -Path $input_path -Pattern $regex -AllMatches | % { $_.Matches } | % { $_.Value } > $output_file
######################################
http://www.hanselman.com/blog/ScottHanselmans2014UltimateDeveloperAndPowerUsersToolListForWindows.aspx
######################################
#    internal app examples
######################################


web50000.exe E:\Images\151221\10063\manual\manualupload.csv 10063 web50000.xml

afsimport.bat box yyyymmdd

http://www.retaillockbox.supportsystem.com/scp/tickets.php?id=3711

00152
C:\RLMS\apps>createCycloneach2.bat 00152 12 22 2015


######################################
#    calling function
######################################

===Calling functions from another script===
Let's say that you have one script that has the functions that you want to use called function.ps1.
In another script, say testfunctions.ps1 you can call the functions in function.ps1.  Here is how.

Make a ps1 file with the following contents and call the file function.ps1
<code>
function F1{write-host "Here it is";}
function F2{write-host "Here it is again";}
</code>

Next write another ps1 file with the following contents and call the file testfunctions.ps1
<code>
. C:\users\darren.hoehna\desktop\functions.ps1 #The spaces of the dots before C:\ needs to be there.  So the start of
#path is .(space)C:\\

F1
F2
</code>

When you run testfunctions.bat this should be the output:\\
Here it is\\
Here it is again

-----
[[it:retailwebroot | Back to RetailWEB Index]]\\


######################################
#    sleep in batch
######################################
example E:\rlbobj\epay3576.ach.bat
@set SCRIPTF=%temp%\~%BOX%scp.vbs
:Cont

@echo wscript.sleep 10000 >%SCRIPTF%
@start /w wscript.exe %SCRIPTF%
@del %SCRIPTF%


######################################
#    qdir
######################################
"%Program Files%\Q-Dir\Q-Dir.exe" "Z: \"

######################################
#    rdp
######################################
see if its enabled

netsh advfirewall firewall show rule name="Remote Desktop (TCP-In)"
https://support.pertino.com/hc/en-us/articles/200488829-How-to-enable-remote-desktop-from-Windows-command-line-CMD-
##############################
# http://wiki/doku.php?id=it:serverinfo:hosted:swiftco-vsa8xyg
##############################
# to generate this list
Get-ChildItem -Path C:\RLMS\apps\* -include *.bat,*.ps1, *.vbs | select-string cop | select path | out-file c:\rlms\apps\calls_cop.txt

##############

$folder = 'c:\backup'
$filter = '*.*'
$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{IncludeSubdirectories = $false;NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'}
$onCreated = Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated -Action {
$name = $Event.SourceEventArgs.Name
$timeStamp = $Event.TimeGenerated
Send-MailMessage -To shane.null@retaillockbox.com -From shane.null@retaillockbox.com  `
-Subject "File Added" -Body "$name was uploaded on $timestamp" -SmtpServer '10.0.0.12'
}


###################
# sql


function Invoke-SQL {
    param(
        [string] $dataSource = "SEA-2950-22.\SQLEXPRESS",
        [string] $database = "MasterData",
        [string] $sqlCommand = $(throw "Please specify a query.")
      )

    $connectionString = "Data Source=$dataSource; " +
            "Integrated Security=SSPI; " +
            "Initial Catalog=$database"

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($sqlCommand,$connection)
    $connection.Open()

    $adapter = New-Object System.Data.sqlclient.sqlDataAdapter $command
    $dataset = New-Object System.Data.DataSet
    $adapter.Fill($dataSet) | Out-Null

    $connection.Close()
    $dataSet.Tables

}


################


get db names
dir SQLSERVER:\\SQL\sea-2950-22.rli.local\Default\Databases | select name


Using SQL with Powershell

$SqlConnection = New-Object System.Data.SqlClient.SqlConnection #Do not modify.  This makes an object that holds the
connection to the SQL Server
$SqlConnection.ConnectionString = "Data Source=SEA-TESTDB\RLITEST;Initial Catalog=grouphealth;Persist Security Info=True;
User ID=<User ID>;Password=<Password>;"


$SqlCmd = New-Object System.Data.SqlClient.SqlCommand #Do not modify.  This makes the object that holds the SQL Query
$SqlCmd.CommandText = "select DISTINCT City from grouphealth.dbo.statementdetail" #This is the command that you want SQL to do.
$SqlCmd.Connection = $SqlConnection

$SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter #Don't modify.  This makes a new object that holds the
adapter for the SQL server.
$SqlAdapter.SelectCommand = $SqlCmd

$DataSet = New-Object System.Data.DataSet
$numberOfRows = $SqlAdapter.Fill($DataSet)

$SqlConnection.Close() #Always make sure to close the connection
To access data in a cell:

 $DataSet.Tables[0].Rows[0]["City"]

 #find active directory users
  Get-Module –ListAvailable

  #Detailed error message
trap
{
   throw  ($_ | format-list * -force | out-string)
}
#Detailed error message
try{
    #run your code here
}
catch
{
   throw  ($_ | format-list * -force | out-string)
}
############################################################
# 2498 esd cheater test-remove remove files function
############################################################
<#frequent tasks for ach duplicates
#open qdir
$qdirpath = gci -path e:\tools -filter *.qdr -r | ? {$_.name -match "$box*"} | select fullname
if(test-path $qdirpath.FullName){invoke-item $qdirpath.fullname}

#give a date or use today
#$yymmdd = read-host -prompt 'input date yymmdd'
$yymmdd = get-date -Format yyMMdd
#zip a copy of the folder
$box = 2498

# calling backup script, run Backupe to create copy.zip
. c:\backup\backupefolder.ps1
Backupe
pause #pause before removing
$boxfolder = 'e:\' + $yymmdd + '\' + $box

#remove files to be recreated
test-path
E:\160302\2498\160302_2498E:\160302\2498\160302_2498
$htm = $boxfolder + '\' + $yymmdd + '_' + $box + '_ACH.htm'
$ACH =  $boxfolder + '\' + $yymmdd + '_' + $box + '_ACH.dat'
$xml =  $boxfolder + '\' + $yymmdd + '_' + $box + '_Output.xml'

function test-remove
{
param($filepath)
if(Test-Path($filepath)){
write-host "removed $filepath"
Remove-Item -Path $filepath}
else{write-host "$filepath doesn't exist"}
}
test-remove $htm
test-remove $ACH
test-remove $xml

#>



#module that runs exe for box numbers in xml file, validates yymmdd

<#
Automation
job host:
job software:
job folder:
job name:
job time:
job called by:
job calls:
start-process "http://sea-1750-79:9675/tickets/v2#recently_updated/14362"
start-process "http://wiki/doku.php?id=it:software:rlms2retailweb"
validate parameter tips
start-process http://powershell.com/cs/blogs/tips/archive/2013/07/19/using-validatepattern-attribute.aspx
This can be used as a module
#>



function Import-RlmsToRliAll {
  <#
  .SYNOPSIS
  runs e:\rlbobj\rlm20000.exe for all boxes in E:\Accounts\rlm200.xml
  .EXAMPLE
  Import-RlmsToRliAll 20160226
  .PARAMETER
  yymmdd
  #>
  [CmdletBinding()]
  param
  (
     [Parameter(Mandatory=$true,HelpMessage='to pass date use yymmdd')]
     [ValidatePattern('(?# date uses yymmdd)^[0-9][0-9]([0][1-9]|[1][1-2])[0-3][0-9]$')]
     [String] $yymmdd
  )
  begin {
    #if(-not $yymmdd){$yymmdd = get-date -format yyMMdd}
    $rlmsrliconfigfile = 'E:\Accounts\rlm200.xml'
    [xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile
    #$rlmsrlixml.achinfo.box.number
  }
  process {
        foreach($_ in $rlmsrlixml.achinfo.box.number)
        {
        #write-host "do stuff with e:\rlbobj\rlm20000 e:\$yymmdd\$_\ $yymmdd $_ "
        $arg0 = "e:\$yymmdd\$_\"
        $arg1 = "$yymmdd"
        $arg2 = "$_"
        $allArgs = @($arg0,$arg1,$arg2)
        #$rlm200 = 'E:\rlbobj\rlm20000.exe'
        $rlm200 = 'e:\rlbobj\test.exe'
        $CommandLine = "& $rlm200 $allArgs"
        write-host "do this $CommandLine "
        #Invoke-Expression $CommandLine e:\rlbobj\rlm20000 e:\160226\70260\ 160226 70260
        sleep -Seconds 5
        }
    }
}

#variable tests
#no variable
#Import-RlmsToRliAll
#invalid
#Import-RlmsToRliAll 20160101
#valid
#Import-RlmsToRliAll 160101

<#
info gathering

# find customers
$rlmsrliconfigfile = 'E:\Accounts\rlm200.xml'
[xml]$rlmsrlixml = Get-Content $rlmsrliconfigfile
#client
$rlmsrlixml.achinfo.box.client | clip
#box
$rlmsrlixml.achinfo.box.number

# find scripts

#search in files http://wiki/doku.php?id=it:software:rlms2retailweb
$pattern = 'rlm20000'
Get-ChildItem  -Path @("e:\rlbobj","e:\scripts") -file -Filter *.bat | Where {$_.PSisContainer -eq $false} | Select-String -pattern $pattern | group path | select name
.examples
e:\rlbobj\rlm20000 e:\160226\70260\ 160226 70260

@call e:\rlbobj\rlm20000 %RMSDATA% %DATE% %RBOX%
@set DATE=%FULLDATE:~2%
@set WORKPATH=e:\%DATE%\%RBOX%
@set RMSDATA=%WORKPATH%\rms%BOX%.dat
@set RBOX=70260


#>

##########get drive space
$disk = Get-WmiObject Win32_LogicalDisk -ComputerName 10.0.0.32 -Filter "DeviceID='C:'" | Select-Object Size,FreeSpace
$disk.Size
$disk.FreeSpace

