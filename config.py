folders = dict(
    file_folder = 'markdown/',
    html_folder = 'html/', # used by convert & serve
    pdf_folder = 'pdf/',
    latex_folder = 'latex/',
    www_folder = 'www/', # used by deploy
    )

test_serve = dict(
    port = 8080
    )
