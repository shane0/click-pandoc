#!/usr/bin/python
# -*- coding: utf-8 -*-
# command line interfaces are for noobsaucers
import os
import click
import config
import subprocess
import shutil
import test_serve

FILE_FOLDER = config.folders['file_folder']
HTML_FOLDER = config.folders['html_folder']
PDF_FOLDER = config.folders['pdf_folder']
LATEX_FOLDER = config.folders['latex_folder']
WWW_FOLDER = config.folders['www_folder']
PORT = config.test_serve['port']

def _posixify(name):
        return '-'.join(name.split()).lower()

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d) 

def make_dir(directory):
    if not os.path.exists(directory):
        os.mkdir(directory)

@click.group()
@click.option('--verbose', is_flag=True)
def cli(verbose):
    """\b
    the stupid simple command line for a markdown html webserver
    usage: cli.py --help | cli.py <command> --help
    """
    if verbose:
        click.echo('verbose mode is on')

@cli.command('setup')
def setup():
    """setup folders"""
    for key in config.folders:
        try:
            click.echo('creating folder ' + config.folders[key])
            make_dir(config.folders[key])
        except Exception as e:
            print('error ' + str(e))

@cli.command('write')
@click.option('--name',prompt='enter a filename.md',type=click.File('w'))
def write(name):
    """create a markdown file from your template"""
    if click.confirm('write to %s ?' % name):
        name.name = FILE_FOLDER + name.name
        click.echo('filename: ' + name.name)
        # yaml
        click.echo('---',file=name)
        click.echo(message='title: ' + name.name.replace(FILE_FOLDER,'').replace('.md',''),file=name)
        click.echo('---',file=name)
        click.echo('completed')

@cli.command('html') 
@click.option('--mdfile',prompt='filename.md file name',default='filename.md') 
def make_html(mdfile): 
    """ convert markdown file to html """ 
    mdfile = FILE_FOLDER + mdfile 
    outfile = mdfile.replace(FILE_FOLDER,HTML_FOLDER).replace('.md','.html')
    click.echo('converting to: ' + outfile) 
    args = ['pandoc', mdfile,'-f','markdown','-t','html','-s','-o',outfile]
    subprocess.check_call(args)

@cli.command('pdf')
@click.option('--mdfile',prompt='filename.md file name',default='filename.md') 
def make_pdf(mdfile):
    """convert markdown to pdf"""
    mdfile = FILE_FOLDER + mdfile 
    outfile = mdfile.replace(FILE_FOLDER,PDF_FOLDER).replace('.md','.pdf')
    click.echo('converting to: ' + outfile) 
    args = ['pandoc', mdfile,'-f','markdown','-t','latex','-s','-o',outfile]
    subprocess.check_call(args)

@cli.command('docx')
@click.option('--mdfile',prompt='filename.md file name',default='filename.md') 
def make_docx(mdfile):
    """convert markdown to docx"""
    mdfile = FILE_FOLDER + mdfile 
    outfile = mdfile.replace(FILE_FOLDER,PDF_FOLDER).replace('.md','.docx')
    click.echo('converting to: ' + outfile) 
    args = ['pandoc', mdfile,'-f','markdown','-t','docx','-s','-o',outfile]
    subprocess.check_call(args)

@cli.command('convert')
@click.option('--inputfile',prompt='enter filename',default='filename.xxx') 
@click.option('--informat',prompt='enter format',default='markdown') 
@click.option('--outputfile',prompt='enter output filename',default='filename.xxx') 
@click.option('--outformat',prompt='enter output format',default='pdf') 
def make_pdf(inputfile,informat,outputfile,outformat):
    """convert input format to outuput format"""
    click.echo('converting ' + inputfile + informat + ' to ' ) 
    click.echo(outputfile + outformat) 
    args = ['pandoc', inputfile,'-f',informat,'-t',outformat,'-s','-o',outputfile]
    click.echo(args)
    #subprocess.check_call(args)
    
@cli.command('latex')
@click.option('--mdfile',prompt='filename.md file name',default='filename.md') 
def make_pdf(mdfile):
    """convert markdown to latex"""
    mdfile = FILE_FOLDER + mdfile 
    outfile = mdfile.replace(FILE_FOLDER,LATEX_FOLDER).replace('.md','.tex')
    click.echo('converting to: ' + outfile) 
    #pandoc -s MANUAL.txt -o example4.tex
    args = ['pandoc', mdfile,'-f','markdown','-t','latex','-s','-o',outfile]
    subprocess.check_call(args)

@cli.command('serve')
def serve():
    """start the simple python http server"""
    test_serve.main(HTML_FOLDER,PORT)

@cli.command('deploy')
def deploy():
    """copy files to prod www server"""
    src = './'+ HTML_FOLDER
    dst = WWW_FOLDER
    copytree(src,dst)

if __name__ == '__main__':
    cli()
