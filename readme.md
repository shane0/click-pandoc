# click-pandoc readme

Static html site tools and general use [click](https://click.palletsprojects.com/en/7.x/) app with [pandoc](https://pandoc.org/) and simple http server that I use on <http://shanenull.com> and other places.

screencast: <https://www.youtube.com/watch?v=LU_h39xiURQ>

![](click-pandoc-demo.gif)

## install

for linux & python

[run setup.sh](utils/setup.sh)


```sh
pip install -r requirements.txt
```

## usage

```shell
python cli.py


> commands

listed here in order of workflow

* setup - run as needed, sets up folders defined in `config.py` 

* [x] write - creates a markdown file from template
* [x] convert - convert any to any 
* [x] html - convert markdown to html
* [x] docx - convert markdown to docx 
* [x] pdf - convert markdown to pdf 
* [x] latex - convert markdown to latex 
* [x] serve - local python web server
* [x] deploy - copies all html files from test to production

> note on server

```text
# If Python version returned above is 3.X
python3 -m http.server
# On windows try "python" instead of "python3", or "py -3"
# If Python version returned above is 2.X
python -m SimpleHTTPServer
```

## configuration

`config.py`

setup your folders and ports here

